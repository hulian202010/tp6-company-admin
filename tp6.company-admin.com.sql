/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : tp6.company-admin.com

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 06/04/2021 16:49:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for _generate
-- ----------------------------
DROP TABLE IF EXISTS `_generate`;
CREATE TABLE `_generate`  (
  `type` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` date NULL DEFAULT NULL,
  `number` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`type`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数字生成器' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of _generate
-- ----------------------------
INSERT INTO `_generate` VALUES ('abc', '2020-12-17', 70);

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(255) NULL DEFAULT 0,
  `cid` int(11) NULL DEFAULT 0,
  `ct_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `author` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `origin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `views` int(11) NULL DEFAULT 0,
  `status` tinyint(4) NULL DEFAULT 1,
  `sort` tinyint(4) NULL DEFAULT 100,
  `send_date` datetime(0) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE,
  INDEX `send_date`(`send_date`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (1, 0, 6, NULL, '标题', 'uploads/solution_img/20200421/8ada415b1e08ed0707a648591cf382c7.jpg', '作者1', '搜狐', '介绍', '<p><span style=\\\\\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\\\\\\\">内容</span></p>', 112, 1, 100, '2020-04-21 18:18:02', 1587464322, 1607522187, 1607522187);
INSERT INTO `article` VALUES (2, 0, 6, NULL, '体温新闻1', '/uploads/article/20200515/19f464f7087b353866512a8fe09d66d3.png', '体温', '', '介绍', '<p><span style=\\\\\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\\\\\\\">内容</span></p>', 101, 1, 100, '2020-04-21 18:21:05', 1587464491, 1607522094, 1607522094);
INSERT INTO `article` VALUES (3, 0, 6, NULL, '我是新闻标题', 'uploads/solution_img/20200421/0ddda62a8e168f6e2663f2aa01cd3dca.jpg', '我是作者', NULL, '介绍', '<p><span style=\\\\\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\\\\\\\">内容</span></p>', 101, 1, 100, '2020-04-21 19:42:25', 1587469365, 1607522112, 1607522112);
INSERT INTO `article` VALUES (4, 0, 6, NULL, '公司新闻', 'uploads/solution_img/20200421/84f6ead8d273f215964534c9020faad2.jpg', '作者 作者', NULL, '', '<p><label class=\\\\\\\"col-sm-2 control-label\\\\\\\" style=\\\\\\\"box-sizing: border-box; display: inline-block; max-width: 100%; margin-bottom: 0px; font-weight: 700; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 275.484px; padding-top: 7px; text-align: right; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\\\\\\\">作者</label></p><p><input type=\\\\\\\"text\\\\\\\" class=\\\\\\\"form-control\\\\\\\" placeholder=\\\\\\\"作者\\\\\\\" name=\\\\\\\"author\\\\\\\" value=\\\\\\\"\\\\\\\" maxlength=\\\\\\\"150\\\\\\\" style=\\\\\\\"box-sizing: border-box; color: rgb(85, 85, 85); font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.42857; font-family: inherit; margin: 0px; padding: 6px 12px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); outline: 0px; display: block; width: 1347.5px; height: 34px; background-color: rgb(255, 255, 255); background-image: none; border-width: 1px; border-style: solid; border-color: rgb(210, 214, 222); border-radius: 0px; box-shadow: none; transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s, -webkit-box-shadow 0.15s ease-in-out 0s; -webkit-appearance: none;\\\\\\\"/></p><p><br/></p>', 1, 1, 100, '2020-04-21 19:42:50', 1587469388, 1607522129, 1607522129);
INSERT INTO `article` VALUES (5, 0, 6, NULL, 'test', '/uploads/article/20200515/c42bfbf56dbd0bf2b179e8662a20e8bb.jpg', '作者 作者', '搜狐', '描述', '<p><span style=\\\\\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\\\\\\\">内容</span></p>', 0, 2, 100, NULL, 1589539435, 1589539572, 1589539572);
INSERT INTO `article` VALUES (6, 1, 6, NULL, 'xx', '/uploads/article/20200831/99580b3bdfb6e4c02f43803f1f8346b7.jpg', NULL, NULL, 'xxx', NULL, 0, 2, 100, NULL, 1598877914, 1598878121, 1598878121);
INSERT INTO `article` VALUES (7, 1, 6, NULL, 'ssdf', '/uploads/article/20200831/b3c528ebd10b4cf2e1a11637d9d9ef73.png', NULL, NULL, 'asdfsdf', NULL, 0, 1, 1, NULL, 1598877985, 1607522118, 1607522118);
INSERT INTO `article` VALUES (8, 2, 6, NULL, 'honor标题2', '/article/20200910/FnVWlLo7pR1Hmn48SmRBsX57tBSv.png', NULL, NULL, 'sdf2', NULL, 0, 1, 1, NULL, 1598878248, 1607522196, 1607522196);
INSERT INTO `article` VALUES (9, 2, 6, NULL, 'test', '/uploads/article/20200831/8944ccbc1c9e98e8149c0b760d1ba09e.jpg', NULL, NULL, 'test', NULL, 0, 1, 100, NULL, 1598878854, 1607522224, 1607522224);
INSERT INTO `article` VALUES (10, 2, 6, NULL, '3', '/uploads/article/20200831/cd4074a85703104c0c63a965e876be8e.jpg', NULL, NULL, '', NULL, 0, 1, 100, NULL, 1598879067, 1607522211, 1607522211);
INSERT INTO `article` VALUES (11, 3, 6, NULL, '标题', '/uploads/article/20200831/15b3c251396f28e0a3c76d26690fa4a9.jpg', NULL, NULL, 'ate', NULL, 0, 1, 100, NULL, 1598879499, 1598880028, 1598880028);
INSERT INTO `article` VALUES (12, 3, 6, NULL, 'test', '/uploads/article/20200831/dbcde4faf993949d310cb8bc9accec00.jpg', NULL, NULL, 'asdgfdas', NULL, 0, 1, 100, NULL, 1598879530, 1598880031, 1598880031);
INSERT INTO `article` VALUES (13, 3, 6, NULL, '风采三', '/uploads/article/20200831/89c72f5be7e077906a9fd113d05eb09e.jpg', NULL, NULL, 'fdsf', NULL, 0, 1, 100, NULL, 1598879581, 1598880034, 1598880034);
INSERT INTO `article` VALUES (14, 3, 6, NULL, 'test', '/article/20200910/Fit4NI_1-FGwFY77oDf6_FQvMg9o.jpg', NULL, NULL, 'a\r\nb', NULL, 0, 1, 100, NULL, 1598879915, 1607522204, 1607522204);
INSERT INTO `article` VALUES (15, 3, 6, NULL, 'test2', '/uploads/article/20200831/2b6836368ebc0cb38e12d7bd78d20a6b.jpg,/uploads/article/20200831/86d137259559c941ebc8a5f11e39d036.jpg', NULL, NULL, '23\r\n43', NULL, 0, 2, 100, NULL, 1598880968, 1607568704, 1607568704);
INSERT INTO `article` VALUES (16, 0, 6, 0, 'fasdf', '/uploads/article/20200831/923f17d93952f02d3d4e35cd27c9cde3.jpg', 'dsaf', 'ar', 'sdaf', '<p>asdf</p>', 99, 2, 100, '2020-08-31 00:00:00', 1598881224, 1607522063, 1607522063);
INSERT INTO `article` VALUES (17, 0, 6, 7, 'test3', '/uploads/article/20200831/2c1dca00eeb665cc758fff10401672ac.jpg', 'test3', '1', 'test3', '<p><img src=\"http://tp6.ry.com/ueditor/php/upload/image/20200901/1598942037878999.jpg\" title=\"1598942037878999.jpg\" alt=\"about_adv.jpg\"/>test3</p>', 3, 1, 100, '2020-08-31 00:00:00', 1598881533, 1607522086, 1607522086);
INSERT INTO `article` VALUES (18, 0, 6, 0, '标题23', 'http://qn.ymkj6188.com/image/20201116/Fu80lXi6j0EM9-xOxvthh6ig9Fzu.png', '作者1', '来源1', '简介1', '<p>我是详情</p>', 10, 1, 100, '2020-11-17 00:00:00', 1605519477, 1607568597, NULL);
INSERT INTO `article` VALUES (19, 0, 14, 15, '文章标题1', 'http://qn.ymkj6188.com/image/20201210/FjH75MziaEklhbMlAdiIVp-fsG-g.jpg', '作者2', '来源3', '说明123', '<p><b>欢</b><img src=\"http://qn.ymkj6188.com/image/20201210/Fu80lXi6j0EM9-xOxvthh6ig9Fzu.png\" style=\"max-width:100%;\"><b>2342342342344645645658578</b></p><p><img src=\"http://tp6.company-admin.com/uploads/article/20201210/0cc0b223997f97a8ea4338cb67da44a0.png\" style=\"max-width: 100%;\"><img src=\"http://qn.ymkj6188.com/image/20201209/Fu80lXi6j0EM9-xOxvthh6ig9Fzu.png\" style=\"max-width:100%;\"><img src=\"http://qn.ymkj6188.com/image/20201210/FuKKaDA1CYBs5qL-hI3znokuE0bo.png\" style=\"max-width: 100%;\"><br></p>', 100, 2, 100, '2020-12-31 00:00:00', 1607521324, 1607573315, NULL);
INSERT INTO `article` VALUES (20, 0, 14, 15, 'ie', 'http://qn.ymkj6188.com/image/20201209/FjH75MziaEklhbMlAdiIVp-fsG-g.jpg', '作者', '来源', '说明ie', '<p><img style=\"max-width: 100%;\" src=\"http://qn.ymkj6188.com/image/20201209/FjH75MziaEklhbMlAdiIVp-fsG-g.jpg\"><br></p>', 12, 1, 100, '2020-12-25 00:00:00', 1607523613, 1607523613, NULL);
INSERT INTO `article` VALUES (21, 0, 20, 0, '文章标题', 'http://tp6.zbyp.com/uploads/article/20210322/5284b15dac9626a27bef5cc0fcf0f649.png', '作者', '来源', '说明', '<p>内容<br></p>', 2, 1, 100, '2021-03-22 00:00:00', 1616378439, 1616378603, 1616378603);

-- ----------------------------
-- Table structure for article_cate
-- ----------------------------
DROP TABLE IF EXISTS `article_cate`;
CREATE TABLE `article_cate`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT 0,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` tinyint(4) NULL DEFAULT 100,
  `status` tinyint(4) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `sort`(`sort`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_cate
-- ----------------------------
INSERT INTO `article_cate` VALUES (22, 20, 'tetet111', NULL, 100, 2, 1616378207, 1616378217, 1616378217);
INSERT INTO `article_cate` VALUES (21, 18, 'test', NULL, 100, 1, 1607608805, 1607608805, NULL);
INSERT INTO `article_cate` VALUES (20, 0, '商品分类', NULL, 100, 1, 1607581225, 1607581225, NULL);
INSERT INTO `article_cate` VALUES (19, 14, 'tyrt', NULL, 100, 2, 1607523282, 1607608797, NULL);
INSERT INTO `article_cate` VALUES (18, 0, 'ffff', NULL, 100, 1, 1607511647, 1607568684, NULL);
INSERT INTO `article_cate` VALUES (17, 14, '1111', NULL, 100, 1, 1607511629, 1607511629, NULL);
INSERT INTO `article_cate` VALUES (16, 14, '分类1-1', NULL, 100, 1, 1607511609, 1607515534, 1607515534);
INSERT INTO `article_cate` VALUES (15, 14, '分类2222', NULL, 100, 1, 1607511591, 1607568699, 1607568699);
INSERT INTO `article_cate` VALUES (14, 0, '分类1', NULL, 100, 1, 1607511578, 1607514057, NULL);

-- ----------------------------
-- Table structure for c_user
-- ----------------------------
DROP TABLE IF EXISTS `c_user`;
CREATE TABLE `c_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mch_id` int(11) NULL DEFAULT 0 COMMENT '商户id',
  `cid` int(10) NULL DEFAULT 0 COMMENT '优惠券id',
  `uid` int(10) NULL DEFAULT 0 COMMENT '用户id',
  `type` tinyint(4) NULL DEFAULT 0 COMMENT '类型',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '券名',
  `money` int(10) NULL DEFAULT 0 COMMENT '优惠金额',
  `full_money` int(10) NULL DEFAULT 0 COMMENT '满多少金额才能使用',
  `pwd` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 0,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详细内容',
  `over_date` date NULL DEFAULT NULL COMMENT '过期时间',
  `self_get` tinyint(4) NULL DEFAULT 0 COMMENT '自己领取',
  `use_time` int(11) NULL DEFAULT NULL COMMENT '使用时间',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`, `cid`) USING BTREE,
  INDEX `over_date`(`over_date`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 112 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of c_user
-- ----------------------------
INSERT INTO `c_user` VALUES (1, '072R9IN6', 0, 18, 10001, 0, '/assets/images/coupon/d917ac0539238c597e0f95f64e7224f0.jpg', '满10减2', 2, 10, NULL, 2, NULL, '2020-10-24', 1, 1603266883, 1603246765, 1604546416, NULL);
INSERT INTO `c_user` VALUES (2, 'P9IB3ACO', 0, 17, 10001, 0, '/assets/images/coupon/6e6c5dad10af62152adb9c6e773f4e65.jpg', '满5减1', 1, 5, NULL, 0, NULL, NULL, 1, 1609320886, 1603246808, 1609728606, NULL);
INSERT INTO `c_user` VALUES (3, 'VD68JFVS', 0, 19, 10001, 0, 'assets/images/coupon/6e6c5dad10af62152adb9c6e773f4e65.jpg', '永久有效', 1, 10, NULL, 0, NULL, NULL, 1, 1603268094, 1603247179, 1604546416, NULL);
INSERT INTO `c_user` VALUES (4, 'SRM5NM2J', 0, 17, 10003, 0, '/assets/images/coupon/6e6c5dad10af62152adb9c6e773f4e65.jpg', '满5减1', 1, 5, NULL, 0, NULL, NULL, 1, NULL, 1603350316, 1603350316, NULL);
INSERT INTO `c_user` VALUES (5, 'BVVUL0TB', 0, 1, 10001, 0, '/assets/images/coupon/60c088458fecfa6b36e7de6461abebac.jpg', '72块优惠券 全品类通用1', 72, 0, NULL, 2, '<p><img src=\\\\\\\"\\\\\\\\&quot;https://www.juanss.com/ueditor/php/upload/image/20200515/1589534406723576.jpg\\\\\\\\&quot;\\\\\\\" title=\\\\\\\"\\\\\\\\&quot;1589534406723576.jpg\\\\\\\\&quot;\\\\\\\" alt=\\\\\\\"\\\\\\\\&quot;微信图片_20200515171738.jpg\\\\\\\\&quot;/\\\\\\\"/></p>', '2020-10-30', 0, NULL, 1603771390, 1603771390, NULL);
INSERT INTO `c_user` VALUES (6, 'FL53DB0P', 0, 1, 10003, 0, '/assets/images/coupon/60c088458fecfa6b36e7de6461abebac.jpg', '72块优惠券 全品类通用1', 72, 0, NULL, 2, '<p><img src=\\\\\\\"\\\\\\\\&quot;https://www.juanss.com/ueditor/php/upload/image/20200515/1589534406723576.jpg\\\\\\\\&quot;\\\\\\\" title=\\\\\\\"\\\\\\\\&quot;1589534406723576.jpg\\\\\\\\&quot;\\\\\\\" alt=\\\\\\\"\\\\\\\\&quot;微信图片_20200515171738.jpg\\\\\\\\&quot;/\\\\\\\"/></p>', '2020-10-30', 0, NULL, 1603771390, 1603771390, NULL);
INSERT INTO `c_user` VALUES (7, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037868, 1608037868, NULL);
INSERT INTO `c_user` VALUES (8, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037868, 1608037868, NULL);
INSERT INTO `c_user` VALUES (9, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037868, 1608037868, NULL);
INSERT INTO `c_user` VALUES (10, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037874, 1608037874, NULL);
INSERT INTO `c_user` VALUES (11, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037874, 1608037874, NULL);
INSERT INTO `c_user` VALUES (12, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037913, 1608037913, NULL);
INSERT INTO `c_user` VALUES (13, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037913, 1608037913, NULL);
INSERT INTO `c_user` VALUES (14, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037972, 1608037972, NULL);
INSERT INTO `c_user` VALUES (15, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608037972, 1608037972, NULL);
INSERT INTO `c_user` VALUES (16, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038060, 1608038060, NULL);
INSERT INTO `c_user` VALUES (17, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038060, 1608038060, NULL);
INSERT INTO `c_user` VALUES (18, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038069, 1608038069, NULL);
INSERT INTO `c_user` VALUES (19, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038069, 1608038069, NULL);
INSERT INTO `c_user` VALUES (20, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038300, 1608038300, NULL);
INSERT INTO `c_user` VALUES (21, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038316, 1608038316, NULL);
INSERT INTO `c_user` VALUES (22, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038326, 1608038326, NULL);
INSERT INTO `c_user` VALUES (23, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038346, 1608038346, NULL);
INSERT INTO `c_user` VALUES (24, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038407, 1608038407, NULL);
INSERT INTO `c_user` VALUES (25, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038407, 1608038407, NULL);
INSERT INTO `c_user` VALUES (26, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038407, 1608038407, NULL);
INSERT INTO `c_user` VALUES (27, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038412, 1608038412, NULL);
INSERT INTO `c_user` VALUES (28, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038412, 1608038412, NULL);
INSERT INTO `c_user` VALUES (29, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038412, 1608038412, NULL);
INSERT INTO `c_user` VALUES (30, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (31, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (32, NULL, 0, 20, 10005, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (33, NULL, 0, 20, 10006, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (34, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (35, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (36, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038494, 1608038494, NULL);
INSERT INTO `c_user` VALUES (37, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (38, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (39, NULL, 0, 20, 10005, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (40, NULL, 0, 20, 10006, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (41, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (42, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (43, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038505, 1608038505, NULL);
INSERT INTO `c_user` VALUES (44, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (45, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (46, NULL, 0, 20, 10005, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (47, NULL, 0, 20, 10006, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (48, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (49, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (50, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038569, 1608038569, NULL);
INSERT INTO `c_user` VALUES (51, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (52, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (53, NULL, 0, 20, 10005, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (54, NULL, 0, 20, 10006, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (55, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (56, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (57, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038597, 1608038597, NULL);
INSERT INTO `c_user` VALUES (58, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (59, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (60, NULL, 0, 20, 10005, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (61, NULL, 0, 20, 10006, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (62, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (63, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (64, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038608, 1608038608, NULL);
INSERT INTO `c_user` VALUES (65, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (66, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (67, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 0, '21', NULL, 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (68, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (69, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (70, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (71, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038626, 1608038626, NULL);
INSERT INTO `c_user` VALUES (72, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (73, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (74, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (75, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (76, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (77, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (78, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2020-12-16', 0, NULL, 1608038637, 1608038637, NULL);
INSERT INTO `c_user` VALUES (79, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (80, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (81, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (82, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (83, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (84, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (85, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (86, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (87, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (88, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, NULL, 1615457335, 1615457335, NULL);
INSERT INTO `c_user` VALUES (89, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-12', 0, 1615522675, 1615457335, 1615545062, NULL);
INSERT INTO `c_user` VALUES (90, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (91, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (92, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (93, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (94, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (95, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (96, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (97, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (98, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (99, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, NULL, 1615522866, 1615522866, NULL);
INSERT INTO `c_user` VALUES (100, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-13', 0, 1615523042, 1615522866, 1615545063, NULL);
INSERT INTO `c_user` VALUES (101, NULL, 0, 20, 10001, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (102, NULL, 0, 20, 10012, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (103, NULL, 0, 20, 10003, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (104, NULL, 0, 20, 10005, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (105, NULL, 0, 20, 10006, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (106, NULL, 0, 20, 10008, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (107, NULL, 0, 20, 10009, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (108, NULL, 0, 20, 10010, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (109, NULL, 0, 20, 10011, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (110, NULL, 0, 20, 10013, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);
INSERT INTO `c_user` VALUES (111, NULL, 0, 20, 10014, 0, NULL, '立减优惠券名', 10, 23, NULL, 2, '21', '2021-03-23', 0, NULL, 1616378190, 1616378190, NULL);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0,
  `mch_id` int(11) NULL DEFAULT 0,
  `uid` int(11) NULL DEFAULT NULL,
  `cond_id` int(11) NULL DEFAULT NULL COMMENT '针对评论对象',
  `sub_cond_id` int(11) NULL DEFAULT 0 COMMENT '针对评论子对象',
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `level` tinyint(4) NULL DEFAULT 5 COMMENT '评星',
  `cond_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sku信息',
  `reply_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `reply_time` int(11) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gid`(`sub_cond_id`, `uid`, `level`) USING BTREE,
  INDEX `mch_id`(`mch_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (20, 0, 1, 10001, NULL, 1, '/uploads/image/20200908/fc3967ac3edbe58383c7ce967af4176d.jpg,/uploads/image/20200908/980a0caa64674214f6db3e7cda7ab2cf.png', 'test', 2, '蓝色,XL,test2', NULL, NULL, 1, 1599534761, 1604976447, NULL);
INSERT INTO `comment` VALUES (21, 0, 1, 10001, NULL, 1, '', '味道不错', 4, '1斤', NULL, NULL, 1, 1599534860, 1604976461, NULL);
INSERT INTO `comment` VALUES (19, 0, 2, 10003, NULL, 1, '', 'test', 2, '蓝色,XL,test2', NULL, NULL, 0, 1599534590, 1604976520, NULL);
INSERT INTO `comment` VALUES (22, 0, 0, NULL, 2867, 2698, '', '我是评论内容', 5, '属性2,属性22', NULL, NULL, 1, 1608720413, 1608720413, NULL);
INSERT INTO `comment` VALUES (23, 0, 0, 10014, 1, 1, 'http://tp6.company-admin.com/uploads/goods_comment/20210311/cf103cd9676ae29e369e39122a38b744.png', '1', 1, NULL, NULL, NULL, 1, 1615432022, 1615432022, NULL);
INSERT INTO `comment` VALUES (24, 0, 0, 10014, 1, 1, 'http://tp6.company-admin.com/uploads/goods_comment/20210311/cf103cd9676ae29e369e39122a38b744.png', '1', 1, NULL, NULL, NULL, 1, 1615432030, 1615432030, NULL);
INSERT INTO `comment` VALUES (25, 0, 0, 10014, 2902, 67, 'http://tp6.company-admin.com/uploads/goods_comment/20210311/cf103cd9676ae29e369e39122a38b744.png', '123123123', 5, NULL, NULL, NULL, 1, 1615432177, 1615432177, NULL);

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0 COMMENT '类型',
  `mch_id` int(11) NULL DEFAULT 0 COMMENT '商家id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '券名',
  `money` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '优惠金额',
  `full_money` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '满立减金额',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `num` smallint(6) NULL DEFAULT 0 COMMENT '优惠券张数',
  `use_num` smallint(6) NULL DEFAULT 0 COMMENT '已领取张数',
  `award_times` int(11) NULL DEFAULT 0 COMMENT '抽奖 次数',
  `expire_day` tinyint(4) NULL DEFAULT 0 COMMENT '有效天数',
  `status` tinyint(4) NULL DEFAULT 0,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详细内容',
  `sort` smallint(5) UNSIGNED NULL DEFAULT 100,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES (20, 0, 0, NULL, '立减优惠券名', 10.00, 23.00, NULL, 0, 0, 0, 1, 1, '21', 100, 1608035060, 1616378167, NULL);
INSERT INTO `coupon` VALUES (21, 0, 0, NULL, '2元优惠券', 2.00, 0.00, NULL, 0, 0, 0, 0, 2, '', 100, 1608036411, 1608036465, 1608036465);
INSERT INTO `coupon` VALUES (22, 0, 0, NULL, 'test', 1.00, 3.00, NULL, 0, 0, 0, 2, 2, '3', 100, 1608038650, 1608038656, 1608038656);
INSERT INTO `coupon` VALUES (23, 0, 0, NULL, 'test', 122.00, 12.00, NULL, 0, 0, 0, 1, 1, '说明说明说明说明说明说明', 100, 1616378180, 1616378196, 1616378196);

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mch_id` int(11) NULL DEFAULT 0 COMMENT '商家id',
  `type` tinyint(4) NULL DEFAULT 0 COMMENT '类型',
  `bid` int(11) NULL DEFAULT NULL COMMENT '品牌id',
  `cid` int(11) NULL DEFAULT 0 COMMENT '栏目',
  `ct_id` int(11) NULL DEFAULT 0 COMMENT '二级分类id',
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sub_name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品图',
  `commission_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '佣金',
  `og_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '原价',
  `price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '价格',
  `widget` int(11) NULL DEFAULT NULL COMMENT '重量（g）',
  `volume` decimal(10, 4) NULL DEFAULT NULL COMMENT '体积（立方）',
  `spu` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '规格参数',
  `tag` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `freight_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '运费',
  `tax_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '税费',
  `ser_intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '服务说明',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详细资料',
  `stock` int(11) NULL DEFAULT 0 COMMENT '库存',
  `views` int(11) NULL DEFAULT 0 COMMENT '浏览次数',
  `sold_num` smallint(6) NULL DEFAULT 0 COMMENT '销量',
  `sku_id` int(11) NULL DEFAULT 0 COMMENT '对应sku_id',
  `sku` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'sku',
  `is_today` tinyint(4) NULL DEFAULT 0 COMMENT '今日特价',
  `is_jx` tinyint(4) NULL DEFAULT 0 COMMENT '精选',
  `is_new` tinyint(4) NULL DEFAULT NULL COMMENT '新品',
  `status` tinyint(4) NULL DEFAULT 1,
  `mch_status` tinyint(4) NULL DEFAULT 1 COMMENT '平台状态',
  `sort` smallint(4) NULL DEFAULT 100,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 74 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (53, 0, 0, 52, 51, 0, '美容产品名1', NULL, 'http://tp6.meiyuan.com/uploads/goods/20201016/60c74e0ec9acd4793402e3d2e9f342a8.jpg', 3.00, 2.00, 10.00, NULL, NULL, '[{\"name\":\"\\u638c\\u67dc\\u63cf\\u8ff0\",\"value\":\"1\"},{\"name\":\"\\u4e3b\\u6599\",\"value\":\"2\"}]', '标签', 1.00, 2.00, NULL, '简介', '<p>123456798</p>', 6, 0, 100, 597, NULL, 0, 0, 1, 1, 1, 100, 1602836921, 1607608377, 1607608377);
INSERT INTO `goods` VALUES (54, 0, 0, 52, 51, 52, '美容产品- -我是产品名我是产品名我是产品名我是产品名12我是产品名我是产品名我是产品名我是产品名12我是产品名我是产品名我是产品名我是产品名12', NULL, 'http://tp6.meiyuan.com/uploads/goods/20201016/60c74e0ec9acd4793402e3d2e9f342a8.jpg', 1.00, 2.00, 3.00, NULL, NULL, '[{\"name\":\"\\u638c\\u67dc\\u63cf\\u8ff0\",\"value\":\"1\"},{\"name\":\"\\u4e3b\\u6599\",\"value\":\"2\"}]', '标签', 5.00, 0.00, NULL, '简介', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\">详情</span></p>', 6, 0, 100, 0, NULL, 1, 0, 1, 1, 1, 100, 1602837055, 1607608348, 1607608348);
INSERT INTO `goods` VALUES (55, 0, 0, 0, 51, 0, '我是产品名1-有佣金', NULL, 'http://tp6.meiyuan.com/uploads/goods/20201016/60c74e0ec9acd4793402e3d2e9f342a8.jpg', 3.00, 2.00, 6.00, NULL, NULL, '[{\"name\":\"\\u638c\\u67dc\\u63cf\\u8ff0\",\"value\":\"1\"},{\"name\":\"\\u4e3b\\u6599\",\"value\":\"2\"}]', '标签', 5.00, 0.00, NULL, '简介', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\">详情</span></p>', 6, 0, 100, 0, NULL, 0, 0, 1, 1, 1, 100, 1602837065, 1607608324, 1607608324);
INSERT INTO `goods` VALUES (56, 0, 0, 51, 51, 52, '我是产品名我是产品名我是产品名我是产品名我是产品名我是产品名我是产品名我是产品名1123', NULL, 'http://tp6.meiyuan.com/uploads/goods/20201016/60c74e0ec9acd4793402e3d2e9f342a8.jpg', 1.00, 2.00, 3.00, NULL, NULL, '[{\"name\":\"\\u638c\\u67dc\\u63cf\\u8ff0\",\"value\":\"1\"},{\"name\":\"\\u4e3b\\u6599\",\"value\":\"2\"}]', '标签', 5.00, 0.00, NULL, '简介', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, Tahoma, Arial, sans-serif; font-size: 14px; font-weight: 700; text-align: right; background-color: rgb(255, 255, 255);\">详情</span></p>', 6, 0, 100, 555, NULL, 0, 0, 1, 1, 1, 100, 1602837095, 1607608402, 1607608402);
INSERT INTO `goods` VALUES (57, 0, 0, NULL, 20, 21, '华为 HUAWEI P40 Pro+ 麒麟990 5G SoC芯片 5000万超感知徕卡五摄 100倍双目变焦 8GB+256GB陶瓷黑全网通5G', '', 'http://192.168.5.117/uploads/goods/20201225/ab59a26fc14afbe7cee59e0b0609ec66.jpg', NULL, 100.00, 99.99, NULL, NULL, '[]', NULL, 1.00, 0.00, NULL, '我是说明1', '<p>我是内容2</p>', 1, 0, 0, 0, '[{\"name\":\"\\u89c4\\u683c\\u540d\",\"content\":\"\\u5c5e\\u60271,\\u5c5e\\u60272\"}]', 0, 0, NULL, 1, 1, 1, 1607588927, 1609317331, NULL);
INSERT INTO `goods` VALUES (69, 0, 0, NULL, 20, 21, 'HUAWEI WATCH GT2 华为手表 运动智能手表 两周长续航蓝牙通话血氧检测麒麟芯片 华为gt2 46mm 曜石黑', NULL, 'http://192.168.5.113/uploads/goods/20201219/56edb26d98c747e00b3642f66f96579b.jpg', NULL, 99.99, 99.99, NULL, NULL, NULL, NULL, 8.88, 0.00, NULL, 'ie添加的数据-说明', '<p>ie添加的数据<br></p>', 134, 0, 100, 0, '[{\"name\":\"\\u89c4\\u683c\\u540d\",\"content\":\"\\u5c5e\\u60271,\\u5c5e\\u60272,\\u5c5e\\u60273\"},{\"name\":\"\\u89c4\\u683c\\u540d2\",\"content\":\"\\u5c5e22,\\u5c5e\\u602732\"}]', 0, 0, NULL, 1, 1, 100, 1607608196, 1608360772, NULL);
INSERT INTO `goods` VALUES (68, 0, 0, NULL, 20, 21, '华为 HUAWEI P40 Pro+ 麒麟990 5G SoC芯片 5000万超感知徕卡五摄 100倍双目变焦 8GB+256GB陶瓷白全网通5G', '', 'http://192.168.5.117/uploads/goods/20201225/6be8732e5a877cf0d080a3c6d72ba5cf.png,http://192.168.5.117/uploads/goods/20201225/37b10ddf655ffb8e61061bf3d55d5649.png,http://192.168.5.117/uploads/goods/20201225/8987c75686e8e4cc889b0d3dec12632b.png', NULL, 99.99, 88.88, NULL, NULL, NULL, NULL, 1.00, 0.00, NULL, '说明\n', '<p><label>内容</label></p><p><label>内容</label></p><p><label>内容</label></p><p>123</p>', 99, 0, 99, 0, '[{\"name\":\"\\u89c4\\u683c\\u540d\",\"content\":\"\\u5c5e\\u60271\"}]', 0, 0, NULL, 1, 1, 1, 1607605389, 1608865626, NULL);
INSERT INTO `goods` VALUES (67, 0, 0, NULL, 23, 24, '产品名称', '', 'http://tp6.company-admin.com/uploads/goods/20201210/5cd01dceb6f65b549f55be09efb90154.jpg,http://tp6.company-admin.com/uploads/goods/20201210/e02946d3bc4e930b6f29e30bceb87ba4.png,http://tp6.company-admin.com/uploads/goods/20201210/8250fce462f4fa7518d40680d9c375dd.png', NULL, 99.99, 88.88, NULL, NULL, '[{\"name\":\"1\",\"value\":\"2\"},{\"name\":\"3\",\"value\":\"4\"}]', NULL, 3.00, 0.00, NULL, '说明\n', '<p><br></p><p><label><img src=\"http://tp6.company-admin.com/uploads/goods/20201229/03d99b94462935111380780b7a0be44e.png\" style=\"max-width: 100%;\"><img src=\"http://tp6.company-admin.com/uploads/goods/20201229/157dc82a11b14f6358453ef4a4d6c9a8.jpg\" style=\"max-width: 100%;\"></label></p>', 99, 0, 99, 0, '[{\"name\":\"\\u89c4\\u683c\\u540d1\",\"content\":\"\\u5c5e\\u60271,\\u5c5e\\u60272\"},{\"name\":\"\\u89c4\\u683c\\u540d2\",\"content\":\"\\u5c5e\\u602721,\\u5c5e\\u602722\"}]', 0, 0, NULL, 1, 1, 1, 1607605357, 1609316144, NULL);
INSERT INTO `goods` VALUES (66, 0, 0, NULL, 23, 24, '产品名称555', NULL, 'http://tp6.company-admin.com/uploads/goods/20201210/5cd01dceb6f65b549f55be09efb90154.jpg,http://tp6.company-admin.com/uploads/goods/20201210/e02946d3bc4e930b6f29e30bceb87ba4.png,http://tp6.company-admin.com/uploads/goods/20201210/8250fce462f4fa7518d40680d9c375dd.png', NULL, 99.99, 88.88, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, '说明\n', '<p><label>内容</label></p><p><label>内容</label></p><p><label>内容</label></p>', 99, 0, 99, 0, '[{\"name\":\"\\u89c4\\u683c\\u540d\",\"content\":\"\\u5c5e\\u60271,\\u5c5e\\u60272\"},{\"name\":\"\\u89c4\\u683c\\u540d2\",\"content\":\"\\u5c5e\\u602721,\\u5c5e\\u602722\"}]', 0, 0, NULL, 1, 1, 1, 1607605242, 1608360723, NULL);
INSERT INTO `goods` VALUES (70, 0, 0, NULL, 20, 21, '华为商品名', '副标题', 'http://tp6.company-admin.com/uploads/goods/20201221/d4ef7ff6a50b876e6969849714703bff.jpg', NULL, 99.99, 99.99, NULL, NULL, NULL, NULL, 1.00, 0.00, NULL, '说明\n', '<p><label>内容</label></p>', 2, 0, 3, 0, 'null', 0, 0, NULL, 1, 1, 4, 1608523505, 1609317803, NULL);
INSERT INTO `goods` VALUES (71, 0, 0, NULL, 20, 21, '华为商品名', '副标题', 'http://tp6.company-admin.com/uploads/goods/20201221/d4ef7ff6a50b876e6969849714703bff.jpg', NULL, 99.99, 99.99, NULL, NULL, NULL, NULL, 1.00, 0.00, NULL, '说明\n', '<p><label>内容</label></p>', 2, 0, 3, 0, 'null', 0, 0, NULL, 2, 1, 4, 1608523537, 1608523537, NULL);
INSERT INTO `goods` VALUES (72, 0, 0, NULL, 20, 21, '华为商品名', '副标题', 'http://tp6.company-admin.com/uploads/goods/20201221/d4ef7ff6a50b876e6969849714703bff.jpg', NULL, 99.99, 99.99, NULL, NULL, NULL, NULL, 1.00, 0.00, NULL, '说明\n', '<p><label>内容</label></p>', 2, 0, 3, 0, 'null', 0, 0, NULL, 2, 1, 4, 1608523578, 1608523843, NULL);
INSERT INTO `goods` VALUES (73, 0, 0, NULL, 23, 24, '产品名称555', NULL, 'http://tp6.company-admin.com/uploads/goods/20201210/5cd01dceb6f65b549f55be09efb90154.jpg,http://tp6.company-admin.com/uploads/goods/20201210/e02946d3bc4e930b6f29e30bceb87ba4.png,http://tp6.company-admin.com/uploads/goods/20201210/8250fce462f4fa7518d40680d9c375dd.png', NULL, 99.99, 88.88, NULL, NULL, 'null', NULL, 0.00, 0.00, NULL, '说明\n', '<p><label>内容</label></p><p><label>内容</label></p><p><label>内容</label></p>', 99, 0, 99, 0, '[{\"name\":\"\\u89c4\\u683c\\u540d\",\"content\":\"\\u5c5e\\u60271,\\u5c5e\\u60272\"},{\"name\":\"\\u89c4\\u683c\\u540d2\",\"content\":\"\\u5c5e\\u602721,\\u5c5e\\u602722\"}]', 0, 0, NULL, 1, 1, 1, 1607605242, 1608360723, NULL);

-- ----------------------------
-- Table structure for goods_cate
-- ----------------------------
DROP TABLE IF EXISTS `goods_cate`;
CREATE TABLE `goods_cate`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT 0,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名',
  `sub_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` tinyint(4) NULL DEFAULT 100,
  `status` tinyint(4) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `sort`(`sort`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_cate
-- ----------------------------
INSERT INTO `goods_cate` VALUES (21, 20, '华为', NULL, 'http://192.168.5.113/uploads/goods_cate/20201219/b5b25e1c9d018db90e86bb6afa2ab4a1.jpg', NULL, 2, 1, 1607581438, 1608362861, NULL);
INSERT INTO `goods_cate` VALUES (20, 0, '手机数码', NULL, '', NULL, 1, 1, 1607581404, 1608359869, NULL);
INSERT INTO `goods_cate` VALUES (22, 20, 'test', NULL, '', NULL, 100, 1, 1607581551, 1607581555, 1607581555);
INSERT INTO `goods_cate` VALUES (23, 0, '电脑办公', NULL, '', NULL, 100, 1, 1608259663, 1608359901, NULL);
INSERT INTO `goods_cate` VALUES (24, 23, '游戏本', NULL, 'http://192.168.5.113/uploads/goods_cate/20201219/285cf6f21e12577bf761b9c041ec3de7.png', NULL, 100, 1, 1608359922, 1608363061, NULL);
INSERT INTO `goods_cate` VALUES (25, 0, '钟表珠宝', NULL, '', NULL, 100, 1, 1608360031, 1608360031, NULL);
INSERT INTO `goods_cate` VALUES (26, 0, '美妆护肤', NULL, '', NULL, 100, 1, 1608360088, 1608360088, NULL);
INSERT INTO `goods_cate` VALUES (27, 20, '小米', NULL, 'http://192.168.5.113/uploads/goods_cate/20201219/e001f2d4a60761e4e9104930ac89e750.png', NULL, 100, 1, 1608362872, 1608362872, NULL);
INSERT INTO `goods_cate` VALUES (28, 20, 'iPhone', NULL, 'http://192.168.5.113/uploads/goods_cate/20201219/5c23f2795214e0f73f7399f07161465a.jpg', NULL, 100, 1, 1608362892, 1608362892, NULL);

-- ----------------------------
-- Table structure for goods_kill
-- ----------------------------
DROP TABLE IF EXISTS `goods_kill`;
CREATE TABLE `goods_kill`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `at` tinyint(4) NULL DEFAULT 0 COMMENT '开始时间段',
  `gid` int(11) NULL DEFAULT NULL,
  `sku_id` int(11) NULL DEFAULT NULL,
  `stock` int(10) UNSIGNED NULL DEFAULT 0,
  `price` decimal(10, 2) NULL DEFAULT 0.00,
  `is_open` tinyint(4) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `at`(`at`, `gid`, `sku_id`) USING BTREE,
  INDEX `gid`(`gid`, `sku_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of goods_kill
-- ----------------------------
INSERT INTO `goods_kill` VALUES (1, 1, 67, 41, 100, 1.00, 1, 1615458490, 1615458490);
INSERT INTO `goods_kill` VALUES (2, 1, 67, 42, 55, 2.00, 1, 1615458490, 1615458490);
INSERT INTO `goods_kill` VALUES (3, 1, 67, 43, 11, 3.00, 1, 1615458490, 1615458490);
INSERT INTO `goods_kill` VALUES (4, 1, 67, 44, 87, 4.00, 1, 1615458490, 1615458490);
INSERT INTO `goods_kill` VALUES (8, 3, 69, 32, 44, 0.08, 1, 1615515844, 1615515844);
INSERT INTO `goods_kill` VALUES (7, 3, 68, 38, 33, 18.00, 1, 1615515164, 1615515164);
INSERT INTO `goods_kill` VALUES (9, 3, 69, 33, 44, 0.07, 1, 1615515844, 1615515844);
INSERT INTO `goods_kill` VALUES (10, 3, 69, 34, 44, 0.06, 1, 1615515844, 1615515844);
INSERT INTO `goods_kill` VALUES (11, 3, 69, 35, 44, 0.05, 1, 1615515844, 1615515844);
INSERT INTO `goods_kill` VALUES (12, 3, 69, 36, 44, 0.04, 1, 1615515844, 1615515844);
INSERT INTO `goods_kill` VALUES (13, 3, 69, 37, 44, 0.03, 1, 1615515844, 1615515844);
INSERT INTO `goods_kill` VALUES (14, 2, 69, 32, 44, 0.08, 1, 1615515858, 1615515858);
INSERT INTO `goods_kill` VALUES (15, 2, 69, 33, 44, 0.07, 1, 1615515858, 1615515858);
INSERT INTO `goods_kill` VALUES (16, 2, 69, 34, 44, 0.06, 1, 1615515858, 1615515858);
INSERT INTO `goods_kill` VALUES (17, 2, 69, 35, 44, 0.05, 1, 1615515858, 1615515858);
INSERT INTO `goods_kill` VALUES (18, 2, 69, 36, 44, 0.04, 1, 1615515858, 1615515858);
INSERT INTO `goods_kill` VALUES (19, 2, 69, 37, 44, 0.03, 1, 1615515858, 1615515858);

-- ----------------------------
-- Table structure for goods_sku_mix_price
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_mix_price`;
CREATE TABLE `goods_sku_mix_price`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gid` int(10) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT 0.00,
  `stock` int(11) NULL DEFAULT 0,
  `status` tinyint(4) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gid`(`gid`) USING BTREE,
  INDEX `price`(`price`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_sku_mix_price
-- ----------------------------
INSERT INTO `goods_sku_mix_price` VALUES (33, 69, '属性1,属性32', 2.00, 44, 0);
INSERT INTO `goods_sku_mix_price` VALUES (32, 69, '属性1,属22', 1.00, 44, 1);
INSERT INTO `goods_sku_mix_price` VALUES (31, 66, '属性2,属性22', 4.00, 33, 0);
INSERT INTO `goods_sku_mix_price` VALUES (30, 66, '属性2,属性21', 3.00, 3, 1);
INSERT INTO `goods_sku_mix_price` VALUES (29, 66, '属性1,属性22', 2.00, 2, 0);
INSERT INTO `goods_sku_mix_price` VALUES (28, 66, '属性1,属性21', 1.00, 1, 1);
INSERT INTO `goods_sku_mix_price` VALUES (40, 57, '属性2', 1.00, 2, 0);
INSERT INTO `goods_sku_mix_price` VALUES (39, 57, '属性1', 1.00, 2, 1);
INSERT INTO `goods_sku_mix_price` VALUES (37, 69, '属性3,属性32', 9.00, 44, 1);
INSERT INTO `goods_sku_mix_price` VALUES (38, 68, '属性1', 22.00, 33, 1);
INSERT INTO `goods_sku_mix_price` VALUES (36, 69, '属性3,属22', 5.00, 44, 1);
INSERT INTO `goods_sku_mix_price` VALUES (35, 69, '属性2,属性32', 4.00, 44, 0);
INSERT INTO `goods_sku_mix_price` VALUES (34, 69, '属性2,属22', 3.00, 44, 0);
INSERT INTO `goods_sku_mix_price` VALUES (41, 67, '属性1,属性21', 1.00, 100, 1);
INSERT INTO `goods_sku_mix_price` VALUES (42, 67, '属性1,属性22', 2.00, 55, 1);
INSERT INTO `goods_sku_mix_price` VALUES (43, 67, '属性2,属性21', 3.00, 89, 1);
INSERT INTO `goods_sku_mix_price` VALUES (44, 67, '属性2,属性22', 4.00, 87, 1);
INSERT INTO `goods_sku_mix_price` VALUES (45, 72, 'a3', 4.00, 0, 0);
INSERT INTO `goods_sku_mix_price` VALUES (46, 72, 'b5', 3.00, 0, 1);
INSERT INTO `goods_sku_mix_price` VALUES (47, 72, 'ddd', 2.00, 0, 0);
INSERT INTO `goods_sku_mix_price` VALUES (48, 73, '属性2,属性22', 4.00, 33, 0);
INSERT INTO `goods_sku_mix_price` VALUES (49, 73, '属性2,属性21', 3.00, 3, 1);
INSERT INTO `goods_sku_mix_price` VALUES (50, 73, '属性1,属性22', 2.00, 2, 0);
INSERT INTO `goods_sku_mix_price` VALUES (51, 73, '属性1,属性21', 1.00, 1, 1);

-- ----------------------------
-- Table structure for goods_sku_price
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_price`;
CREATE TABLE `goods_sku_price`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gid` int(10) UNSIGNED NULL DEFAULT NULL,
  `group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '每个组合索引',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sku组合属性',
  `price` decimal(10, 2) NULL DEFAULT 0.00,
  `stock` int(11) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gid`(`gid`) USING BTREE,
  INDEX `price`(`price`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_sku_price
-- ----------------------------
INSERT INTO `goods_sku_price` VALUES (33, 69, NULL, '属性1,属性32', 2.00, 44, 0);
INSERT INTO `goods_sku_price` VALUES (32, 69, NULL, '属性1,属22', 1.00, 44, 1);
INSERT INTO `goods_sku_price` VALUES (31, 66, NULL, '属性2,属性22', 4.00, 33, 0);
INSERT INTO `goods_sku_price` VALUES (30, 66, NULL, '属性2,属性21', 3.00, 3, 1);
INSERT INTO `goods_sku_price` VALUES (29, 66, NULL, '属性1,属性22', 2.00, 2, 0);
INSERT INTO `goods_sku_price` VALUES (28, 66, NULL, '属性1,属性21', 1.00, 1, 1);
INSERT INTO `goods_sku_price` VALUES (40, 57, NULL, '属性2', 2.00, 2, 0);
INSERT INTO `goods_sku_price` VALUES (39, 57, NULL, '属性1', 1.00, 2, 1);
INSERT INTO `goods_sku_price` VALUES (37, 69, NULL, '属性3,属性32', 9.00, 44, 1);
INSERT INTO `goods_sku_price` VALUES (38, 68, NULL, '属性1', 22.00, 33, 1);
INSERT INTO `goods_sku_price` VALUES (36, 69, NULL, '属性3,属22', 5.00, 44, 1);
INSERT INTO `goods_sku_price` VALUES (35, 69, NULL, '属性2,属性32', 4.00, 44, 0);
INSERT INTO `goods_sku_price` VALUES (34, 69, NULL, '属性2,属22', 3.00, 44, 0);
INSERT INTO `goods_sku_price` VALUES (41, 67, NULL, '属性1,属性21', 1.00, 100, 1);
INSERT INTO `goods_sku_price` VALUES (42, 67, NULL, '属性1,属性22', 2.00, 55, 1);
INSERT INTO `goods_sku_price` VALUES (43, 67, NULL, '属性2,属性21', 3.00, 0, 1);
INSERT INTO `goods_sku_price` VALUES (44, 67, NULL, '属性2,属性22', 4.00, 87, 1);
INSERT INTO `goods_sku_price` VALUES (45, 73, NULL, '属性2,属性22', 4.00, 33, 0);
INSERT INTO `goods_sku_price` VALUES (46, 73, NULL, '属性2,属性21', 3.00, 3, 1);
INSERT INTO `goods_sku_price` VALUES (47, 73, NULL, '属性1,属性22', 2.00, 2, 0);
INSERT INTO `goods_sku_price` VALUES (48, 73, NULL, '属性1,属性21', 1.00, 1, 1);

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0,
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` tinyint(4) NULL DEFAULT 1,
  `sort` tinyint(4) NULL DEFAULT 100,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES (1, 0, '/uploads/follow_img/20200519/0b638a4100fb0c7bd9aa0ed863028f91.jpg', 'test123', 'http://www.baidu.com', 2, 100, 1589880030, 1589956358, 1589956358);
INSERT INTO `image` VALUES (2, 0, '/uploads/follow_img/20200818/f965dedfce1054ec32363cf7caef1943.jpg', '营养餐', '/pages/goods/goods_detail?id=57', 1, 2, 1589956385, 1605084340, 1605084340);
INSERT INTO `image` VALUES (3, 0, '/uploads/follow_img/20200807/38e9ba9d9464c0e4395bf74b13d33d07.jpg', '康陆福酒', '/pages/goods/goods_detail?id=53', 1, 100, 1589956394, 1605084345, 1605084345);
INSERT INTO `image` VALUES (4, 1, '/uploads/follow_img/20200707/c9df8af96557da53be40fe1e6996d098.png', '1', '/pages/login/login', 2, 100, 1589968961, 1608273190, 1608273190);
INSERT INTO `image` VALUES (5, 1, '/uploads/follow_img/20200520/2ebe83e4dc5ec0fce7efb7d6b5cea3b5.jpg', '2', '', 1, 100, 1589968976, 1591088065, 1591088065);
INSERT INTO `image` VALUES (6, 1, '/uploads/follow_img/20200520/df3c1282c63ef94d87f143286caf9100.jpg', '3', '', 1, 100, 1589968987, 1591088067, 1591088067);
INSERT INTO `image` VALUES (7, 2, '/uploads/follow_img/20200618/76c5235309fe577919b2ce828af0f227.jpg', '', '', 1, 100, 1590229793, 1592477280, NULL);
INSERT INTO `image` VALUES (8, 2, '/uploads/follow_img/20200523/3addb11b19580c72d50e127eb52665f0.jpg', '', '', 1, 100, 1590229802, 1592477125, 1592477125);
INSERT INTO `image` VALUES (9, 2, '/uploads/follow_img/20200523/1346a90a21232ec23656d44caed70c8d.jpg', '', '', 1, 100, 1590230891, 1592477128, 1592477128);
INSERT INTO `image` VALUES (10, 0, '/uploads/follow_img/20200602/5950a351eea530fa62f6dfd1a93214e6.jpg', '', '', 1, 100, 1591087997, 1596696607, 1596696607);
INSERT INTO `image` VALUES (11, 2, '/uploads/follow_img/20200618/3391b7679e281c7c79f44ee44853aa8d.jpg', '1', '', 1, 100, 1592477384, 1592477384, NULL);
INSERT INTO `image` VALUES (12, 0, '/uploads/follow_img/20200807/bb08c24bcdace695d9db461de0d566de.jpg', 'f福缘茶', '/pages/goods/goods_detail?id=58', 1, 2, 1596770843, 1605084343, 1605084343);
INSERT INTO `image` VALUES (13, 0, '/uploads/follow_img/20200817/749c412208c99f9214786e9d2efbef98.jpg', '膳食餐', '/pages/goods/goods_detail?id=55', 1, 100, 1596770899, 1605084348, 1605084348);
INSERT INTO `image` VALUES (14, 0, '/uploads/follow_img/20200807/9ff05406c852760c978386e4a3afd2f3.jpg', '营养膳食餐', '', 2, 100, 1596770921, 1600673519, 1600673519);
INSERT INTO `image` VALUES (15, 0, '/uploads/follow_img/20200815/3ff690b25b65b7c210a7a7366864c2c4.jpg', '营养餐活动', '', 1, 100, 1597495549, 1597495592, 1597495592);
INSERT INTO `image` VALUES (16, 0, '/uploads/follow_img/20200815/c5249a1766c0021e92a7539d690dcd74.jpg', '巽寮湾旅游', '', 1, 100, 1597495565, 1597495766, 1597495766);
INSERT INTO `image` VALUES (17, 0, '/uploads/follow_img/20200818/c5e49531fec1e26dc20021f8e0015bbc.png', '巽寮湾旅游', '', 1, 2, 1597495742, 1599032449, 1599032449);
INSERT INTO `image` VALUES (18, 0, '/follow_img/20200921/FmPNCUhE6d2fm-LOAw-hvtwE3BSz.png', '康陆福月亮', '	/pages/goods/goods_detail?id=53', 1, 2, 1597715142, 1602243984, 1602243984);
INSERT INTO `image` VALUES (19, 0, '/uploads/follow_img/20200828/38752f5d8f2a0fb829d94f9a44cbfe16.jpg', '宝贝早餐', '/pages/goods/goods_detail?id=57', 1, 2, 1598576021, 1602243988, 1602243988);
INSERT INTO `image` VALUES (20, 0, '/follow_img/20200930/Fi6UgPmrA3bpeqfdxQNNdwdqO_hm.jpg', '国庆', '', 1, 1, 1601454745, 1602243979, 1602243979);
INSERT INTO `image` VALUES (21, 0, '/follow_img/20201027/FqFHx18NHLRs8alaz-v8_m1dGerq.jpg', '福缘茶上市', '/pages/goods/goods_detail?id=58', 1, 1, 1603760981, 1605084338, 1605084338);
INSERT INTO `image` VALUES (22, 0, '/follow_img/20201102/FrCCs7Ep_QwcjhYXAeq9C9MBoJVZ.jpg', '', '/pages/goods/goods_detail?id=52', 1, 100, 1604304581, 1605084350, 1605084350);
INSERT INTO `image` VALUES (23, 0, 'http://tp6.ry2.com/uploads/follow_img/20201111/52c8d4a1d04a4a29a714bf4337646c1d.png', '12', 'http://www.baidu.com', 1, 100, 1605084358, 1609233554, NULL);
INSERT INTO `image` VALUES (24, 1, 'http://tp6.company-admin.com/uploads/image/20201218/5decc966c8900fac7b51e6aaf16fb3af.jpg', '图片名称3', 'url2', 1, 7, 1608272990, 1608280913, 1608280913);
INSERT INTO `image` VALUES (25, 1, 'http://tp6.company-admin.com/uploads/image/20201218/57ce6a3b97065800f10fd07bcf901eaa.jpg', '01', '', 1, 1, 1608280934, 1608280934, NULL);
INSERT INTO `image` VALUES (26, 1, 'http://tp6.company-admin.com/uploads/image/20201218/fd32eb614971dbbb5066f84754d3f36c.jpg', '02', '', 1, 2, 1608280949, 1608280949, NULL);
INSERT INTO `image` VALUES (27, 1, 'http://tp6.company-admin.com/uploads/image/20201218/2196bfdd000f3d238578f053b4ad1749.jpg', '03', '', 1, 3, 1608280961, 1608280961, NULL);

-- ----------------------------
-- Table structure for o_addr
-- ----------------------------
DROP TABLE IF EXISTS `o_addr`;
CREATE TABLE `o_addr`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` int(11) NULL DEFAULT NULL,
  `phone` bigint(20) NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `addr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `addr_extra` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lng_lat` point NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oid`(`oid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of o_addr
-- ----------------------------

-- ----------------------------
-- Table structure for o_back_follow
-- ----------------------------
DROP TABLE IF EXISTS `o_back_follow`;
CREATE TABLE `o_back_follow`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT 0,
  `m_uid` int(11) NULL DEFAULT 0 COMMENT '管理员id',
  `cond_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '订单产品id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` tinyint(4) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `oid`(`cond_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of o_back_follow
-- ----------------------------

-- ----------------------------
-- Table structure for o_goods
-- ----------------------------
DROP TABLE IF EXISTS `o_goods`;
CREATE TABLE `o_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mch_id` int(11) NULL DEFAULT 0 COMMENT '商户id',
  `share_uid` int(11) NULL DEFAULT 0 COMMENT '分销用户id',
  `uid` int(11) NULL DEFAULT 0,
  `oid` int(11) NULL DEFAULT NULL,
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `gid` int(11) NULL DEFAULT NULL,
  `sup_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '供应价',
  `price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品价格',
  `commission_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '产品佣金',
  `per` decimal(3, 2) NULL DEFAULT 0.00 COMMENT '折扣',
  `freight_money` decimal(10, 2) NULL DEFAULT 0.00,
  `taxation_money` decimal(10, 2) NULL DEFAULT 0.00,
  `sku_id` int(11) NULL DEFAULT 0,
  `sku_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sku_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `num` smallint(6) NULL DEFAULT 0 COMMENT '购买数量',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `is_kill` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '秒杀状态',
  `is_give` tinyint(4) NULL DEFAULT NULL COMMENT '赠品',
  `extra` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品额外数据',
  `is_pay` tinyint(4) NULL DEFAULT 0 COMMENT '是否支付',
  `is_receive` tinyint(4) NULL DEFAULT 0 COMMENT '收货状态',
  `receive_time` int(11) NULL DEFAULT NULL COMMENT '收货时间',
  `is_comment` tinyint(4) NULL DEFAULT 0 COMMENT '是否评论',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `create_time` int(11) NULL DEFAULT NULL,
  `comment_date` datetime(0) NULL DEFAULT NULL COMMENT '评论时间',
  `back_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '售后单号',
  `back_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '退款金额',
  `step_flow` tinyint(4) NULL DEFAULT 0 COMMENT '处理流程',
  `is_back` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '退货状态',
  `back_start_time` int(11) NULL DEFAULT NULL COMMENT '退货开始时间',
  `back_handle_time` int(11) NULL DEFAULT NULL COMMENT '退货进行中',
  `back_end_time` int(11) NULL DEFAULT NULL COMMENT '退货结束时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oid`(`oid`) USING BTREE,
  INDEX `gid`(`gid`) USING BTREE,
  INDEX `uid`(`uid`, `gid`) USING BTREE,
  INDEX `gid_2`(`gid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of o_goods
-- ----------------------------

-- ----------------------------
-- Table structure for o_logistics
-- ----------------------------
DROP TABLE IF EXISTS `o_logistics`;
CREATE TABLE `o_logistics`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` int(11) NULL DEFAULT NULL,
  `no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '运送单号',
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司名',
  `logistics_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司编号',
  `money` decimal(10, 2) NULL DEFAULT 0.00,
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '快递状态',
  `delete_time` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of o_logistics
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(255) NULL DEFAULT 0 COMMENT '订单类型',
  `no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hno` varchar(33) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '合单号 以h开头',
  `mch_id` int(11) NULL DEFAULT NULL COMMENT '商家id',
  `uid` int(11) NULL DEFAULT NULL,
  `money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '支付金额',
  `goods_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品价格',
  `dis_money` decimal(10, 2) NULL DEFAULT 0.00,
  `integral` int(11) NULL DEFAULT 0 COMMENT '使用的积分',
  `integral_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '积分抵扣金额',
  `mch_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商家获得金额',
  `pay_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '实际支付金额',
  `freight_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '运费',
  `full_freight_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '满减运费金额',
  `taxation_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '税费',
  `commission_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '佣金',
  `total_num` int(11) NULL DEFAULT 0 COMMENT '商品总数量',
  `coupon_id` int(11) NULL DEFAULT NULL COMMENT '优惠券',
  `coupon_dis_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠金额',
  `step_flow` tinyint(4) NULL DEFAULT 0 COMMENT '订单交易流程',
  `invoice` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '发票具体内容',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '创建订单',
  `rec_mode` tinyint(4) NULL DEFAULT 1 COMMENT '收货方式',
  `pay_way` tinyint(4) NULL DEFAULT 0 COMMENT '支付方式',
  `pay_origin` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付方式2',
  `pay_time` int(11) NULL DEFAULT NULL COMMENT '支付时间',
  `pay_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支付信息',
  `pay_way_open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付条件id',
  `cancel_time` int(11) NULL DEFAULT NULL COMMENT '订单取消时间',
  `complete_time` int(11) NULL DEFAULT NULL COMMENT '完成时间',
  `is_reminder` smallint(6) NULL DEFAULT 0 COMMENT '提醒状态',
  `reminder_time` int(11) NULL DEFAULT NULL COMMENT '提醒时间',
  `is_send` tinyint(4) NULL DEFAULT 0 COMMENT '是否配送',
  `send_start_time` int(11) NULL DEFAULT NULL COMMENT '发货-流程开始',
  `send_end_time` int(11) NULL DEFAULT NULL COMMENT '发货-流程结束',
  `is_receive` tinyint(4) NULL DEFAULT 0,
  `rec_time` int(11) NULL DEFAULT NULL COMMENT '收货时间',
  `cancel_back_handle_time` int(11) NULL DEFAULT 0 COMMENT '取消/删除  回原时间',
  `is_comment` tinyint(4) NULL DEFAULT 0 COMMENT '评论状态',
  `comment_time` int(11) NULL DEFAULT NULL COMMENT '评论时间',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `order_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '订单备注',
  `is_back` tinyint(4) NULL DEFAULT 0 COMMENT '售后',
  `is_back_money` tinyint(4) NULL DEFAULT NULL COMMENT '退款',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `no`(`no`) USING BTREE,
  INDEX `mch_id`(`mch_id`) USING BTREE,
  INDEX `step_flow`(`step_flow`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for platform_gift
-- ----------------------------
DROP TABLE IF EXISTS `platform_gift`;
CREATE TABLE `platform_gift`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mch_id` tinyint(3) UNSIGNED NULL DEFAULT 1,
  `type` tinyint(4) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `money` decimal(10, 2) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `stock` smallint(5) UNSIGNED NULL DEFAULT 0,
  `sort` smallint(5) UNSIGNED NULL DEFAULT 100,
  `status` tinyint(4) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '平台礼品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_gift
-- ----------------------------
INSERT INTO `platform_gift` VALUES (1, 1, 0, '签到商品', '签到商品-提示', 'http://tp6.hssz.com/uploads/goods_cate/20210116/d836377cd4ea785a8a8d2ddcbb933af7.png', 100.00, '<p>123123</p>', 97, 100, 2, 1610777710, 1611019312, 1611019312);
INSERT INTO `platform_gift` VALUES (2, 1, 0, '名称1', '提示2', 'http://tp6.hssz.com/uploads/goods_cate/20210116/d50521fe91b723230d00be7e77f19577.png,http://tp6.hssz.com/uploads/goods_cate/20210116/65a246fab05d863882cc2edd8579be15.png', 100.00, '<p>内容内容内容内容内容<br></p>', 100, 100, 2, 1610777773, 1611019314, 1611019314);
INSERT INTO `platform_gift` VALUES (3, 1, 2, '588年货礼包赠品', '现金购买价值588元“年货礼包”，送以下组合酒产品（价值589元）：', 'https://www.hcssz365.com/uploads/goods_cate/20210118/ff8ae1c971a18c8d8bcedb19486050e9.jpg,https://www.hcssz365.com/uploads/goods_cate/20210118/e2bb42300e7b4ab6b69fef1c68632e7b.jpg,https://www.hcssz365.com/uploads/goods_cate/20210118/281453ec1eac8eb592ee10', 589.00, '<p><br></p><p><!--[if-->1、<!--[endif]-->夜郎洞藏·民国酱香100mL：1瓶，单价143元/瓶；<o:p></o:p></p><p><!--[if-->2、<!--[endif]-->夜郎洞藏·典藏125mL：2瓶，单价75元/瓶；<o:p></o:p></p><p><!--[if-->3、<!--[endif]-->黔新酱酒珍品100mL×4礼盒：1盒，单价128元/盒；<o:p></o:p></p><p>4、好厨色·四色糯米酒250mL：4瓶，单价42元/瓶。<o:p></o:p></p>', 100, 100, 1, 1610854728, 1610948546, NULL);
INSERT INTO `platform_gift` VALUES (4, 1, 2, '我是赠品2', '我是赠品2', 'http://tp6.hssz.com/uploads/goods_cate/20210117/00d3539e2244d871e55e21e9b47704df.png', 0.00, '<p>我是赠品2</p>', 0, 100, 2, 1610854921, 1610948928, NULL);
INSERT INTO `platform_gift` VALUES (5, 1, 1, '我是积分奖品1', '我是积分奖品1', 'http://tp6.hssz.com/uploads/goods_cate/20210117/5a57f9558482f3c740254cb52fb6ff90.png', 100.00, '<p>我是积分奖品1我是积分奖品1我是积分奖品1我是积分奖品1</p>', 100, 100, 2, 1610854991, 1611019034, 1611019034);
INSERT INTO `platform_gift` VALUES (6, 1, 1, '我是积分奖品2', '我是积分奖品2', 'http://tp6.hssz.com/uploads/goods_cate/20210117/606cfd53da3f8df01bb8286edf800d40.png', 0.00, '<p>我是积分奖品2我是积分奖品2我是积分奖品2我是积分奖品2我是积分奖品2我是积分奖品2</p>', 0, 100, 2, 1610855201, 1611019036, 1611019036);
INSERT INTO `platform_gift` VALUES (7, 1, 2, '888年货礼包赠品', '现金购买价值888元“年货礼包”，送以下组合酒产品（价值902元）：', 'https://www.hcssz365.com/uploads/goods_cate/20210118/9a85d8c76c4f2f6c200ead5f0f8d6b17.jpg,https://www.hcssz365.com/uploads/goods_cate/20210118/90eb355c88a9ed35bdc94a06b6c2b90d.jpg,https://www.hcssz365.com/uploads/goods_cate/20210118/b5010499999f88836b32e3', 902.00, '<p><br></p><p>1、夜郎洞藏·民国酱香100mL：2瓶，单价143元/瓶；<o:p></o:p></p><p>2、夜郎洞藏·典藏125mL：2瓶，单价75元/瓶；<o:p></o:p></p><p>3、黔新酱酒珍品100mL×4礼盒：2盒，单价128元/盒；<o:p></o:p></p><p>4、好厨色·四色糯米酒250mL：5瓶，单价42元/瓶。<o:p></o:p></p>', 1000, 100, 1, 1610949057, 1610949076, NULL);
INSERT INTO `platform_gift` VALUES (8, 1, 2, '1288年货礼包赠品', '购买价值1288元“年货礼包”，送以下组合酒产品（1291元）', 'https://www.hcssz365.com/uploads/goods_cate/20210118/ead9124289789bd1434101adadae2a5d.jpg,https://www.hcssz365.com/uploads/goods_cate/20210118/26982f8f815b6a48a706eedcf3b91bf2.jpg,https://www.hcssz365.com/uploads/goods_cate/20210118/7c30b36b6fb130dc219efd', 1291.00, '<p><br></p><p>1、夜郎洞藏·民国酱香100mL×4：1盒，单价569元/盒；<o:p></o:p></p><p>2、夜郎洞藏·典藏125mL×4：1盒，单价298元/盒；<o:p></o:p></p><p>3、黔新酱酒珍品100mL×4礼盒：2盒，单价128元/盒；<o:p></o:p></p><p>4、好厨色·四色糯米酒250mL：4瓶，单价42元/瓶。<o:p></o:p></p>', 1000, 100, 1, 1610949137, 1610949137, NULL);
INSERT INTO `platform_gift` VALUES (9, 1, 1, '黔新酱酒金品（20积分）2', '一瓶125ml黔新酱酒金品（19.5元/瓶）', 'https://www.hcssz365.com/uploads/goods_cate/20210118/ad61423b1d464453ab79f093091fc829.jpg', 20.00, '', 1000, 100, 1, 1610949357, 1615458813, NULL);
INSERT INTO `platform_gift` VALUES (10, 1, 1, ' 黔新酱酒珍品 100ml 高品质酱香型白酒53° ', '红蓝颜色随机发', 'https://www.hcssz365.com/uploads/goods_cate/20210119/25cb6d39ffac493bd49759fecf65733d.jpeg', 30.00, '', 1000, 100, 2, 1611020481, 1615458740, NULL);
INSERT INTO `platform_gift` VALUES (11, 1, 1, '250ml白色糯米甜酒', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/23ec5ba0971a6c860099ad3b4553a45b.jpg', 30.00, '', 1000, 100, 1, 1611020698, 1615458746, 1615458746);
INSERT INTO `platform_gift` VALUES (12, 1, 1, '250ml黑色（或紫色）糯米酒', '颜色随机', 'https://www.hcssz365.com/uploads/goods_cate/20210119/f816a0b12a405790636af5167040310f.jpg', 40.00, '', 1000, 100, 1, 1611020950, 1611020950, NULL);
INSERT INTO `platform_gift` VALUES (13, 1, 0, '一等奖价值300元的酒类产品', '连续签到6天，有礼品！', 'https://www.hcssz365.com/uploads/goods_cate/20210119/a80fc25dc88c0548e4b2aacd68663ff6.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/be38231eec5de7bbf1a8b136d7407198.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/917fe7a73897f16153b2de', 300.00, '<p>奖励酒类产品组合为：<br></p><p><o:p></o:p></p><p>1、黔新酱酒（珍品）1盒（100ml*4），或是选择1瓶500ml的黔新酱酒（珍品）；<o:p></o:p></p><p>2、黔新酱酒（金品53°或43°）1盒（125ml*4），或是选择1瓶500ml的黔新酱酒（金品53°或43°）。<o:p></o:p></p>', 5, 100, 1, 1611021990, 1611022338, NULL);
INSERT INTO `platform_gift` VALUES (14, 1, 1, '1瓶*170g小甜酒（白糯米或黑糯米或紫糯米）', '随机发货', 'https://www.hcssz365.com/uploads/goods_cate/20210119/ab22aaaf352ed3fc1c3afa310fc34ee5.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/e6c84270e2765b93ddc8c6ff051e5b07.jpg', 20.00, '', 1000, 100, 1, 1611022250, 1611037904, NULL);
INSERT INTO `platform_gift` VALUES (15, 1, 0, '二等奖价值200元的酒类产品', '连续签到6天，有礼品！', 'https://www.hcssz365.com/uploads/goods_cate/20210119/5f58448c06a7105addd15b02f1077a10.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/8969c24bc24b2a0c1de48812d0255490.jpg', 200.00, '<p>奖励酒类产品为：</p><p>黔新酱酒（珍品）1盒（100ml*4），或是选择2盒（100ml*4）的黔新酱酒（金品53°或43°）。</p><p><o:p></o:p></p>', 10, 100, 1, 1611022287, 1611022330, NULL);
INSERT INTO `platform_gift` VALUES (16, 1, 1, '2瓶*170g小甜酒（白糯米或黑糯米或紫糯米）', '随机发货', 'https://www.hcssz365.com/uploads/goods_cate/20210119/69583dd182960a2d76eeb0bb1d64f205.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/b2f6e3abfc3541cbc6da6720884f0add.jpg', 30.00, '', 1000, 100, 1, 1611022364, 1611022364, NULL);
INSERT INTO `platform_gift` VALUES (17, 1, 1, '3瓶*170g小甜酒（白糯米或黑糯米或紫糯米）', '随机发货', 'https://www.hcssz365.com/uploads/goods_cate/20210119/6d4a9f3179efef426852092a810307c1.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/0e70117cfd1493242d1700ecb808b9ac.jpg', 40.00, '', 1000, 100, 1, 1611022477, 1611038133, NULL);
INSERT INTO `platform_gift` VALUES (18, 1, 0, '三等奖价值100元的酒类产品（不限名额）', '连续签到6天，有礼品！', 'https://www.hcssz365.com/uploads/goods_cate/20210119/07a020816883d7bce3e9bd3d787a8558.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/84e511062838afcdfe711bf0f09df3b9.jpg', 100.00, '<p>奖励酒类产品为：</p><p>黔新酱酒（金品53°或43°）1盒（125ml*4），或是选择1瓶500ml的黔新酱酒（金品53°或43°）。<br></p><p><o:p></o:p></p>', 100, 100, 1, 1611022503, 1611049753, NULL);
INSERT INTO `platform_gift` VALUES (19, 1, 1, '一瓶125ml黔新酱酒金品和一瓶100ml黔新酱酒珍品', '黔新酱酒珍品颜色随机发货', 'https://www.hcssz365.com/uploads/goods_cate/20210119/5a5949505de80e5a74c0305d7293358a.jpeg,https://www.hcssz365.com/uploads/goods_cate/20210119/1f7baa7f5088136eed99597afeae496c.jpg', 50.00, '', 1000, 100, 1, 1611022658, 1611022658, NULL);
INSERT INTO `platform_gift` VALUES (20, 1, 1, '4瓶*170g小甜酒（白糯米或黑糯米或紫糯米，颜色随机）', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/6f600cc3df9e46c8d233e705f67ae4ff.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/cfd120720e88e8c65718a17ec1b849e7.jpg', 50.00, '', 1000, 100, 1, 1611022731, 1611038255, NULL);
INSERT INTO `platform_gift` VALUES (21, 1, 1, '4瓶*125ml黔新酱酒金品', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/0232a78cd70a8bafab51c426e7a0e434.jpeg', 60.00, '', 1000, 100, 1, 1611022771, 1611022771, NULL);
INSERT INTO `platform_gift` VALUES (22, 1, 1, '2瓶*100ml黔新酱酒珍品', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/9cb70c84669b7c3e6206c4b045ded4fa.jpg', 60.00, '', 1000, 100, 1, 1611022851, 1611022851, NULL);
INSERT INTO `platform_gift` VALUES (23, 1, 1, '1瓶*125ml夜郎洞藏', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/8e5d7cfb7e3d3ac3ec200744a4d7c018.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/84c9704fb1a2ef0a3f85db3aa83c6d06.jpg', 75.00, '', 1000, 100, 1, 1611023411, 1611023411, NULL);
INSERT INTO `platform_gift` VALUES (24, 1, 1, '一瓶250ml白色糯米酒和一瓶250ml黑色（或紫色）糯米酒', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/3f81abe91e817037c31b016ea1a74caf.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/443d3110e8baf89d9bb1da275d93d95e.jpg', 75.00, '', 1000, 100, 1, 1611023586, 1611023586, NULL);
INSERT INTO `platform_gift` VALUES (25, 1, 1, '5瓶*170g小甜酒（白糯米或黑糯米或紫糯米，颜色随机）', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/2c95932f4284b9c693384ec0b9751d22.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/36870fa09cbe39046fb917a503b4b266.jpg', 75.00, '', 1000, 100, 1, 1611023741, 1611023741, NULL);
INSERT INTO `platform_gift` VALUES (26, 1, 1, '一瓶125ml夜郎洞藏和一瓶170g小甜酒（白糯米或黑糯米或紫糯米，颜色随机）', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/8a752b0fc8e209fbfe8faa71b6972dfd.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/72d9075112eef457a354a4ed657a05b1.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/a2197431585bd743455ee7', 90.00, '', 1000, 100, 1, 1611024326, 1611024326, NULL);
INSERT INTO `platform_gift` VALUES (27, 1, 1, '一瓶125ml夜郎洞藏和两瓶170g小甜酒（白糯米或黑糯米或紫糯米，颜色随机）', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/9768d59530088760d572e0a4a8c4811b.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/27ab729c31f3b5e539422ca5fb9fc47e.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/ded4ae72aae9984812cc9f', 105.00, '', 1000, 100, 1, 1611024435, 1611038960, NULL);
INSERT INTO `platform_gift` VALUES (28, 1, 1, '一瓶125ml夜郎洞藏和三瓶170g小甜酒（白糯米或黑糯米或紫糯米，颜色随机）', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/32ce25e225450209ef1ca8d37e2375bc.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/ad5bc048ab12e22aaa975a571e30e813.jpg', 125.00, '', 1000, 100, 1, 1611024526, 1611024526, NULL);
INSERT INTO `platform_gift` VALUES (29, 1, 1, '一瓶100ml民国酱香', '', 'https://www.hcssz365.com/uploads/goods_cate/20210119/324aa1d5b5c01b63a96bc1dd6eb501bd.jpg', 150.00, '', 1000, 100, 1, 1611024737, 1611024737, NULL);
INSERT INTO `platform_gift` VALUES (30, 1, 1, '一瓶100ml民国酱香和与余额等值的170g小甜酒（14元/瓶，白糯米或黑糯米或紫糯米，颜色随机）', '赠送规则：消费满170元以上不足300元', 'https://www.hcssz365.com/uploads/goods_cate/20210119/51ab1536e488b30a2266d0576a769eb4.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/592157437d51adbe49e8019391b5a331.jpg', 170.00, '', 1000, 100, 1, 1611025869, 1611025869, NULL);
INSERT INTO `platform_gift` VALUES (31, 1, 1, '两瓶100ml民国酱香和与余额等值的250ml白糯米酒（29.8元/瓶）或250ml黑（或紫）糯米酒', '赠送规则：消费满320元以上不足450元', 'https://www.hcssz365.com/uploads/goods_cate/20210119/97d93f03d8acf9a2086c222ec7e82955.jpg,https://www.hcssz365.com/uploads/goods_cate/20210119/9c92d717bd8e64719ce6e3bf791bd81e.jpg', 320.00, '', 1000, 100, 1, 1611026242, 1611026242, NULL);
INSERT INTO `platform_gift` VALUES (32, 8, 2, '店铺赠品', '提示', 'http://tp6.hssz.com/uploads/goods_cate/20210123/486880621201babbf2008446c7fcf5d4.png', 100.00, '<p>店铺赠品</p>', 100, 100, 1, 1611374456, 1611374456, NULL);
INSERT INTO `platform_gift` VALUES (33, 8, 2, '店铺赠品2', '店铺赠品2-', 'http://tp6.hssz.com/uploads/goods_cate/20210123/32816d7fae16f059d43b3927a7473984.png', 1.00, '<p>店铺赠品2</p>', 2, 100, 1, 1611374477, 1611374477, NULL);

-- ----------------------------
-- Table structure for platform_message
-- ----------------------------
DROP TABLE IF EXISTS `platform_message`;
CREATE TABLE `platform_message`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cond_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` tinyint(3) UNSIGNED NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '平台消息处理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_message
-- ----------------------------
INSERT INTO `platform_message` VALUES (1, 2, '标题', '内容', NULL, 1, 1610771106, 1610771106, NULL);
INSERT INTO `platform_message` VALUES (2, 2, '标题', '内容', '1,2,3', 1, 1610771314, 1610771314, NULL);
INSERT INTO `platform_message` VALUES (3, 1, '标题1', '内容\\n2', '', 1, 1610771410, 1610771410, NULL);
INSERT INTO `platform_message` VALUES (4, 2, 'test', 'test', '10016', 1, 1610951533, 1610951533, NULL);
INSERT INTO `platform_message` VALUES (5, 2, 'test', 'test', '10016', 1, 1610951535, 1610951535, NULL);
INSERT INTO `platform_message` VALUES (6, 2, 'test', 'test', '10016', 1, 1610951547, 1610951547, NULL);
INSERT INTO `platform_message` VALUES (7, 2, '10016', '10016', '10016', 1, 1610951556, 1610951556, NULL);

-- ----------------------------
-- Table structure for sms
-- ----------------------------
DROP TABLE IF EXISTS `sms`;
CREATE TABLE `sms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0,
  `phone` bigint(20) NULL DEFAULT NULL COMMENT '手机号码',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `verify` char(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `use_time` datetime(0) NULL DEFAULT NULL COMMENT '使用时间',
  `info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`, `phone`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sms
-- ----------------------------
INSERT INTO `sms` VALUES (1, 5, 18702783614, '验证码:98717,您正在使用注册功能,请勿泄漏给他人', '98717 ', NULL, '{\"RequestId\":\"2D5469E3-7447-4259-B34F-A522C4D3C8E1\",\"Message\":\"OK\",\"BizId\":\"104610208286809141^0\",\"Code\":\"OK\"}', 1608286795, 1608286795);
INSERT INTO `sms` VALUES (2, 0, 15616869786, '验证码:30562,您正在使用注册功能,请勿泄漏给他人', '30562 ', '2020-12-24 14:33:03', '{\"RequestId\":\"D9347E8F-3E8E-4A19-BFCC-248BBA362BCE\",\"Message\":\"OK\",\"BizId\":\"818103708791568006^0\",\"Code\":\"OK\"}', 1608791557, 1608791583);
INSERT INTO `sms` VALUES (3, 3, 15616869786, '验证码:89135,您正在使用注册功能,请勿泄漏给他人', '89135 ', NULL, '{\"RequestId\":\"3337A6F5-556E-4866-ABAC-8E115A1A1F13\",\"Message\":\"OK\",\"BizId\":\"481023608792032567^0\",\"Code\":\"OK\"}', 1608792021, 1608792021);
INSERT INTO `sms` VALUES (4, 1, 15616869786, '验证码:52671,您正在使用注册功能,请勿泄漏给他人', '52671 ', NULL, '{\"RequestId\":\"8A03DD8F-4B91-4067-910C-A3CE4A5CE992\",\"Message\":\"OK\",\"BizId\":\"863310208794132903^0\",\"Code\":\"OK\"}', 1608794122, 1608794122);
INSERT INTO `sms` VALUES (5, 1, 15616869786, '验证码:88496,您正在使用注册功能,请勿泄漏给他人', '88496 ', NULL, '{\"RequestId\":\"273F7CD2-6DD3-4CFF-A932-F964568CCF36\",\"Message\":\"OK\",\"BizId\":\"229603808794264961^0\",\"Code\":\"OK\"}', 1608794254, 1608794254);
INSERT INTO `sms` VALUES (6, 1, 15616869786, '验证码:60592,您正在使用注册功能,请勿泄漏给他人', '60592 ', NULL, '{\"RequestId\":\"CCDB6FD8-6224-4BB0-AF4B-4C67789E4093\",\"Message\":\"OK\",\"BizId\":\"351025008794323588^0\",\"Code\":\"OK\"}', 1608794312, 1608794312);
INSERT INTO `sms` VALUES (7, 1, 15616869786, '验证码:95644,您正在使用注册功能,请勿泄漏给他人', '95644 ', '2020-12-24 15:22:09', '{\"RequestId\":\"AD36C49B-AAAE-46F5-8A52-060467AE99AA\",\"Message\":\"OK\",\"BizId\":\"944321408794529757^0\",\"Code\":\"OK\"}', 1608794519, 1608794529);

-- ----------------------------
-- Table structure for sys_image_manager
-- ----------------------------
DROP TABLE IF EXISTS `sys_image_manager`;
CREATE TABLE `sys_image_manager`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mime_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hash` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `fsize` double UNSIGNED NULL DEFAULT NULL,
  `width` double UNSIGNED NULL DEFAULT NULL,
  `height` double UNSIGNED NULL DEFAULT NULL,
  `duration` double UNSIGNED NULL DEFAULT NULL,
  `v_width` double UNSIGNED NULL DEFAULT NULL,
  `v_height` double NULL DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NULL DEFAULT 0,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_image_manager
-- ----------------------------
INSERT INTO `sys_image_manager` VALUES (3, 'image', 'video/mp4', 'http://tp6.company-admin.com/uploads/image/20210317/c5615c0ac167ec9f612312e878ed8df0.mp4', '355e13129c34bc80ef5fc861cceb63a99356288d', 1220768, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_image_manager` VALUES (4, 'article', 'image', 'http://tp6.zbyp.com/uploads/article/20210322/a9d5e3c405a6ec210246eaf3918d70e8.png', 'd399ca6d4c80e6ab4f612d9993ca4ba93af3ac1c', 21472, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_image_manager` VALUES (5, 'article', 'image', 'http://tp6.zbyp.com/uploads/article/20210322/5284b15dac9626a27bef5cc0fcf0f649.png', 'd399ca6d4c80e6ab4f612d9993ca4ba93af3ac1c', 21472, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);

-- ----------------------------
-- Table structure for sys_manager
-- ----------------------------
DROP TABLE IF EXISTS `sys_manager`;
CREATE TABLE `sys_manager`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` bigint(20) NULL DEFAULT NULL,
  `rid` int(11) NULL DEFAULT 0,
  `rt_id` int(11) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `salt` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '1正常 2禁用',
  `is_special` tinyint(4) NULL DEFAULT 0 COMMENT '特殊状态',
  `login_times` int(11) NULL DEFAULT NULL,
  `last_login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_time` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `account`(`account`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_manager
-- ----------------------------
INSERT INTO `sys_manager` VALUES (1, 'admin', 'admin', 123, 6, NULL, 'n4doNvWe/p1TxqGy+yYRGcuFBjxV+Ep7lee18KAvRFM=', '1363 ', 1, 1, 96, '192.168.5.119', 1608862835, 1573876478, 1608862835, NULL);
INSERT INTO `sys_manager` VALUES (2, 'yh', 'yehua1', 0, 0, NULL, '792a7beebd75f0f11a498a6ffcdbd678', '41316', 1, 0, 3, '127.0.0.1', 1583935059, 1583934721, 1585883564, 1585883564);
INSERT INTO `sys_manager` VALUES (3, 'yehua', 'yehua2', 0, 0, NULL, '1bed7e2c423a38cdff90e18566607f6c', '79285', 1, 0, NULL, NULL, NULL, 1583936254, 1585883578, 1585883578);
INSERT INTO `sys_manager` VALUES (4, 'yehua', 'yehua', 12345678901, 9, 15, 'pNBvu0jhPhg+DuJU8fyZKL6XA5GNsTYk', '6592 ', 1, 0, 156, '127.0.0.1', 1616378070, 1583938328, 1616378070, NULL);
INSERT INTO `sys_manager` VALUES (5, '超如', 'pancru', NULL, 0, NULL, 'y22zZUSr/ABLdahT5otuGmwqJZIFBg74', '2115 ', 2, 0, 1, '127.0.0.1', 1604396780, 1604396768, 1608260401, 1608260401);
INSERT INTO `sys_manager` VALUES (6, '管理员名', '管理员帐号', NULL, 11, NULL, 'w2PgDG1KqVWq7Te69rrDskedB1oural/', '9435 ', 2, 0, NULL, NULL, NULL, 1608260022, 1608260403, 1608260403);
INSERT INTO `sys_manager` VALUES (7, 'test', 'test', NULL, 9, 14, 'kU33huu2QupeAxJiqMpvx643rhA1I5/M', '6258 ', 2, 0, NULL, NULL, NULL, 1608262765, 1608263245, NULL);

-- ----------------------------
-- Table structure for sys_money_queue
-- ----------------------------
DROP TABLE IF EXISTS `sys_money_queue`;
CREATE TABLE `sys_money_queue`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(255) UNSIGNED NULL DEFAULT 0,
  `uid` int(11) NULL DEFAULT 0 COMMENT '用户/商户id',
  `cond_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `mark_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '标记信息',
  `money` decimal(10, 2) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 0,
  `info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '保留关键数据',
  `handle_time` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cond_id`(`type`, `uid`, `cond_id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_money_queue
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT 0 COMMENT '顶级',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `node` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '可操作的权限节点',
  `sort` tinyint(4) NULL DEFAULT 100 COMMENT '排序',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态',
  `is_sys` tinyint(4) NULL DEFAULT 0 COMMENT '系统指定角色 ',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (5, 0, '管理员', '/user/index,/goods/cate,/goods/index,/order/index,/system/setting,/system/roles,/system/rolesAdd,/system/rolesDel,/system/manager,/system/managerAdd,/system/managerDel', 100, 1, 0, 1573877725, 1573877747, 1573877747);
INSERT INTO `sys_role` VALUES (6, 0, '系统管理员', 'user,help,protocol,system,hide_sign', 100, 1, 0, 1573885868, 1605948223, NULL);
INSERT INTO `sys_role` VALUES (9, 0, '订单管理员', 'user,goods,goods,order,help,protocol', 100, 1, 0, 1596789072, 1599784035, NULL);
INSERT INTO `sys_role` VALUES (10, 0, '券管理', 'user,coupon,goods,order,help,protocol,system', 100, 1, 0, 1597370472, 1597383055, NULL);
INSERT INTO `sys_role` VALUES (7, 0, '管理员', 'user,coupon,goods,order,help,protocol', 100, 2, 0, 1596788058, 1608262047, NULL);
INSERT INTO `sys_role` VALUES (8, 0, '订单管理员', 'goods,order', 100, 1, 0, 1596788065, 1596788148, 1596788148);
INSERT INTO `sys_role` VALUES (11, 0, '商品管理', 'goods', 100, 2, 0, 1597370499, 1608262045, NULL);
INSERT INTO `sys_role` VALUES (12, 0, '订单管理', 'user,hide_sign', 100, 2, 0, 1597370517, 1608262055, 1608262055);
INSERT INTO `sys_role` VALUES (13, 6, '系统1-1', NULL, 2, 1, 0, 1608262136, 1608262176, 1608262176);
INSERT INTO `sys_role` VALUES (14, 9, '订单管理员1', NULL, 3, 1, 0, 1608262403, 1608262439, NULL);
INSERT INTO `sys_role` VALUES (15, 9, '订单管理员2', NULL, 1, 1, 0, 1608262416, 1608262434, NULL);

-- ----------------------------
-- Table structure for sys_setting
-- ----------------------------
DROP TABLE IF EXISTS `sys_setting`;
CREATE TABLE `sys_setting`  (
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`type`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_setting
-- ----------------------------
INSERT INTO `sys_setting` VALUES ('third_wx_open_app_secret', NULL, NULL, NULL);
INSERT INTO `sys_setting` VALUES ('third_wx_open_app_id', NULL, NULL, NULL);
INSERT INTO `sys_setting` VALUES ('qn_upload_preview_url', 'http://qn.ymkj6188.com/', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('tipProtocol', '<p>隐私政策协议</p>', 1605233099, 1605233099);
INSERT INTO `sys_setting` VALUES ('sms_key_secret', 'I2ZkredWM7JaeKgxdzi0OflgDObzbI', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('sms_key_id', 'LTAI4G8GJ96s545Z1KWnFpS2', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('sms_sign_name', '亿苗网', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('qn_upload_host', 'http://up-z2.qiniup.com', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('qn_upload_bucket', 'ymw', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('qn_upload_sk', 'bmQoFymqfQKLAJR2HhAzG_7D4ibtCSK9SOJC5TiL', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('qn_upload_ak', 'XRYEV0cLW55PIbgRSWwyY9YdwXRYehDiem2zbTtt', NULL, NULL);
INSERT INTO `sys_setting` VALUES ('other_goods_service_intro', '服务提示语', 1605165974, 1606894004);
INSERT INTO `sys_setting` VALUES ('other_goods_hot_key_comma', ' iphone,华为', 1605165974, 1606894004);
INSERT INTO `sys_setting` VALUES ('normal_title', '345', 1605165968, 1606910534);
INSERT INTO `sys_setting` VALUES ('serviceProtocol', '<p><strong>我是用协议</strong></p>', 1605165650, 1605166846);
INSERT INTO `sys_setting` VALUES ('registerProtocol', '<p>注册协议</p>', 1605166846, 1605515484);
INSERT INTO `sys_setting` VALUES ('normal_sign_integral', '2', 1605165968, 1606910534);
INSERT INTO `sys_setting` VALUES ('guaranteeProtocol', '<p>担保认证内容</p>', 1605515481, 1605515484);
INSERT INTO `sys_setting` VALUES ('aboutUs', '<p>关于我们</p>', 1605528265, 1605528265);
INSERT INTO `sys_setting` VALUES ('other_sign_intro_line', '1.签到当日获得的积分实时累加，不清零\n2.连续签到，获得积分加倍，如中断签到则重新开始加速\n3.累计的积分可以用于兑换积分商城里面的礼品，详情请看产品详情介绍\n4.累计的积分可以用于抵扣次年续费，详情请咨询平台客服，最终解释权归平台所有', 1605528546, 1605528546);
INSERT INTO `sys_setting` VALUES ('other_sign_intro_enter', '1.签到当日获得的积分实时累加，不清零\n2.连续签到，获得积分加倍，如中断签到则重新开始加速\n3.累计的积分可以用于兑换积分商城里面的礼品，详情请看产品详情介绍\n4.累计的积分可以用于抵扣次年续费，详情请咨询平台客服，最终解释权归平台所有', 1605528810, 1606894004);
INSERT INTO `sys_setting` VALUES ('normal_integral_per', '100', 1605530366, 1605530370);
INSERT INTO `sys_setting` VALUES ('normal_integral_per_int', '100', 1605530416, 1606910534);
INSERT INTO `sys_setting` VALUES ('award_pool', '0', 1605530416, 1605530416);
INSERT INTO `sys_setting` VALUES ('user_type', NULL, 1606460214, 1606460214);
INSERT INTO `sys_setting` VALUES ('user_type_json', '[{\"name\":\"\\u4f1a\\u5458\",\"value\":\"0\",\"integral\":\"1\",\"money\":\"2\",\"per\":\"3\",\"intro\":\"4\"},{\"name\":\"VIP1\",\"value\":\"1\",\"integral\":\"5\",\"money\":\"13\",\"per\":\"6\",\"intro\":\"dfg\"},{\"name\":\"VIP2\",\"value\":\"2\",\"integral\":\"23\",\"money\":\"14\",\"per\":\"432\",\"intro\":\"234\"},{\"name\":\"VIP3\",\"value\":\"3\",\"integral\":\"34\",\"money\":\"500\",\"per\":\"23\",\"intro\":\"sdfg\"},{\"name\":\"VIP4\",\"value\":\"4\",\"integral\":\"234\",\"money\":\"23\",\"per\":\"234\",\"intro\":\"23\"}]', 1606460230, 1607682264);
INSERT INTO `sys_setting` VALUES ('other_about_us', '您好！深圳市亿苗科技有限公司联合知名农林学院，\n我公司专业，专注，专心打造国内一流苗木供求平台。\n发布供求信息，项目融资，园林绿化相关内容免费培训。', 1606893134, 1606894004);
INSERT INTO `sys_setting` VALUES ('normal_custom_tel', '400-108-2328', 1606893180, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_play_second_int', '3', 1606910531, 1606910534);
INSERT INTO `sys_setting` VALUES ('normal_email', '2798605572@qq.com', 1606910531, 1606910534);
INSERT INTO `sys_setting` VALUES ('normal_qq', '2798605572', 1606910531, 1606910534);
INSERT INTO `sys_setting` VALUES ('normal_mobile', '07558979478', 1606910531, 1606910534);
INSERT INTO `sys_setting` VALUES ('normal_address', '深圳市龙岗区南湾街道樟树布社区坪埔路17号101', 1606910531, 1606910534);
INSERT INTO `sys_setting` VALUES ('normal_contact_name', '123321', 1607674144, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_company_name', '432234', 1607674144, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_company_addr', '123123', 1607674144, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_wechat_code', 'http://tp6.company-admin.com/uploads/system/20201211/31c7ea866e3f8f896ca640a260808a9e.png', 1607674144, 1616378869);
INSERT INTO `sys_setting` VALUES ('integral_sign', '0.1', 1607675719, 1607675719);
INSERT INTO `sys_setting` VALUES ('freight_style_int', '1', 1607677071, 1608015806);
INSERT INTO `sys_setting` VALUES ('wechat_mch_id', 'mch_id', 1607677395, 1607677395);
INSERT INTO `sys_setting` VALUES ('wechat_mch_key', 'key', 1607677395, 1607677395);
INSERT INTO `sys_setting` VALUES ('wechat_open_app_id', 'app_id', 1607677395, 1607677395);
INSERT INTO `sys_setting` VALUES ('wechat_open_app_secret', 'app_secret', 1607677395, 1607677395);
INSERT INTO `sys_setting` VALUES ('wechat_pub_app_id', 'app_secret', 1607677395, 1607677395);
INSERT INTO `sys_setting` VALUES ('user_type_undefined', '4', 1607681926, 1607681926);
INSERT INTO `sys_setting` VALUES ('freight_logistics', '顺风\n韵达\n圆通\n天天快递\n邮政', 1608015729, 1608015729);
INSERT INTO `sys_setting` VALUES ('freight_logistics_enter', '顺风\n韵达\n圆通\n天天快递\n邮政', 1608015806, 1608015806);
INSERT INTO `sys_setting` VALUES ('integral_sign_float', '1', 1608865800, 1608868213);
INSERT INTO `sys_setting` VALUES ('integral_per_float', '1', 1608865800, 1608868213);
INSERT INTO `sys_setting` VALUES ('integral_intro', '1.签到当日获得的积分实时累加，不清零\n2.连续签到，获得积分加倍，如中断签到则重新开始加速\n3.累计的积分可以用于兑换积分商城里面的礼品，详情请看产品详情介绍\n4.累计的积分可以用于抵扣次年续费，详情请咨询平台客服，最终解释权归平台所有', 1608868213, 1608868213);
INSERT INTO `sys_setting` VALUES ('normal_search_keyword_comma', '热门字1,热门字2,热门字3', 1609224472, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_copyright', '©2018 All Rights 深圳市深正互联网络有限公司版权所有', 1609236195, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_copyright_sub', '粤ICP备13050901号-1 ', 1609236195, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_platform_description', 'meta description 通用设置', 1616378869, 1616378869);
INSERT INTO `sys_setting` VALUES ('normal_platform_keyword', 'meta keyword 通用设置', 1616378869, 1616378869);

-- ----------------------------
-- Table structure for u_addr
-- ----------------------------
DROP TABLE IF EXISTS `u_addr`;
CREATE TABLE `u_addr`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL,
  `phone` bigint(20) NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `addr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `addr_extra` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `zip_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮编',
  `lng` decimal(10, 6) NULL DEFAULT NULL COMMENT '经度',
  `lat` decimal(10, 6) NULL DEFAULT NULL COMMENT '纬度',
  `is_default` tinyint(4) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_addr
-- ----------------------------
INSERT INTO `u_addr` VALUES (1, 10001, 18702783600, '张三', '广东省/深圳市/福田区', '沙尾东村', NULL, NULL, NULL, 0, 1608631793, 1608632122, 1608632122);
INSERT INTO `u_addr` VALUES (2, 10001, 18702783600, '张三', '广东省/深圳市/福田区', '沙尾东村', NULL, NULL, NULL, 0, 1608631826, 1609320665, NULL);
INSERT INTO `u_addr` VALUES (3, 10001, 18702783600, '张三3', '广东省/深圳市/福田区', '沙尾东村2', NULL, NULL, NULL, 0, 1608631856, 1609320665, NULL);
INSERT INTO `u_addr` VALUES (4, 10011, 15616869786, '123', '北京市市辖区东城区', '15', NULL, NULL, NULL, 0, 1608795052, 1608795466, NULL);
INSERT INTO `u_addr` VALUES (5, 10011, 15616869786, '啊啊啊啊', '吉林省长春市绿园区', '123', NULL, NULL, NULL, 1, 1608795466, 1608795466, NULL);
INSERT INTO `u_addr` VALUES (6, 10011, 15616869786, '为二人', '安徽省合肥市瑶海区', '515', NULL, NULL, NULL, 0, 1608795489, 1608795509, 1608795509);
INSERT INTO `u_addr` VALUES (7, 10001, 18702783600, '张三123asdfasdf', '北京市市辖区东城区', '沙尾东村123sadfsadf', NULL, NULL, NULL, 1, 1609320369, 1609320665, NULL);
INSERT INTO `u_addr` VALUES (8, 10001, 18702783600, '张三123', '广东省汕头市潮阳区', '沙尾东村123123', NULL, NULL, NULL, 0, 1609320397, 1609320665, NULL);
INSERT INTO `u_addr` VALUES (9, 10001, 18702783600, '张三123123', '北京市市辖区东城区', '沙尾东村123123sadfsadf', NULL, NULL, NULL, 0, 1609320454, 1609320665, NULL);
INSERT INTO `u_addr` VALUES (10, 10014, 18702783003, '张三', '广东省深圳市福田区', '沙尾东城', NULL, NULL, NULL, 1, 1615344734, 1615344734, NULL);
INSERT INTO `u_addr` VALUES (11, 10014, 18702783003, '李四', '广东省深圳市福田区', '沙尾村', NULL, NULL, NULL, 0, 1615344808, 1615344808, NULL);

-- ----------------------------
-- Table structure for u_cart
-- ----------------------------
DROP TABLE IF EXISTS `u_cart`;
CREATE TABLE `u_cart`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL,
  `mch_id` int(11) NULL DEFAULT NULL,
  `gid` int(11) UNSIGNED NULL DEFAULT NULL,
  `sku_id` int(11) UNSIGNED NULL DEFAULT 0,
  `num` smallint(6) UNSIGNED NULL DEFAULT 0,
  `is_checked` tinyint(4) NULL DEFAULT 1 COMMENT '是否选中',
  `is_multi` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`, `gid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of u_cart
-- ----------------------------
INSERT INTO `u_cart` VALUES (9, 10001, NULL, 67, 43, 4, 0, 0);
INSERT INTO `u_cart` VALUES (14, 10011, NULL, 68, 38, 2, 1, 0);
INSERT INTO `u_cart` VALUES (8, 10001, NULL, 67, 44, 17, 1, 0);
INSERT INTO `u_cart` VALUES (15, 10011, NULL, 57, 39, 1, 1, 0);
INSERT INTO `u_cart` VALUES (16, 10001, NULL, 70, 0, 1, 1, 0);

-- ----------------------------
-- Table structure for u_coll_goods
-- ----------------------------
DROP TABLE IF EXISTS `u_coll_goods`;
CREATE TABLE `u_coll_goods`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NULL DEFAULT NULL,
  `gid` int(10) UNSIGNED NULL DEFAULT NULL,
  `coll_time` datetime(0) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of u_coll_goods
-- ----------------------------
INSERT INTO `u_coll_goods` VALUES (1, 10014, 69, NULL, 1615775109);

-- ----------------------------
-- Table structure for u_date_sign
-- ----------------------------
DROP TABLE IF EXISTS `u_date_sign`;
CREATE TABLE `u_date_sign`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0,
  `uid` int(11) UNSIGNED NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  `times` int(11) NULL DEFAULT NULL,
  `lx_times` int(11) NULL DEFAULT NULL,
  `integral` decimal(10, 2) NULL DEFAULT 0.00,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid`(`uid`, `type`, `date`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of u_date_sign
-- ----------------------------
INSERT INTO `u_date_sign` VALUES (1, 0, 10001, '2020-12-24', 1, 1, 2.00, 1608869148, 1608869148);
INSERT INTO `u_date_sign` VALUES (2, 0, 10014, '2020-12-25', 2, 2, 1.00, 1608869201, 1608869201);
INSERT INTO `u_date_sign` VALUES (6, 0, 10014, '2021-03-12', 1, 1, 1.00, 1615546924, 1615546924);
INSERT INTO `u_date_sign` VALUES (8, 0, 10014, '2021-03-15', 2, 1, 1.00, 1615772322, 1615772322);

-- ----------------------------
-- Table structure for u_feedback
-- ----------------------------
DROP TABLE IF EXISTS `u_feedback`;
CREATE TABLE `u_feedback`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `phone` bigint(20) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_feedback
-- ----------------------------
INSERT INTO `u_feedback` VALUES (1, 0, '我是反馈内容', NULL, NULL, 0, 1608346408, 1608346408, NULL);
INSERT INTO `u_feedback` VALUES (2, 10010, '123', '', NULL, 0, 1608369933, 1608369933, NULL);
INSERT INTO `u_feedback` VALUES (3, 10010, '123123123', '', NULL, 0, 1608370361, 1608370361, NULL);
INSERT INTO `u_feedback` VALUES (4, 10010, '12314124124', '', NULL, 0, 1608370368, 1608370368, NULL);
INSERT INTO `u_feedback` VALUES (5, 10011, '2134234234', '', NULL, 0, 1608862595, 1608862595, NULL);

-- ----------------------------
-- Table structure for u_logs
-- ----------------------------
DROP TABLE IF EXISTS `u_logs`;
CREATE TABLE `u_logs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0 COMMENT '通知主分类',
  `m_type` tinyint(4) NULL DEFAULT NULL COMMENT '二级分类',
  `uid` int(11) NULL DEFAULT NULL,
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `q_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '变动前余额',
  `money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '变动余额',
  `h_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '变动后余额',
  `info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `is_hide` tinyint(4) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`, `uid`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_logs
-- ----------------------------
INSERT INTO `u_logs` VALUES (1, 2, 1, 10001, '签到增加积分', 150.00, 2.00, 152.00, '{\"q_money\":150,\"h_money\":152,\"id\":\"1\"}', 0, 1608869148);
INSERT INTO `u_logs` VALUES (2, 2, 1, 10014, '签到增加积分', 152.00, 1.00, 153.00, '{\"q_money\":152,\"h_money\":153,\"id\":\"2\"}', 0, 1608869201);
INSERT INTO `u_logs` VALUES (3, 1, 1, 10014, '签到增加', 0.00, 1.00, 1.00, '{\"q_money\":0,\"h_money\":1,\"id\":\"4\",\"sign_integral\":\"1\"}', 0, 1615546835);
INSERT INTO `u_logs` VALUES (4, 1, 1, 10014, '签到增加', 1.00, 1.00, 2.00, '{\"q_money\":1,\"h_money\":2,\"id\":\"5\",\"sign_integral\":\"1\"}', 0, 1615546876);
INSERT INTO `u_logs` VALUES (5, 1, 1, 10014, '签到增加', 2.00, 1.00, 3.00, '{\"q_money\":2,\"h_money\":3,\"id\":\"6\",\"sign_integral\":\"1\"}', 0, 1615546924);
INSERT INTO `u_logs` VALUES (6, 1, 1, 10014, '签到增加', 3.00, 1.00, 4.00, '{\"q_money\":3,\"h_money\":4,\"id\":\"7\",\"sign_integral\":\"1\"}', 0, 1615772274);
INSERT INTO `u_logs` VALUES (7, 1, 1, 10014, '签到增加', 4.00, 1.00, 5.00, '{\"q_money\":4,\"h_money\":5,\"id\":\"8\",\"sign_integral\":\"1\"}', 0, 1615772322);
INSERT INTO `u_logs` VALUES (8, 2, 1, 10001, '签到增加积分', 150.00, 2.00, 152.00, '{\\\"q_money\\\":150,\\\"h_money\\\":152,\\\"id\\\":\\\"1\\\"}', 0, 1608869148);
INSERT INTO `u_logs` VALUES (9, 2, 1, 10001, '签到增加积分', 152.00, 1.00, 153.00, '{\\\"q_money\\\":152,\\\"h_money\\\":153,\\\"id\\\":\\\"2\\\"}', 0, 1608869201);
INSERT INTO `u_logs` VALUES (10, 1, 1, 10014, '签到增加', 0.00, 1.00, 1.00, '{\\\"q_money\\\":0,\\\"h_money\\\":1,\\\"id\\\":\\\"4\\\",\\\"sign_integral\\\":\\\"1\\\"}', 0, 1615546835);
INSERT INTO `u_logs` VALUES (11, 1, 1, 10014, '签到增加', 1.00, 1.00, 2.00, '{\\\"q_money\\\":1,\\\"h_money\\\":2,\\\"id\\\":\\\"5\\\",\\\"sign_integral\\\":\\\"1\\\"}', 0, 1615546876);
INSERT INTO `u_logs` VALUES (12, 1, 1, 10014, '签到增加', 2.00, 2.00, 3.00, '{\\\"q_money\\\":2,\\\"h_money\\\":3,\\\"id\\\":\\\"6\\\",\\\"sign_integral\\\":\\\"1\\\"}', 0, 1615546924);
INSERT INTO `u_logs` VALUES (13, 1, 1, 10014, '签到增加', 3.00, 3.00, 4.00, '{\\\"q_money\\\":3,\\\"h_money\\\":4,\\\"id\\\":\\\"7\\\",\\\"sign_integral\\\":\\\"1\\\"}', 0, 1615772274);
INSERT INTO `u_logs` VALUES (14, 1, 1, 10014, '签到增加', 4.00, 4.00, 5.00, '{\\\"q_money\\\":4,\\\"h_money\\\":5,\\\"id\\\":\\\"8\\\",\\\"sign_integral\\\":\\\"1\\\"}', 0, 1615772322);
INSERT INTO `u_logs` VALUES (15, 1, 1, 10014, '签到增加', 1.00, 5.00, 2.00, '{\\\\\\\"q_money\\\\\\\":1,\\\\\\\"h_money\\\\\\\":2,\\\\\\\"id\\\\\\\":\\\\\\\"5\\\\\\\",\\\\\\\"sign_integral\\\\\\\":\\\\\\\"1\\\\\\\"}', 0, 1615546876);
INSERT INTO `u_logs` VALUES (16, 1, 1, 10014, '签到增加', 2.00, 6.00, 3.00, '{\\\\\\\"q_money\\\\\\\":2,\\\\\\\"h_money\\\\\\\":3,\\\\\\\"id\\\\\\\":\\\\\\\"6\\\\\\\",\\\\\\\"sign_integral\\\\\\\":\\\\\\\"1\\\\\\\"}', 0, 1615546924);
INSERT INTO `u_logs` VALUES (17, 1, 1, 10014, '签到增加', 3.00, 7.00, 4.00, '{\\\\\\\"q_money\\\\\\\":3,\\\\\\\"h_money\\\\\\\":4,\\\\\\\"id\\\\\\\":\\\\\\\"7\\\\\\\",\\\\\\\"sign_integral\\\\\\\":\\\\\\\"1\\\\\\\"}', 0, 1615772274);
INSERT INTO `u_logs` VALUES (18, 1, 1, 10014, '签到增加', 4.00, 8.00, 5.00, '{\\\\\\\"q_money\\\\\\\":4,\\\\\\\"h_money\\\\\\\":5,\\\\\\\"id\\\\\\\":\\\\\\\"8\\\\\\\",\\\\\\\"sign_integral\\\\\\\":\\\\\\\"1\\\\\\\"}', 0, 1615772322);
INSERT INTO `u_logs` VALUES (19, 1, 1, 10014, '签到增加', 4.00, 4.00, 5.00, '{\\\\\\\"q_money\\\\\\\":4,\\\\\\\"h_money\\\\\\\":5,\\\\\\\"id\\\\\\\":\\\\\\\"8\\\\\\\",\\\\\\\"sign_integral\\\\\\\":\\\\\\\"1\\\\\\\"}', 0, 1615772322);
INSERT INTO `u_logs` VALUES (20, 1, 1, 10014, '签到增加', 1.00, 5.00, 2.00, '{\\\\\\\\\\\\\\\"q_money\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"h_money\\\\\\\\\\\\\\\":2,\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"5\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"sign_integral\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1\\\\\\\\\\\\\\\"}', 0, 1615546876);
INSERT INTO `u_logs` VALUES (21, 1, 1, 10014, '签到增加', 2.00, 6.00, 3.00, '{\\\\\\\\\\\\\\\"q_money\\\\\\\\\\\\\\\":2,\\\\\\\\\\\\\\\"h_money\\\\\\\\\\\\\\\":3,\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"6\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"sign_integral\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1\\\\\\\\\\\\\\\"}', 0, 1615546924);
INSERT INTO `u_logs` VALUES (22, 1, 1, 10014, '签到增加', 3.00, 7.00, 4.00, '{\\\\\\\\\\\\\\\"q_money\\\\\\\\\\\\\\\":3,\\\\\\\\\\\\\\\"h_money\\\\\\\\\\\\\\\":4,\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"7\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"sign_integral\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1\\\\\\\\\\\\\\\"}', 0, 1615772274);
INSERT INTO `u_logs` VALUES (23, 1, 1, 10014, '签到增加', 4.00, 8.00, 5.00, '{\\\\\\\\\\\\\\\"q_money\\\\\\\\\\\\\\\":4,\\\\\\\\\\\\\\\"h_money\\\\\\\\\\\\\\\":5,\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"8\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"sign_integral\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1\\\\\\\\\\\\\\\"}', 0, 1615772322);

-- ----------------------------
-- Table structure for u_notice
-- ----------------------------
DROP TABLE IF EXISTS `u_notice`;
CREATE TABLE `u_notice`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0 COMMENT '通知类型',
  `c_type` tinyint(4) NULL DEFAULT 0 COMMENT '二级类型',
  `cond_id` int(11) NULL DEFAULT 0 COMMENT '条件id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `uid` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`, `uid`) USING BTREE,
  INDEX `uid`(`uid`, `cond_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_notice
-- ----------------------------
INSERT INTO `u_notice` VALUES (1, 0, 2, 2659, '/assets/images/logo.png', 10001, '订单完成通知', '订单号为:202009105940005订单交易已完成.', NULL, 1, 1608013770, 1608013770, NULL);
INSERT INTO `u_notice` VALUES (2, 0, 3, 2657, '/assets/images/logo.png', 10001, '订单发货通知', '订单号为:202009100730003已发货', NULL, 1, 1608017669, 1608017669, NULL);
INSERT INTO `u_notice` VALUES (3, 0, 1, 2696, '/assets/images/logo.png', 10001, '订单支付完成通知', '订单号为:202012232085010订单交易已付款.', NULL, 1, 1608713544, 1608713544, NULL);
INSERT INTO `u_notice` VALUES (4, 0, 3, 2696, '/assets/images/logo.png', 10001, '订单发货通知', '订单号为:202012232085010已发货', NULL, 1, 1608713572, 1608713572, NULL);
INSERT INTO `u_notice` VALUES (5, 0, 2, 2696, '/assets/images/logo.png', 10001, '订单完成通知', '订单号为:202012232085010订单交易已完成.', NULL, 1, 1608713578, 1608713578, NULL);
INSERT INTO `u_notice` VALUES (6, 0, 1, 2697, '/assets/images/logo.png', 10001, '订单支付完成通知', '订单号为:202012230948011订单交易已付款.', NULL, 1, 1608714203, 1608714203, NULL);
INSERT INTO `u_notice` VALUES (7, 0, 3, 2697, '/assets/images/logo.png', 10001, '订单发货通知', '订单号为:202012230948011已发货', NULL, 1, 1608714399, 1608714399, NULL);
INSERT INTO `u_notice` VALUES (8, 0, 2, 2697, '/assets/images/logo.png', 10001, '订单完成通知', '订单号为:202012230948011订单交易已完成.', NULL, 1, 1608714407, 1608714407, NULL);
INSERT INTO `u_notice` VALUES (9, 0, 1, 2708, '/assets/images/logo.png', 10001, '订单支付完成通知', '订单号为:20201228511701001订单交易已付款.', NULL, 1, 1609156582, 1609156582, NULL);
INSERT INTO `u_notice` VALUES (10, 0, 3, 2708, '/assets/images/logo.png', 10001, '订单发货通知', '订单号为:20201228511701001已发货', NULL, 1, 1609156643, 1609156643, NULL);
INSERT INTO `u_notice` VALUES (11, 0, 2, 2708, '/assets/images/logo.png', 10001, '订单完成通知', '订单号为:20201228511701001订单交易已完成.', NULL, 1, 1609156972, 1609156972, NULL);
INSERT INTO `u_notice` VALUES (12, 0, 1, 2719, '/assets/images/logo.png', 10014, '订单支付完成通知', '订单号为:20210310156301001订单交易已付款.', NULL, 1, 1615345191, 1615345191, NULL);
INSERT INTO `u_notice` VALUES (13, 0, 1, 2720, '/assets/images/logo.png', 10014, '订单支付完成通知', '订单号为:20210310528101002订单交易已付款.', NULL, 1, 1615345694, 1615345694, NULL);
INSERT INTO `u_notice` VALUES (14, 0, 3, 2661, '/assets/images/logo.png', 10001, '订单发货通知', '订单号为:202009101592007已发货', NULL, 1, 1615345732, 1615345732, NULL);
INSERT INTO `u_notice` VALUES (15, 0, 3, 2719, '/assets/images/logo.png', 10014, '订单发货通知', '订单号为:20210310156301001已发货', NULL, 1, 1615345780, 1615345780, NULL);
INSERT INTO `u_notice` VALUES (16, 0, 2, 2719, '/assets/images/logo.png', 10014, '订单完成通知', '订单号为:20210310156301001订单交易已完成.', NULL, 1, 1615345808, 1615345808, NULL);
INSERT INTO `u_notice` VALUES (17, 0, 3, 2720, '/assets/images/logo.png', 10014, '订单发货通知', '订单号为:20210310528101002已发货', NULL, 1, 1615345923, 1615345923, NULL);
INSERT INTO `u_notice` VALUES (18, 0, 2, 2720, '/assets/images/logo.png', 10014, '订单完成通知', '订单号为:20210310528101002订单交易已完成.', NULL, 1, 1615348433, 1615348433, NULL);
INSERT INTO `u_notice` VALUES (19, 0, 1, 2723, '/assets/images/logo.png', 10014, '订单支付完成通知', '订单号为:20210311125901001订单交易已付款.', NULL, 1, 1615447359, 1615447359, NULL);
INSERT INTO `u_notice` VALUES (20, 0, 3, 2723, '/assets/images/logo.png', 10014, '订单发货通知', '订单号为:20210311125901001已发货', NULL, 1, 1615447371, 1615447371, NULL);
INSERT INTO `u_notice` VALUES (21, 0, 2, 2723, '/assets/images/logo.png', 10014, '订单完成通知', '订单号为:20210311125901001订单交易已完成.', NULL, 1, 1615447409, 1615447409, NULL);
INSERT INTO `u_notice` VALUES (22, 0, 1, 2731, '/assets/images/logo.png', 10001, '订单支付完成通知', '订单号为:20210315165001001订单交易已付款.', NULL, 1, 1615776384, 1615776384, NULL);
INSERT INTO `u_notice` VALUES (23, 0, 2, 2731, '/assets/images/logo.png', 10001, '订单完成通知', '订单号为:20210315165001001订单交易已完成.', NULL, 1, 1615776390, 1615776390, NULL);
INSERT INTO `u_notice` VALUES (24, 0, 1, 2732, '/assets/images/logo.png', 10001, '订单支付完成通知', '订单号为:20210315097901002订单交易已付款.', NULL, 1, 1615776436, 1615776436, NULL);
INSERT INTO `u_notice` VALUES (25, 0, 3, 2732, '/assets/images/logo.png', 10001, '订单发货通知', '订单号为:20210315097901002已发货', NULL, 1, 1615776441, 1615776441, NULL);
INSERT INTO `u_notice` VALUES (26, 0, 2, 2732, '/assets/images/logo.png', 10001, '订单完成通知', '订单号为:20210315097901002订单交易已完成.', NULL, 1, 1615776450, 1615776450, NULL);

-- ----------------------------
-- Table structure for u_notice_read
-- ----------------------------
DROP TABLE IF EXISTS `u_notice_read`;
CREATE TABLE `u_notice_read`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL COMMENT '消息类型',
  `c_type` tinyint(4) NULL DEFAULT NULL COMMENT '消息类型2',
  `nid` int(11) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`, `nid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of u_notice_read
-- ----------------------------
INSERT INTO `u_notice_read` VALUES (1, 10001, 0, NULL, 8, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (2, 10001, 0, NULL, 7, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (3, 10001, 0, NULL, 6, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (4, 10001, 0, NULL, 5, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (5, 10001, 0, NULL, 4, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (6, 10001, 0, NULL, 3, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (7, 10001, 0, NULL, 2, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (8, 10001, 0, NULL, 1, 2, 1608802459, 1608802609);
INSERT INTO `u_notice_read` VALUES (9, 10001, 0, NULL, 11, 1, 1609225484, 1609225484);
INSERT INTO `u_notice_read` VALUES (10, 10001, 0, NULL, 10, 1, 1609225484, 1609225484);
INSERT INTO `u_notice_read` VALUES (11, 10001, 0, NULL, 9, 1, 1609225484, 1609225484);
INSERT INTO `u_notice_read` VALUES (12, 10014, 0, NULL, 21, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (13, 10014, 0, NULL, 20, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (14, 10014, 0, NULL, 19, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (15, 10014, 0, NULL, 18, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (16, 10014, 0, NULL, 17, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (17, 10014, 0, NULL, 16, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (18, 10014, 0, NULL, 15, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (19, 10014, 0, NULL, 13, 1, 1615456007, 1615456007);
INSERT INTO `u_notice_read` VALUES (20, 10014, 0, NULL, 12, 1, 1615456007, 1615456007);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NULL DEFAULT 0 COMMENT '用户类型',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `money` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00,
  `total_cost_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '累计消费金额',
  `integral` int(11) NULL DEFAULT 0 COMMENT '积分',
  `history_integral` int(11) NULL DEFAULT NULL COMMENT '累计积分',
  `email` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `sex` tinyint(4) NULL DEFAULT 0 COMMENT '性别',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `phone` bigint(20) NULL DEFAULT NULL,
  `age` smallint(6) NULL DEFAULT NULL COMMENT '年龄',
  `addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员所在地区',
  `birth_y` smallint(6) NULL DEFAULT NULL COMMENT '年份',
  `birth_m` tinyint(4) NULL DEFAULT NULL COMMENT '月份',
  `birth_d` tinyint(4) NULL DEFAULT NULL COMMENT '天',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户简介',
  `total_commission` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '累计总佣金',
  `req_open_num` int(11) NULL DEFAULT 0 COMMENT '累计邀请人数',
  `f_uid1` int(11) NULL DEFAULT 0,
  `f_uid2` int(11) NULL DEFAULT 0,
  `f_uid3` int(10) UNSIGNED NULL DEFAULT 0,
  `req_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邀请码',
  `min_openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序openid',
  `min_wx_auth_state` tinyint(4) NULL DEFAULT 0 COMMENT '微信授权状态',
  `wx_openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `wx_unionid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信unionid',
  `qq_openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `wb_openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `withdraw_wxcode` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信号',
  `auth_key` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(5) NULL DEFAULT 1 COMMENT '状态',
  `ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `create_time` int(18) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(18) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE,
  INDEX `f_uid1`(`f_uid1`, `f_uid2`, `f_uid3`) USING BTREE,
  INDEX `f_uid2`(`f_uid2`, `f_uid3`) USING BTREE,
  INDEX `f_uid3`(`f_uid3`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10015 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (10001, 3, 'a蒜', 8.00, 0.00, 153, NULL, NULL, 2, 'http://192.168.5.118/uploads/cb4b270f7f27dbc998728186634d5eb7.png', 18702783614, 23, '内蒙古自治区/赤峰市/巴林右旗', 1997, 8, 14, 'MgB8BnNj4y/yGRiXd5PQz5enl+EdNKsb', '2394', '', 0.00, 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'bbbb', NULL, 1, '127.0.0.1', NULL, 1584034177, 1615776264, NULL);
INSERT INTO `user` VALUES (10012, 0, '000用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://tp6.company-admin.com/assets/images/avatar.png', 18702783000, NULL, NULL, NULL, NULL, NULL, 'zTOyqaxA5L1h/pD9KRvZKBnuI7yrO/JW', '7250', NULL, 0.00, 0, 0, 0, 0, 'MHAKMSGI', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1615340131, 1615340131, NULL);
INSERT INTO `user` VALUES (10003, 0, 'a蒜', 9.00, 24.00, 0, NULL, NULL, 1, 'https://thirdwx.qlogo.cn/mmopen/vi_32/5SneLMm7n5Wu5tLRcpmuWw7u1cEyYdovBs2K2z5lywnyx3MhM4tw1SmbmqXTI0d6RuGMC9tzRYLOxe9gRGa8OA/132', NULL, NULL, '河北省 邯郸市 复兴区', NULL, NULL, NULL, NULL, NULL, NULL, 9.00, 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1603349044, 1603885318, NULL);
INSERT INTO `user` VALUES (10004, 0, 'a蒜', 0.00, 0.00, 0, NULL, NULL, 1, 'https://thirdwx.qlogo.cn/mmopen/vi_32/5SneLMm7n5Wu5tLRcpmuWw7u1cEyYdovBs2K2z5lywnyx3MhM4tw1SmbmqXTI0d6RuGMC9tzRYLOxe9gRGa8OA/132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 0, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1603886631, 1608033362, 1608033362);
INSERT INTO `user` VALUES (10005, 0, 'a蒜123', 0.00, 0.00, 0, NULL, NULL, 1, 'https://thirdwx.qlogo.cn/mmopen/vi_32/5SneLMm7n5Wu5tLRcpmuWw7u1cEyYdovBs2K2z5lywnyx3MhM4tw1SmbmqXTI0d6RuGMC9tzRYLOxe9gRGa8OA/132', 18702783610, NULL, NULL, NULL, NULL, NULL, 'oxbg3X8kzlI/qtd8ir8oBlSs1Qtoya/5', '8711', NULL, 0.00, 0, 0, 0, 0, 'B1926GAB', 'ohsjd4lTFwldKCHy5702cGzvCnQk', 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, '127.0.0.1', NULL, 1603887930, 1604749484, NULL);
INSERT INTO `user` VALUES (10006, 0, '600用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://tp6.houtai.com/assets/images/avatar.jpg', 18702783600, NULL, NULL, NULL, NULL, NULL, 'FtsC15VV/vXmCS2+j8fy7d+KWuuBZW5W', '5202', NULL, 0.00, 0, 0, 0, 0, 'EQG6Q6VU', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, '127.0.0.1', NULL, 1605166591, 1608033346, NULL);
INSERT INTO `user` VALUES (10007, 0, '601用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://tp6.houtai.com/assets/images/avatar.jpg', 18702783601, NULL, NULL, NULL, NULL, NULL, '/xbIflrhD36dg0auoXJQuafZQiLPE/TG', '2442', NULL, 0.00, 0, 0, 0, 0, 'OBNN1HMD', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1605166608, 1608033358, 1608033358);
INSERT INTO `user` VALUES (10008, 0, '602用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://tp6.houtai.com/assets/images/avatar.jpg', 18702783602, NULL, NULL, NULL, NULL, NULL, 'JsIlebKANGpW1M/uSvojt2UQ8ndrCQYT', '2419', NULL, 0.00, 0, 0, 0, 0, 'VEVKLTF8', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1605166631, 1605166631, NULL);
INSERT INTO `user` VALUES (10009, 0, '603用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://tp6.houtai.com/assets/images/avatar.jpg', 18702783603, NULL, NULL, NULL, NULL, NULL, 'xAFviEPXeZcX8IVpnQqpHDNTcx1tRgkf', '2458', NULL, 0.00, 0, 0, 0, 0, 'PR3JGS8F', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1605166668, 1605166668, NULL);
INSERT INTO `user` VALUES (10010, 2, '003用户名', 23.00, 0.00, 43, NULL, NULL, 0, 'http://192.168.5.113/uploads/avatar/20201219/76e4b63890ddf3f1c601bca5f4e9cf82.jpg', 18702783604, NULL, NULL, NULL, NULL, NULL, 'sRQ67H+s2dYlV8TSNmf9iLoEgiQ9eg1b', '3084', NULL, 0.00, 0, 0, 0, 0, '9RLMP146', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '192.168.5.109', NULL, 1605239134, 1608367019, NULL);
INSERT INTO `user` VALUES (10011, 0, '786用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://192.168.5.104/assets/images/avatar.png', 15616869786, NULL, NULL, NULL, NULL, NULL, 'amYDzjneuzCmmP9S1lEVG4J7I3e6G9FQ', '6673', NULL, 0.00, 0, 0, 0, 0, '4J8CCDFI', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '192.168.5.119', NULL, 1608791584, 1608794529, NULL);
INSERT INTO `user` VALUES (10013, 0, '001用户名', 0.00, 0.00, 0, NULL, NULL, 0, 'http://tp6.company-admin.com/assets/images/avatar.png', 18702783001, NULL, NULL, NULL, NULL, NULL, 'R23w/7egQ+fm7MjdvnXy0OaK5+oRjFri', '1330', NULL, 0.00, 0, 0, 0, 0, 'T1U7PEQC', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1615340330, 1615340330, NULL);
INSERT INTO `user` VALUES (10014, 0, '6666', 0.00, 0.00, 5, NULL, NULL, 2, 'http://tp6.company-admin.com/uploads/926620d1ea58cead6faaf59531249b1c.png', 18702783003, NULL, NULL, NULL, NULL, NULL, 'AwDlVNFdcz01r/tae9UyzeD5rxxfv7rK', '5106', NULL, 0.00, 0, 0, 0, 0, 'MVTNBG9R', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL, 1615344239, 1615772322, NULL);

-- ----------------------------
-- Table structure for wgt
-- ----------------------------
DROP TABLE IF EXISTS `wgt`;
CREATE TABLE `wgt`  (
  `id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NULL DEFAULT 0,
  `platform` tinyint(4) NULL DEFAULT 0,
  `version` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` tinyint(4) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wgt
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
