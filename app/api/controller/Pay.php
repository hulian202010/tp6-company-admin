<?php
namespace app\api\controller;

use app\common\model\OrderModel;

class Pay extends Common
{
    public function order()
    {
        try {
            $model = OrderModel::getOrderPayInfo(input());
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }
        $pay_info = $model->getPayInfo('wechat',0);
        return $this->_resData(1,'操作成功',[
            'pay_money' => $pay_info['pay_money'],
            'no' => $pay_info['no'],
            'body' => $pay_info['body'],
            'pay_list' => OrderModel::getPropInfo('fields_pay_way'),
        ]);
    }
    //订单支付
    public function info()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        try {
            $res = OrderModel::usePayWay($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,'操作成功',$res);

    }


    //微信支付回调
    public function wechatNotify()
    {
        \think\facade\Log::write("wechatNotify-notify:" .json_encode(input()),'-------input-----');
        \think\facade\Log::write("wechatNotify-notify:" .file_get_contents("php://input"),'-----file_get_contents-----');
        \app\common\service\WechatV3Pay::notify();
    }

    //支付宝支付回调
    public function alipayNotify()
    {
        \think\facade\Log::write("alipayNotify-notify:" .json_encode(input()),'-------input-----');
        \think\facade\Log::write("alipayNotify-notify:" .file_get_contents("php://input"),'-----file_get_contents-----');
        return \app\common\service\Alipay::notify();
    }
}