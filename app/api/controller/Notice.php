<?php
namespace app\api\controller;

use app\common\model\CommentModel;
use app\common\model\OrderGoodsModel;
use app\common\model\OrderModel;
use app\common\model\UserNoticeModel;

class Notice extends Common
{

    public function info()
    {
        $list = UserNoticeModel::getPropInfo('fields_type');
        foreach ($list as $key=>&$vo){
            $last_info = UserNoticeModel::where(['type'=>$key])->order('id desc')->find();
            //未读说明
            $vo['intro'] = empty($last_info['content'])?'':$last_info['content'];
            //未读数量
            $vo['num'] = UserNoticeModel::getNoReadCount($this->user_model,['type'=>$key]);
        }

        return $this->_resData(1,'获取成功',[
            'list'=>$list
        ]);
    }

    public function lists()
    {
        $list = [];
        $info = UserNoticeModel::getPageData($this->user_model,input())->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'total_page'=>$info->lastPage()]);
    }

}