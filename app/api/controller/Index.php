<?php
namespace app\api\controller;


use app\common\model\GoodsCateModel;
use app\common\model\GoodsModel;
use app\common\model\ImageModel;
use app\common\model\SysSettingModel;
use app\common\model\UserCartModel;
use app\common\model\UserFeedbackModel;
use app\common\model\UserModel;
use app\common\model\UserNoticeModel;
use app\common\model\WgtModel;
use app\common\service\Config;
use app\common\service\TimeActivity;
use app\common\service\WechatV3Pay;

class Index extends Common
{
    public function index()
    {
        return $this->_resData(0,'入口异常',[]);
    }

    public function test()
    {

        $preg_str = "股票、移民、面试招聘、彩票、返利、贷款、催款、投资理财、中奖、一元夺宝、一元秒杀、A货、医疗、整形、美容、会所、酒吧、足浴、带有威胁性质、皮草、助考、商标注册、加群、加QQ或者加微信、贩卖个人信息、APP下载链接、申请好评返现、宣传短信通道、用户拉新、用户召回、";
        $content = "我是一句话,移民,面试,33,返,32";
        $content=str_ireplace(explode('、',$preg_str),"**",$content);
        dump($content);exit;
//        return 'test123';
//        密码解密
//        dump(UserModel::de_generatePwd('FygecV4gdLxFz9wDrvIOZ+Ua/I8A2XKt','5041'));exit;
//        dump(Config::payWay('wechat_mini','config','app_id'));exit;
//        $number = WechatV3Pay::auth();
//        dump($number);exit;
    }

    //获取各种图
    private function _image($type)
    {
        $list = [];
        //获取启动图
        ImageModel::getPageData(['limit'=>5,'type'=>$type])->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $list;
    }

    public function image()
    {
        $type = input('type',0,'intval');
        $list = $this->_image($type);
        return $this->_resData(1,'获取成功',[
            'list' => $list,
        ]);
    }


    public function startImage()
    {
        $state = 0; //禁用启动图
        $open_close = input('open_close',0);
        $cache_name = 'start_state';
        $cache_state = cache($cache_name);
        if(!empty($open_close)){
            cache($cache_name,$open_close);
            $cache_state = $open_close;
        }

        $image_list = [];

        if($cache_state==1){ //开启屏蔽
            $state = 1; //广告图
            $down_time = 3;
            $image_list[] = ['img'=>'../../static/images/guide/guide-img1.jpg'];
        }else{
            //获取启动图
            $image_list = $this->_image(1);
            $img_len= count($image_list);
            if($img_len > 0){
                $state = 1; //广告图
                $down_time = $img_len>1?0:3;
            }

        }


        return $this->_resData(1,'获取成功',[
            'state' => $state,  //0禁用 1广告图
            'down_time' => $down_time,
            'image_list' => $image_list,
        ]);
    }

    public function defInfo()
    {
        $data = [];
        $type = input('type','','trim');
        if($type=='search_keyword'){
            $data['hot_key'] = SysSettingModel::getContent('normal','search_keyword_comma');
        }elseif($type=='about_us'){
            $data['logo'] = '/static/images/logo.png';
            $data['copyright'] = (string)SysSettingModel::getContent('normal','copyright');
            $data['copyright_sub'] = (string)SysSettingModel::getContent('normal','copyright_sub');
        }elseif($type=='kill_page'){
            $data['kill_time'] = TimeActivity::allInfo();
            //购物车数量
            $data['cart_num']  = UserCartModel::getNum($this->user_id);
        }

        return $this->_resData(1,'获取成功',$data);
    }

    public function data()
    {
        $data = [];
        $data['follow_img'] = $this->_image(0);
        $nav = [];
        GoodsCateModel::getAllCate([])->each(function($item,$index)use(&$nav){
            array_push($nav,$item->apiNormalInfo());
        });
        $data['nav'] = $nav;

        $data['msg_num'] = UserNoticeModel::getNoReadCount($this->user_model);


        //当前进行秒杀活动
        $data['kill_info'] = TimeActivity::getRunning();
        $kill_at = empty($data['kill_info']['value'])?0:$data['kill_info']['value'];
        $kill_list = [];
        GoodsModel::getPageData(['kill_at'=>$kill_at,'limit'=>8])->each(function($item)use(&$kill_list){
            array_push($kill_list,$item->apiNormalInfo());
        });
        $data['kill_list'] = $kill_list;


        return $this->_resData(1,'获取成功',$data);
    }

    //登录
    public function login()
    {

        try{
            $model = \app\common\model\UserModel::handleLogin(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'登录成功',$this->loginInfo($model));
    }
    //注册
    public function reg()
    {

        try{
            $model = \app\common\model\UserModel::register(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,'注册成功',$this->loginInfo($model));

    }

    public function forget()
    {
        try{
            $model = \app\common\model\UserModel::forget(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,'找回成功');
    }


    //发送短信
    public function sendSms()
    {

        $type = $this->request->param('type','');
        $phone = $this->request->param('phone','','trim');
        try{
            \app\common\model\SmsModel::send($type,$phone);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'发送成功');
    }

    //意见反馈
    public function feedback()
    {
        try {
            $data = input();
            $data['uid'] = $this->user_id;
            UserFeedbackModel::handleFeedback($data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'感谢您的反馈');
    }


    //协议入口
    public function webProtocol()
    {
        $type = input('type','','trim');
        $content = SysSettingModel::getContent($type);
        return view('/protocol',[
            'title'=> isset(SysSettingModel::$protocol_info[$type])?SysSettingModel::$protocol_info[$type]['name']:config('app.app_name'),
            'content'=>$content,
        ]);
    }

    /*
     * 分享
     * type: 0-图文（weixin、sinaweibo）,1-纯文字,2-纯图片，3-音乐（weixin、qq）,4-视频（weixin、sinaweibo）,5-小程序(仅支持weixin)
     * */

    public function shareContent()
    {
        $page = input('page');
        $req_code = input('req_code');
        $provider = input('provider');
        $href = "";
        $domain=$this->request->domain();
        $model = null;

        $type = 0;
        $title = "商品分享";
        $summary= "分享内容";
        $imageUrl = $domain.'/assets/images/logo.png';
        if(false){
        }else{
            $title = "用户邀请";
            $summary= "邀请用户";
            $req_tip_msg = SysSettingModel::getContent('normal','req_tip_msg');
            $summary = empty($req_tip_msg) ? $summary : $req_tip_msg;

            $imageUrl = $domain.'/assets/images/logo.png';
            $href = $domain."/index/index/login?req_code=".$req_code;
            $href = $domain."/api/index/down?req_code=".$req_code;
        }
        if($provider=='qq'){
            $type=1;
        }

        return $this->_resData(1,'获取成功',[
            'share_content'=>[
                'type' => $type,
                'title' => $title,     //	分享内容的标题
                'href' => $href, //type 为 0 时必选
                'imageUrl' => $imageUrl,//type 为 0、2、5 时必选  图片地址。type为0时，推荐使用小于20Kb的图片
                'mediaUrl' => '',//type type 为 3、4 时必选  音视频地址
                'summary' => $summary, //分享内容的摘要
            ],
        ]);
    }




    //检测版本更新
    public function checkUpdate()
    {
        $platform = input('platform');
        $name = input('name');

        $type = 0;
        //查找最后一条记录
        $last_model = WgtModel::where(['status'=>1,'type'=>$type,'platform'=>0])->order('id desc')->find();
        if($platform=='ios'){
            $ios_model = WgtModel::where(['status'=>1,'type'=>$type,'platform'=>2])->order('id desc')->find();
            if(!empty($ios_model) && $ios_model['id']>=$last_model['id']){
                $last_model = $ios_model;
            }
        }elseif($platform=='android'){
            $android_model = WgtModel::where(['status'=>1,'type'=>$type,'platform'=>1])->order('id desc')->find();
            if(!empty($android_model) && $android_model['id']>=$last_model['id']){
                $last_model = $android_model;
            }
        }
//        dump($last_model->toArray());exit;
        $version = input('version');
        $wgtUrl = '';//更新组件
        $pkgUrl = '';//更新包
        $update = false;
        $msg = '';
        if($last_model['version'] > $version){
            $msg = '发现新包,后台更新中..';
            $update=true;
            $wgtUrl = $last_model['path'];
        }

        $data = [
            'msg' => $msg,
            'update' => $update,
            'platform' => $platform,
            'content' => empty($last_model['content'])?'':$last_model['content'],
            'wgtUrl' => $wgtUrl,
            'pkgUrl' => $pkgUrl,//更新包
        ];
        return $this->_resData(1,'获取成功',$data);
    }


    public function richText()
    {
        $type = input('type','','trim');
        $tip_version = input('tip_version','','trim');
        if(empty($type)){
            return $this->_resData(0, '参数异常:type');
        }

        $cache_state = input('cache_state',0,'intval');
        if($cache_state>0){
            cache('cache_state',$cache_state);
        }

        $cache_state = cache('cache_state');
        $data  = [
            'content' => '',
            'version' => '',
            'tip_second' => 0,
        ];
        if($cache_state==1){
            $data['version'] = date('Y-m-d H:i:s');
            $data['content'] = '<p> 项目验收中...</p>';
        }else{
            //用户协议
            $model_protocol = SysSettingModel::where(['type'=>$type])->find();

            if(!empty($model_protocol) && $tip_version!=$model_protocol->getData('update_time')){
                $data['content'] = (String)$model_protocol['content'];
                $data['version'] = (int)$model_protocol->getData('update_time');

            }
        }



        return $this->_resData(1,'获取成功',$data);
    }

}
