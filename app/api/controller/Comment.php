<?php
namespace app\api\controller;


use app\common\model\CommentModel;

class Comment extends Common
{
    protected $open_action_validate = true;

    //订单评论信息
    public function orderGoodsDetail()
    {
        list($order,$goods_list) = CommentModel::orderGoodsDetail($this->user_model,input());
        return $this->_resData(1,'获取成功',[
            'order'=>$order,
            'goods_list'=>$goods_list,
        ]);
    }


    //评论
    public function orderGoodsComment()
    {
        try{
            CommentModel::OrderGoodsComment($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'评论成功');
    }
}