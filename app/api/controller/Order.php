<?php
namespace app\api\controller;


use app\common\model\CouponUserModel;
use app\common\model\GoodsCateModel;
use app\common\model\GoodsModel;
use app\common\model\ImageModel;
use app\common\model\OrderGoodsModel;
use app\common\model\OrderModel;
use app\common\model\SysSettingModel;
use app\common\model\UserCartModel;
use app\common\service\Config;
use app\common\service\WechatV3Pay;

class Order extends Common
{
    protected $open_action_validate = true;

    protected $ignore_action=['lists','detail','buy','confirm','commentlists','salesafterlist'];

    public function lists()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $list = [];
        $info = \app\common\model\OrderModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }


    public function detail()
    {
        $id = input('id',0,'intval');
        $input_data['uid'] = $this->user_id;
        $input_data['id'] = $id;
        $model = [];
        OrderModel::getPageData($input_data)->each(function ($item, $index) use (&$model) {
            $model = $item->apiFullInfo();
        });
        return $this->_resData(1,'获取成功',['detail'=>$model]);
    }

    public function buy()
    {
        $input_data = input();
        //绑定订单用户对象
        OrderModel::$user_model = $this->user_model;
        list($mch_goods_data,$model_addr) = OrderModel::handleOrderData($input_data);
//        $max_integral=$max_q_integral=0;
        $goods_data = [];
        foreach ($mch_goods_data as $vo){
            $merchant = $vo['mch_info']??null;
            $goods_list = $vo['goods_list']??[];

            $all_goods = [];
            foreach ($goods_list as $item){
                array_push($all_goods,$item->apiNormalInfo());
            }

            array_push( $goods_data,[
                'mch_info' => empty($merchant)?(object)[]:$merchant->apiNormalInfo(),
                'goods_list' => $all_goods,
                'extra_info' => $vo['extra_info']??(object)[],
            ]);
        }

        //优惠券列表
        $coupon_list = [];
        if(!empty($this->user_id)){
            CouponUserModel::where(['uid'=>$this->user_id,'status'=>0])->select()->each(function($item)use(&$coupon_list){
                array_push($coupon_list,$item->apiNormalInfo());
            });
        }

        $first_goods_data = $goods_data[0]??[];
        $first_goods_data_extra_info = $first_goods_data['extra_info']??[];

        $freight_money = empty($first_goods_data_extra_info)?'0.00': $first_goods_data_extra_info['total_freight_money'] ;
        $tax_money = empty($first_goods_data_extra_info)?'0.00': $first_goods_data_extra_info['total_tax_money'] ;
        //积分使用规则
        $integral_per = SysSettingModel::getContent('integral','per_float'); //积分抵现比
        $integral_low = (int)SysSettingModel::getContent('integral','low_int'); //最低使用积分
        $integral_top = (int)SysSettingModel::getContent('integral','top_int'); //最高使用积分

        $user_integral = empty($this->user_model['integral'])?0:$this->user_model['integral'];

        return $this->_resData(1,'获取成功',[
            'freight_money'  => $freight_money,
            'tax_money'  => $tax_money,
            'integral_per'  => $integral_per,
            'integral_low'  => $integral_low,
            'integral_top'  => $integral_top,
            'user_integral'  => $user_integral,
            'order_goods'   =>  $goods_data,
            'coupon_list'   =>  $coupon_list,
            'model_addr'    =>  empty($model_addr)?[]:$model_addr->apiNormalInfo(),
        ]);
    }


    public function confirm()
    {
        $input_data = input();
        $input_data['user_id'] = $this->user_id;
        $model = new OrderModel();
        try{
            OrderModel::$user_model = $this->user_model;
            list($order_id,$h_order_no,$is_pay) = $model->confirm($input_data);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已创建订单',['order_id'=>$order_id,'h_order_no'=>$h_order_no,'is_pay'=>$is_pay]);
    }



    //删除订单
    public function del()
    {
        try{
            OrderModel::del($this->user_model,input('id',0,'intval'));
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //取消订单
    public function cancel()
    {
        try{
            $model = OrderModel::cancel($this->user_model,input('id',0,'intval'));
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }

    //确认收货订单
    public function receive()
    {
        try{
            $model = OrderModel::receive($this->user_model,input('id',0,'intval'));
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }

//    //确认收货订单
//    public function complete()
//    {
//        try{
//            $model = OrderModel::complete($this->user_model,input('id',0,'intval'));
//        }catch (\Exception $e){
//            return $this->_resData(0,$e->getMessage());
//        }
//        return $this->_resData(1,'操作成功',[
//            'status'=> (int)$model['status'],
//            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
//            'status_name' => $model->getStepFlowInfo($model['step_flow']),
//            'handle_action' => $model->getHandleAction('m_handle'),
//        ]);
//    }
    //确认收货订单
    public function reminder()
    {
        try{
            $model = OrderModel::reminder($this->user_model,input('id',0,'intval'));
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已通知',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }


    //售后列表
    public function salesAfterList()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $list = [];
        $info = OrderGoodsModel::getSalesAfterList($input_data)->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }


    public function salesAfterGoodsDetail()
    {
        $input_data = input();
        $input_data['id'] = input('id',0,'intval');
        $input_data['uid'] = $this->user_id;
        $detail = [];
        OrderGoodsModel::getSalesAfterList($input_data)->each(function($item)use(&$detail){
            $detail = $item;
        });
        return $this->_resData(1,'获取成功',[
            'detail'=>empty($detail)?(object)[]:$detail->apiNormalInfo(),
        ]);
    }


    //订单售后
    public function salesAfterCreate()
    {
        try{
            OrderGoodsModel::createSalesAfter($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已提交申请,请耐心等待');
    }
    //订单售后
    public function backInfo()
    {
        $input_data = input();
        $input_data['id'] = input('id',0,'intval');
        $input_data['uid'] = $this->user_id;
        $detail = [];
        OrderGoodsModel::getSalesAfterList($input_data)->each(function($item)use(&$detail){
            $detail = $item;
        });

        return $this->_resData(1,'获取成功',[
            'custom_tel' => SysSettingModel::getContent('normal','custom_tel'),//客服电话
            'detail' =>empty($detail)?(object)[]:$detail->apiBackInfo(),
        ]);
    }

    //评价
    public function backCancel()
    {
        try{
            OrderGoodsModel::backCancel($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'取消成功');
    }

}
