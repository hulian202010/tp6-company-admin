<?php
namespace app\api\controller;

use app\common\model\CouponUserModel;
use app\common\model\CurrencyModel;
use app\common\model\GdCommentModel;
use app\common\model\HotelModel;
use app\common\model\MchSubscribeModel;
use app\common\model\MerchantModel;
use app\common\model\OrderGoodsModel;
use app\common\model\OrderModel;
use app\common\model\ProblemCateModel;
use app\common\model\ProblemModel;
use app\common\model\SysSettingModel;
use app\common\model\UserAddrModel;
use app\common\model\UserCartModel;
use app\common\model\UserCollGoodsModel;
use app\common\model\UserCollMchModel;
use app\common\model\UserCollModel;
use app\common\model\UserDataSignModel;
use app\common\model\UserDrawLogsModel;
use app\common\model\UseReqInfoModel;
use app\common\model\UserIntegralGiveModel;
use app\common\model\UserLogsModel;
use app\common\model\UserMaterialModel;
use app\common\model\UserModel;
use app\common\model\UserNoticeModel;
use app\common\model\UserSignInfoModel;

class User extends Common
{
	protected $open_action_validate = true;

	protected $ignore_action=['flush','data','moneylogs','coupon','signinfo','addrlists','addrdetail'];

    public function flush()
    {
        if(empty($this->user_id)){
            return $this->_resData(1,'请先登录');
        }

        return $this->_resData(1,'刷新成功',$this->loginInfo($this->user_model));
    }

    public function data()
    {
        $mode = input('mode','','trim');
        $data = [
            'custom_tel'=>SysSettingModel::getContent('normal','custom_tel'),//客服电话
            'money'=>empty($this->user_id)?'0.00':$this->user_model['money'],

        ];
        if($mode=='mine'){
            $data['notice_num'] = empty($this->user_id)?0:UserNoticeModel::getNoReadCount($this->user_model);
            $data['coupon_num'] =  empty($this->user_id)?0:CouponUserModel::where(['uid'=>$this->user_id,'status'=>0])->count();
            $data['coll_num'] = UserCollGoodsModel::getCount($this->user_id);//商品收藏数量
            $data['integral'] =  empty($this->user_id)?'0':$this->user_model['integral'];
            //待付款
            $data['order_pay_num'] = empty($this->user_id)?0:OrderModel::where(['uid'=>$this->user_id])->where(OrderModel::getStateWhere(1))->count();
            //待发货
            $data['order_send_num'] = empty($this->user_id)?0:OrderModel::where(['uid'=>$this->user_id])->where(OrderModel::getStateWhere(3))->count();
            //待收货
            $data['order_rec_num'] = empty($this->user_id)?0:OrderModel::where(['uid'=>$this->user_id])->where(OrderModel::getStateWhere(4))->count();
            //待评价
            $data['order_comment_num'] = empty($this->user_id)?0:OrderGoodsModel::where(['uid'=>$this->user_id,'is_pay'=>1,'is_comment'=>0])->count();
        }


        return $this->_resData(1,'获取成功',$data);

    }


    //用户信息更新
    public function modInfo()
    {
        try {
            $this->user_model->modifyInfo(input());
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已保存');
    }


    public function addrLists()
    {
        $input_data =input();
        $input_data['uid'] = $this->user_id;
        $list = [];

        $info = UserAddrModel::getPageData($input_data)->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });

        return $this->_resData(1,'获取成功',[
            'list' => $list,
            'total'=>$info->total(),
            'last_page'=>$info->lastPage()
        ]);
    }
    public function addrDetail()
    {
        $id =input('id',0,'intval');
        $input_data['uid'] = $this->user_id;
        $input_data['id'] = $id;

        $detail = [];
        UserAddrModel::getPageData($input_data)->each(function($item)use(&$detail){
            $detail = $item->apiFullInfo();
        });

        return $this->_resData(1,'获取成功',[
            'detail' => $detail,
        ]);
    }

    //保存数据
    public function addrAdd()
    {

        $php_input = $this->request->param();
        try{
            $php_input['uid'] = $this->user_id;
            UserAddrModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }

    //删除数据
    public function addrDel()
    {
        $id = input('id',0,'intval');
        try{
            UserAddrModel::actionDel(['id'=>$id,'uid'=>$this->user_id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    public function addrModInfo()
    {
        $id = input('id',0,'intval');
        try {
            $update = [];
            $update['is_default'] = input('is_default',0,'intval');
            UserAddrModel::modInfo($update,['id'=>$id,'uid'=>$this->user_id]);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已保存');
    }



    //签到信息
    public function signInfo()
    {
        $red_times = 0;//剩余签到次数
        //签到积分
        $sign_integral = SysSettingModel::getContent('integral','sign_float');
        $integral_per = SysSettingModel::getContent('integral','per_float');

        //本周信息
        $date = get_current_week();

        $sign_day = [];
        UserDataSignModel::where([
            ['uid','=',$this->user_id],
            ['date','>=',$date[0]??''],
            ['date','<=',$date[count($date)-1]??'']
        ])->order('id asc')->select()->each(function($item,$index)use(&$sign_day,&$last_times){
            $day =$item['date'];
            array_push($sign_day,$day);
            $last_times = $item['lx_times'];
        });
        $week_info = [];
        $day_info = ['日','一','二','三','四','五','六'];
        foreach ($date as $vo){
            $week_info[]=[
                'day' => $vo,
                'integral' => $sign_integral,
                'intro' =>'周'.($day_info[date('w',strtotime($vo))]??''),
            ];
        }

        //说明
        $sign_intro = SysSettingModel::getContent('other','sign_intro_enter');

        //签到数据
        $sing_model = UserDataSignModel::order('id desc')->where(['uid'=>$this->user_id])->find();
        $last_times = empty($sing_model)?0:$sing_model['lx_times']; //连续签到次数
        $today_sign_status = $sing_model['date']==date('Y-m-d')?1:0; //今天是否签到
        return $this->_resData(1,'操作成功',[
            'integral' => empty($this->user_model['integral'])?0:$this->user_model['integral'],
            'sign_intro'=>$sign_intro,
            'week_info'=>$week_info,
            'sign_day'=>$sign_day,
            'last_times'=>$last_times,
            'red_times'=>$red_times,
            'is_sign'=>$today_sign_status,
            'integral_per'=>$integral_per,
        ]);
    }

    //用户签到
    public function sign()
    {
        try{
            list($last_times,$num,$sign_day )= UserDataSignModel::sign($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,'签到成功',[
            'last_times'=>$last_times,
            'num'=>$num-0,
            'sign_day'=>$sign_day,
        ]);
    }



    public function moneyLogs()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $list = [];
        //日志记录
        $info = UserLogsModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,[
                'intro'=>$item['intro'],
                'money'=>$item['money'],
                'date'=>date('Y-m-d',$item->getData('create_time')),
            ]);
        });
        return $this->_resData(1,'获取成功',[
            'money'=>empty($this->user_model['money'])?'0.00':$this->user_model['money'],
            'integral'=>empty($this->user_model['integral'])?0:$this->user_model['integral'],
            'list'=>$list,
            'total_page'=>$info->lastPage()
        ]);
    }


    public function coupon()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $list = [];
        $info = CouponUserModel::getPageData($input_data)->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total_page'=>$info->lastPage()]);
    }



}