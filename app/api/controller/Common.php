<?php
namespace app\api\controller;

use app\BaseController;
use app\common\model\CouponModel;
use app\common\model\CouponUserModel;
use app\common\model\UserIdentAuthModel;
use app\common\model\UserModel;
use think\App;

class Common extends BaseController
{

    /**
     * @var UserModel
     * 用户登录模型
     * */
    protected $user_model = null;

//    public function initialize()
//    {
//
//        //仅仅测试使用
//        $this->user_id = 10001;
//        $this->user_model = UserModel::find($this->user_id);
//
//        parent::initialize();
//
//    }

    /**
     * 生产登录凭证
     * @param $model UserModel
     * @return array
     * */
    protected function loginInfo(UserModel $model):array
    {
        $type = $model->getAttr('type');
        return [
            'token'     => $model->generateUserToken($model['password']),
            'user_id' => $model->getAttr('id'),

            'user_avatar' => $model->getAttr('avatar'),
            'user_name' => (string)$model->getAttr('name'),
            'user_is_share_ident' => $model->getAttr('is_share_ident'), //分销权限

            'user_phone' => $model->getAttr('phone'),
            'user_hide_phone' => $model->getAttr('hide_phone'),


            'user_sex' => (int)$model->getAttr('sex'),//性别
            'user_email' => (string) $model->getAttr('email'),//邮箱
            'user_type' => $type,
            'user_type_name' => UserModel::getPropInfo('fields_type',$type,'name'),
            'my_req_code' => (string)$model->getAttr('req_code'),


        ];

    }

}
