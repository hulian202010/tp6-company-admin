<?php
namespace app\api\controller;


use app\common\model\GoodsCateModel;
use app\common\model\GoodsModel;
use app\common\model\ImageModel;
use app\common\model\SysSettingModel;
use app\common\model\UserCartModel;
use app\common\model\UserCollGoodsModel;
use app\common\service\Config;
use app\common\service\WechatV3Pay;

class Goods extends Common
{
    protected $open_action_validate = true;

    protected $ignore_action=['cate','lists','detail','cartlists','colllist'];

    public function cate()
    {
        $input_data = input();
        $input_data['status']=1;
        $list =[];
        GoodsCateModel::getAllCate($input_data)->each(function($item,$index)use(&$list){
            $info = $item->apiNormalInfo();
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if(!empty($linkChild)){
                foreach ($linkChild as $vo){
                    array_push($child_list,$vo->apiNormalInfo());
                }
            }
            $info['child_list'] = $child_list;
            array_push($list,$info);
        });
        return $this->_resData(1,'获取成功',[
            'list' => $list,
        ]);
    }

    public function lists()
    {
        $input_data = input();
        $input_data['state']='up'; //上架状态
        $list =[];
        $info = GoodsModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });

        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }

    public function detail()
    {
        $id = input('id',0,'intval');
        $sku_id = input('sku_id',0,'intval');
        $detail = [];
        GoodsModel::getPageData(['id'=>$id,'sku_id'=>$sku_id])->each(function($item,$index)use(&$detail,$sku_id){
            $item->sku_id = $sku_id; //设置sku_id
            $detail = $item->apiDetailInfo();
        });

        //验证是否收藏
        $is_coll = empty($this->user_id) ? 0 : UserCollGoodsModel::where(['uid'=>$this->user_id,'gid'=>$id])->value('coll_time');

//        dump($detail);exit;
        return $this->_resData(1,'获取成功',[
            'is_coll' => empty($is_coll) ? 0 : 1,
            'detail' => $detail,
            'custom_tel' => SysSettingModel::getContent('normal','custom_tel'),//客服电话
        ]);
    }


    public function cartLists()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $input_data['handle'] = 1;

        $num=0;
        $all_data = UserCartModel::getMchAllData($input_data);



        return $this->_resData(1,'获取成功',[
            'list' => $all_data,
            "num"=>UserCartModel::getNum($this->user_id),
        ]);
    }





    //添加购物车
    public function cartAdd()
    {
        try{
            UserCartModel::cartAdd($this->user_model,input());

        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'添加成功',[
            'num' => UserCartModel::getNum($this->user_id),
        ]);
    }

    public function cartDel()
    {
        try{
            UserCartModel::cartDel($this->user_model,input());

        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'num' => UserCartModel::getNum($this->user_id),
        ]);
    }
    public function cartModInfo()
    {
        try{
            UserCartModel::selfModInfo($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'num' => UserCartModel::getNum($this->user_id),
        ]);
    }


    public function coll()
    {
        try{
            $is_coll = UserCollGoodsModel::coll($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,$is_coll?'收藏成功':'取消收藏',[
            'is_coll' => $is_coll,
        ]);
    }



    //产品收藏
    public function collList()
    {
        $lists = [];
        $info = UserCollGoodsModel::getPageData(['uid'=>$this->user_id,'limit'=>10])->each(function($item,$index)use(&$lists){
            array_push($lists,$item->apiNormalInfo());
        });
        $result['list'] = $lists;

        $result['total_page'] = $info->lastPage();
        return $this->_resData(1,'获取成功',$result);
    }
}
