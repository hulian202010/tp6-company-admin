<?php
namespace app\common\service;


use app\common\model\SmsModel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class AliSms
{
    const URL = 'https://dysmsapi.aliyuncs.com/';
    const SignName = '';

    const KEYID = '';
    const KEYSECRET = '';

    const TEMPLATECODE        = '';  //短信验证码
    const TEMPLATECODE_ACTION = '';  //验证码--场景

    const PRODUCT = '';
    const REGION = "cn-";


    /**
     * 发送短信验证码
     * @param $code
     * @param $mobile
     * @return
     */
    public static function send($code, $mobile)
    {
        return self::smsSend($mobile,self::TEMPLATECODE,"{'code':${code}}");

    }

    /**
     * 发送短信验证码-场景
     * @param $code
     * @param $action
     * @param $mobile
     * @return
     */
    public static function sendAction($code,$action, $mobile)
    {
        return self::smsSend($mobile,self::TEMPLATECODE_ACTION,"{'code':${code},'action':'${action}'}");

    }

    //处理发送请求
    public static function smsSend($mobile,$template_code,$param="",$is_record=false)
    {
        try {
            $method = 'GET';
            $req_data=self::_sign($mobile,$param,$template_code,'sendSms',$method);
            $client = new Client();
            $url = self::URL.'?';
            foreach ($req_data as $key=>$vo){
                $url .=$key.'='.$vo.'&';
            }
            $response = $client->request($method,$url,[
                'verify'=>false,

            ]);
//            $result = HttpCurl::req(self::URL,$req_data,'POST');
            $result = $response->getBody()->getContents();
        }catch (RequestException $e) {
            $err_json = $e->getResponse()->getBody()->getContents();
            $err_json = empty($err_json)?[]:json_decode($err_json,true);
            // 进行错误处理
            throw new \Exception($err_json['Message']??'信息发送异常,请联系管理员');

        } catch (\Exception $e) {
            // 进行错误处理
            throw new \Exception($e->getMessage());
        }
        //记录短信
        if(is_numeric($is_record)){
            SmsModel::insert([
                'type' => $is_record,
                'phone' => $mobile,
                'create_time' => time(),
                'update_time' => time(),
                'info' => $result,
            ]);
        }

        return $result;
    }


    private static function _sign($mobile,$tmp_param="",$tmp_code = self::TEMPLATECODE,$action='sendSms',$method='POST')
    {
        $method = strtoupper($method);
        $req_data = [
            'Action' => $action,
            'AccessKeyId' => self::KEYID,
            'PhoneNumbers' => (string)$mobile,
            'SignName' => self::SignName,
            'TemplateCode' => $tmp_code,
            'RegionId' => 'cn-hangzhou',
            'SignatureMethod' => 'HMAC-SHA1',    //签名方式
            'SignatureNonce' => uniqid(),     //签名唯一随机数。用于防止网络重放攻击，建议您每一次请求都使用不同的随机数。
            'SignatureVersion' => '1.0',     //签名唯一随机数。用于防止网络重放攻击，建议您每一次请求都使用不同的随机数。
            'Timestamp' => gmdate("Y-m-d\TH:i:s\Z", time()),     //请求的时间戳。按照ISO8601 标准表示，并需要使用UTC时间，格式为yyyy-MM-ddTHH:mm:ssZ
            'Version' => '2017-05-25',     //API 的版本号
            'Format' => 'JSON',     //API 的版本号
        ];
//        dump($req_data);exit;

        if(!empty($tmp_param)){
            $req_data['TemplateParam'] = $tmp_param;
        }
        ksort($req_data);

        $str = "";
        foreach ($req_data as $key=>$vo){
            $str .= $key.'='.urlencode($vo).'&';
        }
        //组合数据
        // AccessKeyId=testId&Action=SendSms&Format=XML&OutId=123&PhoneNumbers=15300000001&RegionId=cn-hangzhou&SignName=阿里云短信测试专用&SignatureMethod=HMAC-SHA1&SignatureNonce=45e25e9b-0a6f-4070-8c85-2956eda1b466&SignatureVersion=1.0&TemplateCode=SMS_71390007&TemplateParam={"customer":"test"}&Timestamp=2017-07-12T02:42:19Z&Version=2017-05-25
        $str = mb_substr($str,0,-1);
        //再次组合--GET&%2F&AccessKeyId%3DtestId%26Action%3DSendSms%26Format%3DXML%26OutId%3D123%26PhoneNumbers%3D15300000001%26RegionId%3Dcn-hangzhou%26SignName%3D%25E9%2598%25BF%25E9%2587%258C%25E4%25BA%2591%25E7%259F%25AD%25E4%25BF%25A1%25E6%25B5%258B%25E8%25AF%2595%25E4%25B8%2593%25E7%2594%25A8%26SignatureMethod%3DHMAC-SHA1%26SignatureNonce%3D45e25e9b-0a6f-4070-8c85-2956eda1b466%26SignatureVersion%3D1.0%26TemplateCode%3DSMS_71390007%26TemplateParam%3D%257B%2522customer%2522%253A%2522test%2522%257D%26Timestamp%3D2017-07-12T02%253A42%253A19Z%26Version%3D2017-05-25
        $str = str_replace(["+","*","%7E"],["%20","%2A","~"],$str);
        $str = $method.'&%2F&'.urlencode($str);
        $str = str_replace(["+","*","%7E"],["%20","%2A","~"],$str);
        //签名数据-----类似  zJDF+Lrzhj/ThnlvIToysFRq6t4=
        $sign_key = self::KEYSECRET.'&';
//        $sign_key = 'nZV8hRXWKVuXLpdjarLvRU5Y7tcFmf&';
        $sign_str = base64_encode(hash_hmac("sha1",$str,$sign_key,true));
        $req_data['Signature'] = $sign_str;

        return $req_data;
    }
}