<?php
namespace app\common\service;

use app\common\model\BaseModel;
use app\common\model\OrderModel;
use app\common\service\AesUtil;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use think\Model;
!defined('CURLINFO_APPCONNECT_TIME') && define ('CURLINFO_APPCONNECT_TIME',3145761);
class WechatV3Pay
{

    public static function app(\think\Model $model)
    {
        $origin = 'wechat_app'; //支付来源
        list($prepay_id,$timestamp,$nonce) = self::_handleData($origin,$model,'https://api.mch.weixin.qq.com/v3/pay/transactions/app');

//        $message = Config::payWay($origin,'config','app_id')."\n".
//            $timestamp."\n".
//            $nonce."\n".
//            $prepay_id."\n";
        $result_data = array(
            'appid'  => Config::payWay($origin,'config','app_id'),
            'partnerid'  => Config::wechat('wx_mch','mch_id'),
            'timestamp' => $timestamp.'',
            'noncestr' => $nonce,
            'prepayid' => $prepay_id,
            'package' => 'Sign=WXPay',
        );


        ksort($result_data,SORT_STRING);
        $str = '';
        foreach($result_data as $key=>$vo){
            $str.=$key.'='.$vo.'&';
        }
        $str.='key='.Config::wechat('wx_mch','key');
        $sign = strtoupper(md5($str));

        $result_data['sign'] =$sign;

        return $result_data;
    }


    /**
     * jsapi支付
     * @param \think\Model $model 订单对象
     * @param $open_id string 微信openid
     * @param $origin string 支付来源
     * @return array
     * @throws \Exception
     */
    public static function jsapi( Model $model, $open_id,$origin = 'wechat_mini')
    {

        list($prepay_id,$timestamp,$nonce) = self::_handleData($origin,$model,'https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi',[
            "payer"=>[
                "openid"=> $open_id
            ]
        ]);
        $message = Config::payWay($origin,'config','app_id')."\n".
            $timestamp."\n".
            $nonce."\n".
            'prepay_id='.$prepay_id."\n";
        $result_data = array(
            'appId'  => Config::payWay($origin,'config','app_id'),
            'timeStamp' => $timestamp.'',
            'nonceStr' => $nonce,
            'package' => 'prepay_id='.$prepay_id,
        );
//        $sign_data = array_values($result_data);
//        dump($message);exit;
        $mch_private_key = file_get_contents(root_path('app/common/service/wechat_cert').'apiclient_key.pem');
        openssl_sign($message, $raw_sign, $mch_private_key, 'sha256WithRSAEncryption');
        $sign = base64_encode($raw_sign);
        $result_data['signType'] = 'RSA';
        $result_data['paySign'] =$sign;
        return $result_data;
    }

    //Native下单API
    public static function native(Model $model)
    {
        $origin = 'wechat_app'; //支付来源
        list($code_url,$timestamp,$nonce) = self::_handleData($origin,$model,'https://api.mch.weixin.qq.com/v3/pay/transactions/native');
        return $code_url;
    }
    //Native下单API
    public static function h5(Model $model)
    {
        $origin = 'wechat_app'; //支付来源
        list($h5_url,$timestamp,$nonce) = self::_handleData($origin,$model,'https://api.mch.weixin.qq.com/v3/pay/transactions/h5',[
            'scene_info' => [
                'payer_client_ip'  =>  app()->request->ip(),
                'store_info'  =>  [
                    'id' => empty($model['mch_id'])?'000':$model['mch_id'],
                ],
                'h5_info'  =>  [
                    'type' => 'h5_info_type',
                ],
            ],
        ]);
        return $h5_url;
    }


    //退款流程
    public static function refund(BaseModel $model)
    {

        if($model['pay_way']!=2 || !$model['pay_time']){
            return false;
        }
        $pay_info = $model->getAttr('pay_info');
        if(empty($pay_info)){
            return false;
        }
        $pay_info = $pay_info['resource'];
//        dump($pay_info);exit;

        $out_refund_no = "sdkphp".date("YmdHis");
        $amount = $pay_info['amount'];
        $payer_total = $amount['payer_total'];
        //支付金额
//        $amount = $model['pay_money']*100;
//        $config = self::configInstance($this->wx_config_field);
        $params = [
            "appid"=>  $pay_info['appid'],
            "mch_id"=> $pay_info['mchid'],
            "out_trade_no"=> $model['no'],
            "out_refund_no"=> $out_refund_no,
            "total_fee" => $payer_total,
            "refund_fee" => $payer_total,

        ];
        $url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
        //获取签名信息
        list($token,$nonce,$timestamp) = \app\common\service\WechatV3Pay::authorization($url,'POST',$params);

        $params['nonce_str'] = $nonce;
        ksort($params,SORT_STRING);
        $str = '';
        foreach($params as $key=>$vo){
            $str.=$key.'='.$vo.'&';
        }
        $str.='key='.Config::wechat('wx_mch','key');
//        dump($str);
        $sign = strtoupper(md5($str));

        $params['sign'] =$sign;

//        dump($params);exit;

        try {
//            dump($token);exit;
            $stack = \GuzzleHttp\HandlerStack::create();
            $client = new \GuzzleHttp\Client(['handler' => $stack]);

            $resp = $client->request('POST', $url, [
                'body' => arr2xml($params),
                'verify'=>false,
                'cert'=>root_path().'/app/common/service/wechat_cert/apiclient_cert.pem',
                'ssl_key'=>root_path().'/app/common/service/wechat_cert/apiclient_key.pem',
                'headers' => [
                    'Authorization'=>'WECHATPAY2-SHA256-RSA2048 '.$token,
                    'Accept' => 'application/json',
                ]
            ]);
//            dump(arr2xml($params),$resp->getBody()->getContents());exit;
            $result = $resp->getBody()->getContents();
        }catch (RequestException $e) {
//            dump($e);exit;
            $result = $e->getResponse()->getBody()->getContents();
        } catch (\Exception $e) {
            $result=$e->getMessage();
            // 进行错误处理
//            throw new \Exception($e->getMessage());

        }

        $model->setAttrs([
            'refund_no'=>$out_refund_no,
            'refund_time'=>time(),
            'refund_info'=>$result,
        ]);
        $model->save();
    }


    //支付回调
    public static function notify(array $input_data = [])
    {
//        {"id":"6b3e2f7d-a0fe-5cc3-b428-fa7f2d298d84","create_time":"2020-12-16T22:35:26+08:00","resource_type":"encrypt-resource","event_type":"TRANSACTION.SUCCESS","summary":"\u652f\u4ed8\u6210\u529f","resource":{"id":"4200000838202012161259820854","appid":"wx9d46f2b28267bc01","mchid":"123334065","out_trade_no":"202012164025005","payer":{"openid":"ohsjd4g3omol2BcC-hz2CbK4OzxY"},"amount":{"total":12192,"currency":"HKD","payer_total":10275,"payer_currency":"CNY","exchange_rate":{"type":"SETTLEMENT_RATE","rate":100000000}},"trade_type":"JSAPI","trade_state":"SUCCESS","trade_state_desc":"\u652f\u4ed8\u6210\u529f","bank_type":"OTHERS","attach":"order_id=2910&mode=wechat","success_time":"2020-12-16T22:35:26+08:00"}}
        $resource = $input_data['resource']??[];
//        dump($resource);exit;
        $AesUtil = new AesUtil(Config::wechat('wx_mch','key_v3'));
        $associatedData=$resource['associated_data']??'';
        $nonceStr=$resource['nonce']??'';
        $ciphertext  = $resource['ciphertext']??'';
        $json = $AesUtil->decryptToString($associatedData,$nonceStr,$ciphertext);
        $data = json_decode($json,true);

        if($data){
            $input_data['resource'] = $data;
            $attach = explode('&',$data['attach']);
            $origin = $attach[0];
            $order_no = $data["out_trade_no"];
            $order_no_first = substr($order_no,0,1);

            OrderModel::handleNotify($data["out_trade_no"],$input_data,2,$origin);

            return ['code'=>'SUCCESS','message'=>''];
        }else{
            return ['code'=>'DECRYPT_ERROR','message'=>'解密失败'];
        }

    }


    //支付信息
    private static function _handleData($origin,Model $model,$url,array $init_param = [])
    {
        $pay_info = $model->getPayInfo($origin,'wechat');

        $params = array_merge($init_param,[
            "appid"=> Config::payWay($origin,'config','app_id'),
            "mchid"=> Config::wechat('wx_mch','mch_id'),
            "description" => $pay_info['body']??config('app.app_name'),
            "out_trade_no" => $pay_info['no'],
            "time_expire" => gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()+$pay_info['expire_time']),
            "attach"=> $pay_info['attach'],
            "notify_url"=> $pay_info['notify_url'],
            "goods_tag"=> $pay_info['goods_tag']??'goods_tag',
            "amount"=> [
                "total"=>intval($pay_info['pay_money']*100),
                "currency"=> Config::wechat('wx_mch','amount_currency')
            ]
        ]);
//        dump($params);exit;
        //获取签名信息
        list($token,$nonce,$timestamp) = WechatV3Pay::authorization($url,'POST',$params);
        try {

            $client = new \GuzzleHttp\Client();
            $resp = $client->request('POST', $url, [
                'json' => $params,
                'verify'=>false,
                'headers' => [
                    'Authorization'=>'WECHATPAY2-SHA256-RSA2048 '.$token,
                    'Accept' => 'application/json',
                ]
            ]);
            $result = $resp->getBody()->getContents();
        }catch (RequestException $e) {
            $err_json = $e->getResponse()->getBody()->getContents();
            $err_json = empty($err_json)?[]:json_decode($err_json,true);
            // 进行错误处理
            throw new \Exception($err_json['message']??'支付信息异常,请联系管理员');

        } catch (\Exception $e) {
            // 进行错误处理
            throw new \Exception($e->getMessage());
        }
        $result = empty($result)?[]:json_decode($result,true);
        $info = $result[key($result)]??'';
        return [$info,$timestamp,$nonce];
    }

    public static function auth(){
        $url = 'https://api.mch.weixin.qq.com/v3/certificates';
        list($token) = self::authorization($url,'GET',[]);

        try{
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url, [
                'verify'=>false,
                'headers' => [
                    'Authorization'=>'WECHATPAY2-SHA256-RSA2048 '.$token,
                    'Accept' => 'application/json',
                ]
            ]);
            $result = $response->getBody()->getContents(); // '{"id": 1420053, "name": "guzzle", ...}'
        }catch (RequestException $e){
            $result = $e->getResponse()->getBody()->getContents();
        }


//        echo $response->getStatusCode(); // 200
//        echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
        $result = empty($result)?[]:json_decode($result,true);
        if(!empty($result['code'])){
            throw new \Exception($result['message']??'未知异常:请联系管理员');
        }
        return $result;

    }


    public static function authorization($url,$http_method,array $body=[]){
        // Authorization: <schema> <token>
        $timestamp = time();
        $nonce = $timestamp.'_'.sprintf("%03d", mt_rand(1,999));
        $body = empty($body)?'':json_encode($body);
        $url_parts = parse_url($url);
        $canonical_url = ($url_parts['path'] . (!empty($url_parts['query']) ? "?${url_parts['query']}" : ""));
        $message = $http_method."\n".
            $canonical_url."\n".
            $timestamp."\n".
            $nonce."\n".
            $body."\n";
        $mch_id = Config::wechat('wx_mch','mch_id');
        $mch_private_key = file_get_contents(root_path('app/common/service/wechat_cert').'apiclient_key.pem');
        $serial_no =  Config::wechat('wx_mch','serial_no');
        openssl_sign($message, $raw_sign, $mch_private_key, 'sha256WithRSAEncryption');
        $sign = base64_encode($raw_sign);

        $schema = 'WECHATPAY2-SHA256-RSA2048';
        $token = sprintf('mchid="%s",nonce_str="%s",timestamp="%d",serial_no="%s",signature="%s"',
            $mch_id, $nonce, $timestamp, $serial_no, $sign);
        return [$token,$nonce,$timestamp];
    }

}