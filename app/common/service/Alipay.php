<?php
namespace app\common\service;
use Alipay\EasySDK\Kernel\Factory;
use Alipay\EasySDK\Kernel\Config;
use app\common\model\BaseModel;
use app\common\model\OrderModel;
use app\common\model\OrderServiceTimeModel;
use app\common\service\Config as MyConfig;
!defined('CURLINFO_APPCONNECT_TIME') && define ('CURLINFO_APPCONNECT_TIME',3145761);
class Alipay
{

    public function __construct()
    {
        //1. 设置参数（全局只需设置一次）
        Factory::setOptions($this->getOptions());
    }

    public function appPay(BaseModel $model)
    {
        $pay_info = $model->getPayInfo('alipay','alipay');
        //2. 发起API调用（以支付能力下的统一收单交易创建接口为例）
//        $client=Factory::payment()->App()->asyncNotify($pay_info['notify_url']);
        $result = Factory::payment()->App()->pay($pay_info['body'], $pay_info['no'], $pay_info['pay_money']);
//        $responseChecker = new ResponseChecker();
//        //3. 处理响应或异常
//        if ($responseChecker->success($result)) {
//            echo "调用成功". PHP_EOL;
//        } else {
//            echo "调用失败，原因：". $result->msg."，".$result->subMsg.PHP_EOL;
//        }
        return $result->body;

    }

    public function wapPay(BaseModel $model)
    {
        $pay_info = $model->getPayInfo('alipay','alipay');
        //2. 发起API调用（以支付能力下的统一收单交易创建接口为例）
//        $client=Factory::payment()->App()->asyncNotify($pay_info['notify_url']);
        $result = Factory::payment()->Wap()->pay($pay_info['body'], $pay_info['no'], $pay_info['pay_money'], "", "");
//        $responseChecker = new ResponseChecker();
//        //3. 处理响应或异常
//        if ($responseChecker->success($result)) {
//            echo "调用成功". PHP_EOL;
//        } else {
//            echo "调用失败，原因：". $result->msg."，".$result->subMsg.PHP_EOL;
//        }
        return $result->body;

    }

    public function notify(array $data = [])
    {
        $bool = Factory::payment()->common()->verifyNotify($data);
        if($bool){
            $order_no = $data["out_trade_no"];
            $order_no_first = substr($order_no,0,1);
            $order_no = $data["out_trade_no"];
            $check_field = 'no';
            if(strpos($order_no,'H')===0){
                $check_field = 'h_no';
                $order_no = mb_substr($order_no,1);
            }
            OrderModel::handleNotify($order_no,$data,'alipay',$check_field);

//            \app\common\model\OrderModel::handleNotify($data["out_trade_no"],$data,3);
        }else{

            trace('支付宝签验失败!相关数据:'.json_encode($_POST));
        }
    }


    //退款处理
    public function refund(OrderModel $model,$back_money=null)
    {
        if($model['pay_way']!=3 || !$model['pay_time']){
            return false;
        }
        //支付金额
        $amount = is_null($back_money) ? $model['pay_money'] : $back_money;

        $refund_info = empty($model['refund_info']) ? [] : json_decode($model['refund_info'],true);
        $backIntro = '';
        try{
            $result = Factory::payment()->common()->refund($model['no'],$amount);
            $result=$result->httpBody;
        }catch (\Exception $e){
            $result = $e->getMessage();
            $backIntro = $result;
        }
        array_push($refund_info,[
            'datetime'=>date('Y-m-d H:i:s'),
            'refund_info'=>$result,
        ]);
//        dump($result);exit;
        $model->setAttrs([
            'refund_no'=>"",
            'refund_time'=>date('Y-m-d H:i:s'),
            'refund_info'=>json_encode($refund_info,JSON_UNESCAPED_UNICODE),
        ]);
        $model->save();
        return $backIntro;
    }

    function getOptions()
    {
        $options = new Config();
        $options->protocol = 'https';
        $options->gatewayHost = 'openapi.alipay.com';
        $options->signType = 'RSA2';

        $options->appId = MyConfig::alipay('app_id');//'<-- 请填写您的AppId，例如：2019022663440152 -->';

        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        $options->merchantPrivateKey = MyConfig::alipay('rsa');//'<-- 请填写您的应用私钥，例如：MIIEvQIBADANB ... ... -->';

//        $options->alipayCertPath = '<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->';
//        $options->alipayRootCertPath = '<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt" -->';
//        $options->merchantCertPath = '<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->';

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
        $options->alipayPublicKey = MyConfig::alipay('pk');//'<-- 请填写您的支付宝公钥，例如：MIIBIjANBg... -->';

        //可设置异步通知接收服务地址（可选）
        $options->notifyUrl = url('/api/pay/alipayNotify',[],false,true)->build();

        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
//        $options->encryptKey = "<-- 请填写您的AES密钥，例如：aa4BtZ4tspm2wnXLb1ThQA== -->";



        return $options;
    }
}