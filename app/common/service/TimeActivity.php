<?php
namespace app\common\service;

use app\common\model\BaseModel;
use app\common\model\ChatUserInfoModel;
use app\common\model\SysSettingModel;
use app\common\model\UserModel;

//活动
class TimeActivity{

    //活动时间段
    public static function getTime()
    {
        return [
            ['name'=>'7点','start_date'=>'7:00','value'=>1,'hold_minute'=>120,'state'=>1],
            ['name'=>'10点','start_date'=>'10:00','value'=>2,'hold_minute'=>120,'state'=>1],
            ['name'=>'12点','start_date'=>'12:00','value'=>3,'hold_minute'=>120,'state'=>1],
            ['name'=>'14点','start_date'=>'14:00','value'=>4,'hold_minute'=>120,'state'=>1],
            ['name'=>'16点','start_date'=>'16:00','value'=>5,'hold_minute'=>120,'state'=>1],
            ['name'=>'18点','start_date'=>'18:00','value'=>6,'hold_minute'=>120,'state'=>1],
        ];
    }

    //获取所有时间
    public static function allInfo()
    {
        $data = [];
        foreach (self::getTime() as $vo){
            $info = self::getKillInfo($vo['start_date'],$vo['hold_minute'],$vo['state']);
            $info['value'] = $vo['value'];
            $info['name'] = $vo['name'];
            $data[] = $info;
        }
        return $data;
    }

    //获取正在进行得数据
    public static function getRunning($field=null)
    {
        $all_data = self::allInfo();
        $info = $all_data[0]??[];
        foreach ($all_data as $vo){
            if($vo['state']==2){
                $info = $vo;
                break;
            }
        }
        if(is_null($field)){
            return $info;
        }else{
            return $info[$field]??null;
        }
    }

    //判断是否正在进行中
    public static function isRunning($at)
    {
        $info = self::getRunning();
        $bool = 0;
        if(!empty($info)){
            if($info['value']==$at && $info['state']==2){
                $bool = 1;
            }
        }
        return $bool;
    }


    //秒杀信息
    public static function getKillInfo($kill_start_time,$hold_minute,$is_use)
    {
        $data = [
            'start_time' => '', //开始时间
            'end_time' => '', //结束时间
            'reduce_second' => 0, //剩余多少秒
            'name' => '',  //
            'desc' => '已关闭',  //活动desc
            'intro' => '',  //活动说明
            'state' => 0,  //活动状态 0-活动已关闭 1-未开始 2-进行中 3-已结束
        ];

//        $kill_start_time = SysSettingModel::getContent('normal','kill_start_time');
//        $hold_time = SysSettingModel::getContent('normal','kill_hold_time_int'); //持续时间
        if(empty($kill_start_time) || empty($hold_minute) || !check_date($kill_start_time)){
            $data['state']=0;
            $data['intro']='秒杀活动已关闭';
        }else{
            $start_time = date('H:i',strtotime($kill_start_time));
//            dump($start_time);
            //结束时间不得超过 00:00
            $end_day = date('d',strtotime('+'.$hold_minute.' minute',strtotime($start_time)));
            $next_day = date('m-d H:i',strtotime('+1 day',strtotime($start_time)));
            $end_time = date('H:i',strtotime('+'.$hold_minute.' minute',strtotime($start_time)));
            if($end_day>date('d')){//是否大于今天
                $end_time = '23:59';
            }
            $data['desc']='倒计时';

//            $data['name'] = $start_time.'点场'; //开始时间
            $data['start_time'] = $start_time; //开始时间
            $data['end_time'] = $end_time; //结束时间

            $current_time = date('H:i');
//            dump($end_time,$end_day,$hold_time,$current_time);exit;
            if($is_use && $current_time<$start_time){
                $data['intro']='即将开始';
                $data['reduce_second'] = strtotime($start_time)-time();
                $data['state'] =1 ;
                $data['name']='今日'.$start_time.'点开始';
            }elseif (!$is_use || $end_time<$current_time){
                $data['name'] = '将于 '.$start_time.' 开始'; //开始时间
                $data['state'] = 3 ;
                $data['intro']='明天开始';
            }else{
                $data['reduce_second'] = strtotime($end_time)-time();
                $data['state'] = 2 ;
                $data['intro']='进行中';
            }
        }
        return $data;
    }

}