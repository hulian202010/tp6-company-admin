<?php
namespace app\common\service;

use app\common\model\BaseModel;
use Qiniu\Auth;
use think\facade\Db;

class Upload
{
    const PREVIEW_VIEW = "http://qn.scsssc.cn/";
    const show_local_file = true;

    const OPEN_DB_SAVE = true;

    private $user_id;
    protected $fsizeLimit=0;//文件上传大小

    protected $need_login=false;

    protected static $use_qiniu_type = ['video'];

    private $root_path;
    public function __construct($user_id=0)
    {
        $this->user_id = $user_id;
        $this->root_path = root_path();
        $this->fsizeLimit = 1024*1024*10;
    }


    private static function qiniuInfo():string{
        $file_domain=self::PREVIEW_VIEW;

        $auth = new Auth("JHWhW3QqgK8N34G0MlUtR3cnLL8ebfFc32L_GV5G", "RXNAwbXKPX9oam6Fr8dvrzrumbT63aPggmVGcSIn");
        $save_key = '/$(x:img_type)/$(year)$(mon)$(day)/$(etag)$(ext)';
        $body = '{"code":1,"msg":"上传成功","data":{"key":"'.self::previewUrl().$save_key.'","type":"$(x:img_type)","mime_type":"$(mimeType)","hash":"$(etag)","fsize":$(fsize),"width":"$(imageInfo.width)","height":"$(imageInfo.height)","duration":"$(avinfo.video.duration)","v_width":"$(avinfo.video.width)","v_height":"$(avinfo.video.height)"}}';
        $policy = [
            'forceSaveKey'=>true,
            'saveKey' => $save_key,
            'returnBody' =>  $body,
        ];

        if(self::OPEN_DB_SAVE){ //开启回调
            $policy['callbackUrl'] =  request()->domain().'/api/upload/callback';
            $policy['callbackBody'] =  $body;
            $policy['callbackBodyType'] =  'application/json';
        }
//        dump($policy);exit;
        $key=null;
        $token = $auth->uploadToken("rysm-mall",$key,7200,$policy);
        return $token;
    }

    //获取上传凭证
    public static function info($type='image',array $params = [])
    {
        $return_json = $params['return_json']??false;
        $accept = $params['accept']??'images';
        $acceptMine = $params['acceptMine']??'images/*';
        $point_url = $params['url']??url('upload/upload',[],false,true).'?type='.$type;



        $data = [
            'accept'=>'images',
            'preview' => self::previewUrl(),
            'url' => $point_url,
            'acceptMine' => $acceptMine,
        ];
        $data['data']['token']='';
        if(!self::show_local_file){
            $data['data']['x:img_type'] = $type;
            $data['data']['token'] = self::qiniuInfo();
            $data['preview'] = self::PREVIEW_VIEW;
            $data['url'] = isset($params['url'])?$point_url:"https://up.qiniup.com";
        }

        if($accept=='video'){
            $data['accept'] = 'video';
        }else{
            $data['accept'] = 'file';
        }



        return $return_json?json($data)->getContent():$data;

    }


    //上传
    public function upload($type='image')
    {
        $upload_file_key=key($_FILES);
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file($upload_file_key);

        empty($file) && abort(0,'请选择上传文件');
        // 上传到本地服务器
        if($type=='apk'){
            $savename = \think\facade\Filesystem::putFileAs( 'android', $file,'android.apk');
        }elseif($type=='apk_mch'){
            $savename = \think\facade\Filesystem::putFileAs( 'android', $file,'android_mch.apk');
        }else{
            if(empty($file->getOriginalExtension())){
                $file_name = md5($file->getOriginalName()).'.png';
                $savename = \think\facade\Filesystem::putFileAs( $type, $file,$file_name);
            }else{
                $savename = \think\facade\Filesystem::putFile( $type, $file);
            }
        }
        $savename = str_replace('\\',"/",$savename);
        //上传路径
        $mime_type = $file->getOriginalMime();
        $key = self::previewUrl().'/uploads/'.$savename;
        $mime_type = stripos($mime_type,'image')!==false?'image':$mime_type;
        $data = [
            'key'=> $key,
            'mime_type' => $mime_type,
            'preview' => self::previewUrl(),
        ];

        //保存上传图片
        self::saveImage([
            'type' => $type,
            'key' => $key,
            'hash' => $file->hash(),
            'mime_type' => $mime_type,
            'fsize' => $file->getSize(),
        ]);

        return $data;
    }

    //保存图片
    public static function saveImage(array $input_data = [])
    {
        Db::table('sys_image_manager')->save([
            'type' => $input_data['type']??null,
            'mime_type' => $input_data['mime_type']??null,
            'key' => $input_data['key']??'',
            'hash' => $input_data['hash']??null,
            'fsize' => $input_data['fsize']??null,
            'width' => $input_data['width']??null,
            'height' => $input_data['height']??null,
            'duration' => $input_data['duration']??null,
            'v_width' => $input_data['v_width']??null,
            'v_height' => $input_data['v_height']??null,
            'status' => 0,
        ]);
    }

    public static function getUploadDomain()
    {
        $path = '';
        if(!self::show_local_file){
            $path = self::PREVIEW_VIEW;
        }
        return $path;
    }

    public static function previewUrl()
    {
        $url = request()->domain();
        if(!self::show_local_file){
            $url = self::PREVIEW_VIEW;
        }
        return $url;
    }
}