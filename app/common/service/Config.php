<?php
namespace app\common\service;
class Config
{

    public static function payWay($key=null,$field=null,$config_field=null)
    {
        $data = [
            'wechat_mini' => ['name'=>'小程序支付','conf_key'=>'wx_min'],
            'wechat_app' => ['name'=>'app微信支付','conf_key'=>'wx_open'],
            'wechat_web' => ['name'=>'微信h5支付','conf_key'=>'wx_web'],

            'alipay_app' => ['name'=>'app支付宝支付','conf_key'=>null],
            'alipay_web' => ['name'=>'支付宝h5支付','conf_key'=>null],
        ];
        if($field==='config' && !is_null($key)){
            $data = self::_getInfo($data,$key);
            $arr = explode('_',$key);
            if(count($arr)==2){
                $method = $arr[0];
                return self::$method($data['conf_key'],$config_field);
            }else{
                return $data;
            }
        }else{
            return self::_getInfo($data,$key,$field);
        }
    }


    //获取微信信息
    public static function wechat($key=null,$field=null)
    {
        $data = [
            //微信商户
            'wx_mch'=>[
                'key_v3'=>'o2TIuwyAvwTavKC45LGoqWkmqadDo28K',
                'key'=>'o2TIuwyAvwTavKC45LGoqWkmqadDo28K',
                'mch_id'=>'1604731277',
                'serial_no'=>'4F254AD36A7D6FBD0BE794DA5DF910A768F71DC6',
                'amount_currency'=>'CNY',

            ],
            //微信开放平台-网页
            'wx_web'=>[
                'app_id'=>'wx9d46f2b28267bc01',
                'app_secret'=>'05e2ce16e6fa4dcfadf1df14e675902d',
            ],
            //微信开放平台-小程序wx823d0cc692abf129-6c48df07dd5e98855e4c89a843f4187f
            'wx_min'=>[
                'app_id'=>'wx823d0cc692abf129',
                'app_secret'=>'6c48df07dd5e98855e4c89a843f4187f',
            ],
            //微信开放平台-开放平台
            'wx_open'=>[
                'app_id'=>'wxb20e6479c7a6db16',
                'app_secret'=>'e7213da93176948e045d37420b65df95',
            ],
        ];
        return self::_getInfo($data,$key,$field);
    }
    //获取微信信息
    public static function alipay($key=null,$field=null)
    {
        $data = [
            'geteway'=>'https://openapi.alipay.com/gateway.do',
            'app_id'=>'2021003142661057',
            'rsa'=>'MIIEowIBAAKCAQEAlYCm3wolYP+MgPX5mxNwRPCFH9emLtYmPipHQRhAL+9mVEZx+zKQo6g9oxFPhY6yTTPRe83FF25bAmIAhV/dOgtEQSXQKzVW4lWMCMQVVWe3hzz85NIkx41oNRT9UxTxJ6SGkAfu6tMYkJ9CiHmMWiHPpiQTnyG6cGgrlVlkYf6CZk1VVMHQlBvKEKMWykxyV0J1E3shshusAMFeV9MD9qbHkr49n7cnyrOLICg1JOEAQh+FF4uP9/uh1DA0qfy8gedgo16+l3pR3MJXNwKfDkL2V1/AfiYig2mjnKwGc/4loJZL7J1dwMNQKlzzQjhgyka81GsbViU3B3JrTs/YIQIDAQABAoIBAF6PXPy+Ah50BH6dFh+MD/rK1qjpUWdWdCHlw+PcXtzy4xpy5C0qMmoRQ8sca9viAczq9ZkE8aB8YHwFWKcDl33bGnTKYfyyZB4MVeJFg+jdDSww2xQwBqthOYQ5nnEqUc3tNl9rZSaKFoOC+CYyGx/3OsuvDeoc/zuR598zB1cjGABDLE7IOguIfy0O5yqGAyJDMSQ6xj0Ue6GbdWqPcb2ZZ+J91FobjYwz58UqFe3919XYE+eh72zNsPfoU71Qc0z8C/KrmGTDgJ4xtrKEDwEVitvFNSIhrOzxFHxiD6Aumaf0AQ+m4FLInyIS2+PNQx8AyuqLtJA8npvlhKBrQrECgYEAxfDyTsmIT9APr9wyc5CYmWZFx09sFG/ysiM500fyqyEB8rz3aFLXxEFEnxkty7SRIsL0iz6YhOhEysiL92wdxj7noS+jluWLMEYNHd55271SNOqDJ/0vw0z7eL06pOZ/Vs6+As/EZqzXgYrkKyWjxIzplR3rpBxU5Kp+1MM2Jd8CgYEAwVqIbTkoNKG+UdoJsptSlJXw7HdhX199oTeUGTeFEQpXWk62nDxyDeRhuwi4fAPdszs8AqsWuHYgiMzjIsyOTU6jZrnRK+Rz88DvpEjt8CuV8lq0UuTeqaCeCJmwmMAsNqgPtqYsY2Qzb00S96YlWnpbVwYTXqV87jzP0IMNwf8CgYEAkln8TlSj/J8Rei2M//VBXupHXX5ToyjCgQOalaT0v6LAFgGxy+i0r96yJhAv1BXF2bXbqbi2fE5fzq9JDb9apGGOUZcvlTpTF3Yz7Im5/8aTKZzbLI3+6p/YrwT6f0INlNl3lAyaYnAZRyayXVSPKZwmeKKOv+BSxF9rN1S+jHECgYB7CsqwfuP4AkmjutXbA/6cMYK8aAfd8l12/3BOT8dtHaCTBm4spEaMUp5isFz+BlPJ39MDe66cugdgJsoP9g5EYduX3n2eqeWj405cxFAuOv5T2KbR7XVIir6miG8Z9YjtSyuctsieg0A7/tcoI2G33w9LJMGkxDjpPmecsBlMLwKBgGNb+BuuqDNFp9jLCHfjnUMgqcTEvJ8Zqcc8tWgRf0XTh3wmdHsQXyAmxUI4rTaa4rnOwFoBypuU5PnKPc9vszFTb4T44Xl1Tg3TpjbUCAAGVHNTzB7gy3lio8vRw1pb1eiD1w1bbszraq2hXlcQmplj8IyBhjlBiKPIYFqu1yyI',
            'pk'=>'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAujLfMxrwqnJbENH7d95//IpqbcDMRb95+FdCcXKpiYgZKgaIU6Qbp7jzR5AAtWPfn1NRzP2gU91JfWFKYLTYssKftWa7idYZxKPIWHXDYLtokuQEfdGdYi/+euWdJw/bzz8aA/e+Yifa7TLamNiSraWUhZDJCr+ucls5jNIzCvyIsxb2hjAEQTeDb9tS9BkVwCfegQfTeegtyOIvTOS1yDpdoK7JnuXw++wHZA8qQ94PWcoYumQE9krImFXz/BKAB8A/2trMmxKdWLbe+7/XKyJDsKbfJAR3jcfy0gd5y2WygTz6zF9gQ4W8zDO5m/wZYIR0HPMGWMtD59z21qBG6wIDAQAB',
            'yy_pk'=>'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYCm3wolYP+MgPX5mxNwRPCFH9emLtYmPipHQRhAL+9mVEZx+zKQo6g9oxFPhY6yTTPRe83FF25bAmIAhV/dOgtEQSXQKzVW4lWMCMQVVWe3hzz85NIkx41oNRT9UxTxJ6SGkAfu6tMYkJ9CiHmMWiHPpiQTnyG6cGgrlVlkYf6CZk1VVMHQlBvKEKMWykxyV0J1E3shshusAMFeV9MD9qbHkr49n7cnyrOLICg1JOEAQh+FF4uP9/uh1DA0qfy8gedgo16+l3pR3MJXNwKfDkL2V1/AfiYig2mjnKwGc/4loJZL7J1dwMNQKlzzQjhgyka81GsbViU3B3JrTs/YIQIDAQAB',
        ];
        return self::_getInfo($data,$key);
    }

    //推送配置
    public static function push($key=null,$field=null)
    {
        $data=[
            'user'=>[
                'app_id'=>'GwcB9ZaujY5ZnrVpyKjkL9',
                'app_key'=>'GuZLTRD1or8ndYmGdjUdY6',
                'app_secret'=>'BvHzVoJDEJ6u4a3MgHYPK8',
                'master_secret'=>'ijzL1AJoLf6wJywVTdtVy8',
            ],
            'mch'=>[
                'app_id'=>'PqN3GNp4in5FqsDKyEySy3',
                'app_key'=>'KYuowmh0V491UjiIgH5bL5',
                'app_secret'=>'937ZIK7VCN852jBDaT76P1',
                'master_secret'=>'PYYIcrVcTf82w7HDYrEe57',
            ]
        ];
        return self::_getInfo($data,$key,$field);
    }


    private static function _getInfo(array $data,$key=null,$field=null)
    {
        if(is_null($key)){
            return $data;
        }elseif(!isset($data[$key])){
            return false;
        }else{
            $info = $data[$key];
            if(is_null($field)){
                return $info;
            }else{
                return $info[$field];
            }
        }
    }

}