<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class SysSettingModel extends BaseModel
{
    const CACHE_PREFIX = 'cache_setting_sql';
    //数据库表名
    protected $table = 'sys_setting';

    protected $pk = 'type';

    public static $protocol_info = [
        'serviceProtocol'   =>['name'=>'用户与隐私协议'],
        'aboutProtocol'     => ['name'=>'关于我们'],
    ];

    //获取内容字段
    public static function getContent($type,$filed=null,$trance=true)
    {
        $cache_name= $type;
        if(!is_null($filed)){
            $cache_name= $type.'_'.$filed;
        }
        $content = cache($cache_name);
        if(empty($content)){
            $content = self::where('type',$cache_name)->value('content');
            //保存内容
            self::_cacheData($type, $cache_name, $content);
        }
        $content=self::_filterData($cache_name,$content,'get',$trance);
        return $content;
    }

    //获取内容字段
    public static function setContent($type,$content)
    {
        if(is_array($content)){
            if(is_numeric(key($content))){
                $content=self::_filterData($type,$content,'insert');
                self::_writeData($type,$content);
            }else{
                foreach ($content as $key=>$vo){
                    $pk_key = $type.'_'.$key;
                    $content=self::_filterData($pk_key,$vo,'insert');

                    self::_writeData($pk_key,$content);
                }
            }
        }else{
            $content=self::_filterData($type,$content,'insert');
            self::_writeData($type,$content);
        }
        self::_cacheclear($type);
    }

    //写入数据
    private static function _writeData($type,$content)
    {
        $model = self::where(['type'=>$type])->find();
        if(empty($model)){
            $model = new self;
        }
        $model->data([
            'type'=>$type,
            'content'=>$content,
            'update_time'=>time(),
        ],true);
        $model->save();
        //清空缓存
        \think\facade\Cache::delete($type); ;
    }

    private static function _filterData($name,$value,$opt_type = 'get',$trance=false)
    {
        $arr = explode('_',$name);
        $type = end($arr);
        if($opt_type=='get'){
            if($trance && $type=='array'){
                $value = empty($value)?[]:explode(',',$value);
            }elseif($trance && $type=='json'){
                $value = empty($value)?[]:json_decode($value,true);
            }elseif($trance && $type == 'comma'){ //逗号
                $value = empty($value)?[]:$value;
                $value  = explode(',',$value);
            }elseif($trance && $type == 'enter'){//回车
                $value  = explode("\n",$value);
            }elseif($type == 'int'){//
                $value  = (int)$value;
            }
        }else{
            if($type=='int'){
                $value = intval($value);
            }elseif($type=='float'){
                $value = floatval($value);
            }elseif($type =='array'){
                $value = empty($value)?[]:$value;
                $value  = implode(',',$value);
            }elseif($type == 'json'){
                $value = empty($value)?[]:$value;
                $value  = json_encode($value);
            }
        }
        return $value;
    }

    //记录缓存
    private static function _cacheData($tag,$name,$content)
    {
        \think\facade\Cache::tag($tag)->set($name,$content,3600);
    }

    //删除缓存
    private static function _cacheclear($tag)
    {
        \think\facade\Cache::tag($tag)->clear();
    }

    
}