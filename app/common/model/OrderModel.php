<?php
namespace app\common\model;



use app\common\service\ExpressBird;
use app\common\service\third\Alipay;
use app\common\service\third\Wechat;
use think\Collection;
use think\console\command\make\Subscribe;
use think\facade\Db;
use think\model\concern\SoftDelete;
use think\Paginator;


class OrderModel extends BaseModel
{
    use SoftDelete;
    /**
     * @var UserModel
     * */
    public static $user_model=null;


    protected $table='order';

    public static $fields_type = [
        ['name'=>'普通订单'],
    ];


    //订单自动完成日期
    const ORDER_AUTO_COMPLETE_DAY = 7;
    //订单有效时间--有效时间为2小时  单位秒
    const ORDER_EXP_TIME = 7200;
    //退款限制时长
    const ORDER_ORDER_BACK_TIME = 864000;


    //用户可操作常量
//    const U_ORDER_HANDLE_PAY = 'pay';           //订单支付
//    const U_ORDER_HANDLE_CANCEL = 'cancel';     //取消订单
//    const U_ORDER_HANDLE_DEL = 'del';           //删除订单
//    const U_ORDER_HANDLE_SURE_REC = 'receive'; //确认收货
//    const U_ORDER_HANDLE_JOIN_TEAM_ORDER = 'join_team_order'; //加入团购
//    const U_ORDER_HANDLE_comment = 'comment'; //评论
//    const U_ORDER_HANDLE_BACK = 'back'; //退款
//    const U_ORDER_HANDLE_REMINDER = 'reminder'; //提醒
//    const U_ORDER_HANDLE_COMPLETE = 'sure_customer'; //确定消费


    //管理员操作
    const ORDER_HANDLE_PAY = 'pay';           //订单支付
    const ORDER_HANDLE_SURE_REC = 'receive'; //确认收货
    const ORDER_HANDLE_comment = 'comment'; //评论
    const ORDER_HANDLE_SURE_PAY = 'sure-pay';      //确定支付
    const ORDER_HANDLE_REMINDER = 'reminder'; //提醒
    const ORDER_HANDLE_SEND = 'send';         //发送
    const ORDER_HANDLE_MOD_ADDR = 'mod-addr';         //修改物流地址
    const ORDER_HANDLE_DEL = 'del';           //删除
    const ORDER_HANDLE_CANCEL = 'cancel';     //取消
    const ORDER_HANDLE_EDIT_ADDR = 'edit-addr';     //编辑订单地址
    const ORDER_HANDLE_COMPLETE = 'complete';     //订单完成

    //退款
    const ORDER_BACK_START = 'back';
    const ORDER_BACK_detail = 'back_detail';




    protected $json=['invoice_content','pay_info'];



    //支付方式
    public static $fields_pay_way = [
        ['name'=>'余额支付','value'=>1,'icon'=>'money-circle-fill','color'=>'#18B566' ,'img'=>'/assets/pc/images/pay00.jpg','is_hide'=>0],
//        ['name'=>'微信支付','value'=>2,'icon'=>'weixin-circle-fill','color'=>'#18B566' ,'img'=>'/assets/pc/images/pay02.jpg','is_hide'=>0],
        ['name'=>'支付宝支付','value'=>3,'icon'=>'zhifubao-circle-fill','color'=>'#0f3790' ,'img'=>'/assets/pc/images/pay01.jpg','is_hide'=>0],
    ];
    //发货状态
    public static $fields_is_send = [
        ['name'=>'待发货','color'=>'primary' ,'u_handle'=>[self::ORDER_HANDLE_REMINDER,self::ORDER_HANDLE_CANCEL],'m_handle'=>[ self::ORDER_HANDLE_MOD_ADDR,self::ORDER_HANDLE_SEND, self::ORDER_HANDLE_COMPLETE]],
        ['name'=>'已发货','color'=>'primary' ],
    ];
    //收货状态
    public static $fields_is_receive = [
        ['name'=>'待收货','color'=>'success','u_handle'=>[ self::ORDER_HANDLE_SURE_REC ],'m_handle'=>[self::ORDER_HANDLE_COMPLETE]],
        ['name'=>'已收货','color'=>'success'],
    ];
    //收货状态
    public static $fields_is_comment = [
        ['name'=>'待评论','color'=>'success','u_handle'=>[ self::ORDER_HANDLE_comment ]],
        ['name'=>'已评论','color'=>'success'],
    ];


//    public static $fields_status = [
//        ['name'=>'待付款','color'=>'warning','u_handle'=>[
//            self::ORDER_HANDLE_PAY,
//            self::ORDER_HANDLE_CANCEL,
//        ],'m_handle'=>[
//            self::ORDER_HANDLE_SURE_PAY,
//            self::ORDER_HANDLE_EDIT_ADDR,
//            self::ORDER_HANDLE_CANCEL,
//        ]],
//        ['name'=>'已付款','color'=>'success','u_handle'=>[
//            self::ORDER_HANDLE_SURE_REC=>['is_send'=>1]
//        ],'m_handle'=>[ self::ORDER_HANDLE_SEND=>['is_send'=>0]]],
//        ['name'=>'已取消','color'=>'info','u_handle'=>[self::ORDER_HANDLE_DEL],'m_handle'=>[self::ORDER_HANDLE_DEL]],
//        ['name'=>'已完成','color'=>'success','u_handle'=>[self::ORDER_HANDLE_comment=>['is_comment'=>0]],'m_handle'=>[]],
//
//    ];
    protected $step_flow = [
        ['name'=>'待支付','prop_func'=>'fields_status','field'=>'status'],
        ['name'=>'发货流程','prop_func'=>'fields_is_send','field'=>'is_send'],
        ['name'=>'收货流程','prop_func'=>'fields_is_receive','field'=>'is_receive'],
        ['name'=>'订单已完成','prop_func'=>'fields_status','field'=>'status'],
    ];

    public static function fields_status(){
        return [
            ['name'=>'待付款','color'=>'warning','u_handle'=>[
                self::ORDER_HANDLE_PAY,
                self::ORDER_HANDLE_CANCEL,
            ],'m_handle'=>[
                self::ORDER_HANDLE_SURE_PAY,
                self::ORDER_HANDLE_EDIT_ADDR,
                self::ORDER_HANDLE_CANCEL,
            ]],
            ['name'=>'已付款','color'=>'success','u_handle'=>[
                self::ORDER_HANDLE_SURE_REC=>['is_send'=>1]
            ],'m_handle'=>[ self::ORDER_HANDLE_SEND=>['is_send'=>0]]],
            ['name'=>'已取消','color'=>'info','u_handle'=>[self::ORDER_HANDLE_DEL],'m_handle'=>[self::ORDER_HANDLE_DEL]],
            ['name'=>'已完成','color'=>'success','u_handle'=>[
                self::ORDER_HANDLE_comment=>['is_comment'=>0],
                self::ORDER_BACK_START=>['complete_time'=>['>=',time()-self::ORDER_ORDER_BACK_TIME]]
            ],'m_handle'=>[]],

        ];
    }


    /**
     * 订单状态条件
     * @param $state int 订单状态
     * @return
     * */
    public static function getStateWhere($state)
    {
        $where=[];
        if ($state == 1) {
            //待付款
            $where[] =['step_flow','=',0];
            $where[] =['status','=',0];
        } elseif ($state == 2) {
            //已付款
            $where[] = ['pay_time','>',0];
        } elseif ($state == 3) {
            //待发货
            $where[] =['step_flow','=',1];
            $where[] =['status','=',1];
            $where[] =['is_send','=',0];
        }  elseif ($state == 4) {
            //已发货
            $where[] =['step_flow','=',2];
            $where[] =['status','=',1];
            $where[] =['is_send','=',1];
        } elseif ($state == 5) {
            //待收货
            $where[] =['step_flow','=',2];
            $where[] =['status','=',1];
            $where[] =['is_send','=',1];
            $where[] =['is_receive','=',0];
        } elseif ($state == 6) {
            //已完成
            $where[] =['step_flow','=',3];
            $where[] =['status','=',3];
        } elseif ($state == 7) {
            //待评价
            $where[] =['step_flow','=',3];
            $where[] =['status','=',3];
            $where[] =['is_comment','=',0];
        } elseif ($state == 8) {
            //可以退款列表
            $where[] =['step_flow','=',3];
            $where[] =['status','=',3];
            $where[] =['create_time','>',time()-self::ORDER_ORDER_BACK_TIME];
        } elseif ($state == 9) {
            //售后
            $where[] =['is_back','=',1];
        }  elseif ($state == 10) {
            //售后退款处理
            $where[] =['is_back_money','=',1];
        }elseif($state==11){
            //已支付
            $where[] =['is_pay','=',1];
        }elseif($state==12){
            //已取消
            $where[] =['status','=',2];
        }
        return $where;
    }



    //发货通知
    public static function onSendNoticeSend($model)
    {
        UserNoticeModel::recordLog('订单发货通知','订单号为:'.$model['no'].'已发货',$model['id'],$model['uid'],0,3);
//        UserPushIdentModel::sendPush($model['uid'],'订单发货通知','订单号为:'.$model['no'].'已发货');
    }
    //收货通知
    public static function onSendNoticeReceive($model)
    {
        UserNoticeModel::recordLog('订单完成通知','订单号为:'.$model['no'].'订单交易已完成.',$model['id'],$model['uid'],0,2);
//        UserPushIdentModel::sendPush($model['uid'],'订单完成通知','订单号为:'.$model['no'].'订单交易已完成.');

//        MchNoticeModel::recordLog('订单完成通知','订单号为:'.$model['no'].'订单交易已完成.',$model['id'],$model['mch_id'],0,1);
//        UserPushIdentModel::sendPush($model['mch_id'],'订单完成通知','订单号为:'.$model['no'].'订单交易已完成.'.$model['no'].'.',1);
    }


    //订单支付成功通知事件
    public static function onOrderPaySuccess($model)
    {

        //查询订单商品
        UserNoticeModel::recordLog('订单支付完成通知','订单号为:'.$model['no'].'订单交易已付款.',$model['id'],$model['uid'],0,1);
//        UserPushIdentModel::sendPush($model['uid'],'订单支付完成通知','订单号为:'.$model['no'].'订单交易已付款.');

//        MchNoticeModel::recordLog('用户下单通知','有用户下单,订单号为:'.$model['no'].'.',$model['id'],$model['mch_id'],0,1);
//        UserPushIdentModel::sendPush($model['mch_id'],'订单支付完成通知','有用户下单,订单号为:'.$model['no'].'.',1);
    }



    //订单返还处理事件
    public static function onOrderBackHandle($model)
    {
        if(empty($model->cancel_back_handle_time)){
            $user_model = UserModel::find($model['uid']);
            $change_integral = $user_model['integral'];
            $change_q_integral = $user_model['q_integral'];
            //更新订单状态
            $update_state = self::update(['cancel_back_handle_time'=>time()],[
                'id'=>$model['id'],
                'cancel_back_handle_time'=>$model['cancel_back_handle_time'],
            ]);
            if($update_state->getNumRows()){

                if($model['integral']>0){
                    //优惠券状态调整-返还优惠券
                    $integral = empty($model['integral'])?0:$model['integral'];
                    UserModel::update(['integral'=>\think\facade\Db::raw('integral+'.$integral)],['id'=>$model['uid']]);
                    //创建日志
                    UserLogsModel::recordData(2,$model['uid'],$integral,"订单取消返回使用积分",['q_money'=>$change_integral,'h_money'=>$change_integral+$integral]);
                }

                if($model['q_integral']>0  ){
                    //优惠券状态调整-返还优惠券
                    $q_integral = empty($model['q_integral'])?0:$model['q_integral'];
                    UserModel::update(['q_integral'=>\think\facade\Db::raw('q_integral+'.$q_integral)],['id'=>$model['uid']]);
                    //创建日志
                    UserLogsModel::recordData(3,$model['uid'],$q_integral,"订单取消返回使用券分",['q_money'=>$change_q_integral,'h_money'=>$change_q_integral+$q_integral]);
                }
                if($model['coupon_id']>0 ){
                    //优惠券状态调整-返还优惠券
                    CouponUserModel::update(['status'=>0],[
                        'id' => $model['coupon_id']
                    ]);
                }
            }


        }
    }


    //处理下单数据
    public static function handleOrderData(array $input_data=[])
    {
        $addr_id = $input_data['addr_id']??0;
        $user_id = empty(self::$user_model['id'])?0:self::$user_model['id'];

        //获取地址
        $model_addr = null;
        if($user_id){
            $addr_where = [];
            $addr_where[] = ['uid','=',$user_id];
            if(!empty($addr_id)){
                $addr_where[] = ['id','=',$addr_id];
            }
            $model_addr = UserAddrModel::where($addr_where)->find();
        }

        $goods_data = self::getGoodsData($input_data,true);

        foreach ($goods_data as &$vo){
            $merchant = $vo['mch_info']??[];
            $goods_list = $vo['goods_list']??[];
            //扩展数据
            $extra_info = [
                'total_goods_money' => 0,
                'total_buy_num' => 0,
                'total_freight_money' => 0,
                'total_platform_freight_money' => 0,
                'total_tax_money' => 0,
                'total_package_money' => 0,
                'total_commission_money' => 0,
                'has_platform_goods'=>0, //是否拥有平台商品
            ];

            foreach ($goods_list as $item){
                if(empty($extra_info['has_platform_goods']) && $item['platform_freight_money_state']){
                    $extra_info['has_platform_goods']=1;
                }

                $sold_price = empty($item['sold_price'])?0:$item['sold_price'];
                $buy_num = $item['buy_num']??1;
                //产品总额
                $extra_info['total_goods_money'] += $sold_price*$buy_num;
                $extra_info['total_buy_num'] += $buy_num;
                //运费
                $extra_info['total_freight_money'] += $item['freight_money']??0;
                //平台配送费用
                $extra_info['total_platform_freight_money'] += $item['platform_freight_money']??0;
                //税费
                $extra_info['total_tax_money'] += $item['tax_money']??0;
                //包装费
                $extra_info['total_package_money'] += ($item['package_money']??0)*$buy_num;
                //佣金
                $extra_info['total_commission_money'] += $item['commission_money']??0;
            }

            //绑定扩展数据
            $vo['extra_info'] = $extra_info;
        }

        return [$goods_data, $model_addr];

    }

    //下单商品处理--获取商品
    public static function getGoodsData($input_data )
    {
        //绑定用户模型
        GoodsModel::$user_model = self::$user_model;

        $user_id = empty(self::$user_model['id'])?0:self::$user_model['id'];
        $channel = $input_data['channel'] ?? '';//购买渠道

        //直接购买商品流程
        $goods_id = $input_data['goods_id']??0;
        $sku_id = $input_data['sku_id']??0;
        $buy_num = $input_data['num']??1;

        $goods_data = [];
        if(!empty($input_data['mch_id'])){//按商家购买

            $goods_data = UserCartModel::getMchAllData(['channel'=>$channel,'uid'=>$user_id,'mch_id'=>$input_data['mch_id']]);

        }elseif($channel=='cart'){
            $goods_data = UserCartModel::getMchAllData(['channel'=>$channel,'uid'=>$user_id]);
        }else{
            GoodsModel::getPageData(['id'=>$goods_id])->each(function($item)use(&$goods_data,$sku_id,$buy_num){
                //获取商家信息
                $merchant = $item->getRelation('linkMerchant');
                $mch_id = $merchant['id']??0;
                //绑定sku属性
                $item->sku_id = $sku_id;
                //购买数量
                $item->setAttr('buy_num',$buy_num);
                $key = 'mch_'.$mch_id;
                $goods_data[$key] = [
                    'mch_info' => $merchant,
                    'goods_list'=>[$item],
                ];
            });
        }

        return $goods_data;

    }

    /**
     * 获取商品数据
     * @param array $input_data 请求内容
     * @param string $select_mode 查询模式
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[],$select_mode="paginate")
    {

        $limit = $input_data['limit']??null;

        $where = $map_where = [];
        $where_fun = null;
        //模块
        if(app()->http->getName()!='admin'){

        }else{
            $type = $input_data['type']??0;
            $where[] = ['type','=',$type];
        }


        if(isset($input_data['mch_id'])){
            $where[]= ['mch_id','=',$input_data['mch_id']];
        }
        if(isset($input_data['id'])){
            $where[]= ['id','=',$input_data['id']];
        }

        if(isset($input_data['uid'])){
            if(empty($input_data['uid'])){
                return Paginator::make(null,1,1,0);
            }
            $where[] = ['uid' ,'=', $input_data['uid'] ];
        }

        $keyword = empty($input_data['keyword'])?'':trim($input_data['keyword']);
        if(!empty($keyword)){
            $where[] =  ['no','like','%'.$keyword.'%'];
        }

        $user_id = $input_data['user_id']??0;
        if(!empty($keyword)){
            $where[] =  ['uid','=', $user_id];
        }
        $active_state = $input_data['activeState']??'all';

        if($active_state=='cancel'){
            $where = array_merge($where,self::getStateWhere(12));
        }elseif($active_state=='wait_pay'){ //待付款
            $where = array_merge($where,self::getStateWhere(1));
        }elseif($active_state=='wait_send'){ //待发货
            $where = array_merge($where,self::getStateWhere(3));
        }elseif($active_state=='wait_rec'){ //待收货
            $where = array_merge($where,self::getStateWhere(5));
        }elseif($active_state=='wait_comment'){ //待评价
            $where = array_merge($where,self::getStateWhere(7));
        }elseif($active_state=='send'){ //已发货
            $where = array_merge($where,self::getStateWhere(4));
        }elseif($active_state=='complete'){ //已完成
            $where = array_merge($where,self::getStateWhere(6));
        }elseif($active_state=='back'){ //售后
            $where = array_merge($where,self::getStateWhere(9));
        }elseif($active_state=='refund'){ //售后退款
            $where = array_merge($where,self::getStateWhere(10));
        }elseif ($active_state=='wait_back'){ //待退款
            $where = array_merge($where,self::getStateWhere(9));
        }

        //事件范围查询
        if(!empty($input_data['search_date']) && is_array($input_data['search_date']) && count($input_data['search_date'])==2){
            $input_data['start_date'] = $input_data[0];
            $input_data['end_date'] = $input_data[1];
        }

        //按时间查询
        $start_date = empty($input_data['start_date'])?'':trim($input_data['start_date']);
        $start_time = empty($start_date)?'':strtotime($start_date);
        $end_date = empty($input_data['end_date'])?'':trim($input_data['end_date']);
        $end__time = empty($end_date)?'':strtotime('+1 day',strtotime($end_date));
        if(!empty($start_time) && !empty($end__time)){
            $where[] = ['create_time','>=',$start_time];
            $where[] = ['create_time','<=',$end__time];
        }elseif(!empty($start_time)){
            $where[] = ['create_time','>=',$start_time];
        }elseif(!empty($end__time)){
            $where[] = ['create_time','<=',$end__time];
        }

        //判断是否导出
        return self::with(['linkUser','linkAddr','linkGoods','linkLogistics'])
            ->where($where)
            ->where($where_fun)
            ->order("id desc")
            ->paginate($limit);
    }





    //确认订单
    public static function confirm(array $input_data = [])
    {


        $proxy_uid = $input_data['share_uid']??0;//分享用户
        $get_mode = $input_data['get_mode']??'';//收货方式
        $channel = $input_data['channel']??'';//渠道
        $use_integral = $input_data['use_integral']??0;//积分
        $coupon_id = $input_data['coupon_id']??0;//优惠券id
        if(empty(self::$user_model)) throw new \Exception('请先登录');

        list($goods_data,$model_addr) = self::handleOrderData($input_data);
//        dump($goods_data,$model_addr,$freight_money,$tax_money,$full_freight_money_dis);exit;

        if(empty($model_addr)) throw new \Exception('请选择收货地址');
        if(empty($goods_data) ) throw new \Exception('请前往选择商品后再下单');

        $remark = trim($input_data['remark']??'');

        $coupon_model = CouponUserModel::where(['uid'=>self::$user_model['id'],'id'=>$coupon_id])->find();
        //检测优惠券是否过期

        if(!empty($coupon_model) && !empty($coupon_model['status'])){
            throw new \Exception('优惠券已被使用,无法再次使用');
        }



        //改变前积分
        $change_integral = self::$user_model['integral'];

        $model_order_all = [];
        $is_pay=1;//前往支付地址
        $order_use_integral = 0; //订单使用积分
        try{
            \think\facade\Db::startTrans();

            $combined_no = "";
            //是否有平台配送产品
            //创建订单
            foreach ($goods_data as $key=>$vo) {

                $goods_list = $vo['goods_list'] ?? [];
                $extra_info = $vo['extra_info'] ?? []; //扩展信息


                $mch_info = empty($vo['mch_info']) ? null : $vo['mch_info']; //商家信息



                //检测购物车商品
                if (empty($goods_list)) {
                    throw new \Exception('请选择商品后下单');
                }

                $commission_money = $extra_info['total_commission_money'] ?? 0;
                $goods_total_money = $extra_info['total_goods_money'] ?? 0; //产品总金额


                //订单数据
                $model_order = new self();
                $model_order->setAttr('type', 0);


//                dump($extra_info);exit;
                $model_order->proxy_uid = $proxy_uid;
                $model_order->get_mode = $get_mode;

                $model_order->mch_id = empty($mch_info['id']) ? 0 : $mch_info['id'];
                $model_order->uid = self::$user_model['id'];
                $model_order->dis_money = 0;//总优惠金额
                $model_order->no = self::getDateNo('01');
                //取餐码
                $model_order->get_no = sprintf('%03d', self::generateNumber('01'));

                if (empty($combined_no)) { //绑定合单单号
                    $combined_no = 'H' . $model_order->no;
                }
                $model_order->hno = $combined_no;
                //金钱信息
                $model_order->goods_money = $goods_total_money;
                $model_order->tax_money = $extra_info['total_tax_money'] ?? 0; //税费

                $model_order->has_platform_goods = $extra_info['has_platform_goods'] ?? 0;//平台配送状态
                $model_order->platform_freight_money = $extra_info['total_platform_freight_money'] ?? 0;//平台配送费
                $model_order->freight_money = 0;//只有配送费，没运费
                $full_freight_money_dis = SysSettingModel::getContent('freight','full_freight_money_dis_float');
                if($full_freight_money_dis &&  $model_order->goods_money>=$full_freight_money_dis){
                    $model_order->full_freight_money = $full_freight_money_dis; //免运费
                    $model_order->freight_money = 0; //免运费
                }else{
                    $model_order->freight_money = $extra_info['total_freight_money']??0; //运费
                }
                //包装费
                $model_order->package_money = $extra_info['total_package_money'] ?? 0; //包装费


                $model_order->commission_money = empty($commission_money) ? 0.00 : $commission_money; //佣金
                $model_order->mch_money = 0.00; //商家获得金额
                $model_order->integral_money = 0.00; //积分抵现
                if ($model_order->get_mode > 0) {
                    $model_order->money = $goods_total_money + $model_order->package_money + $model_order->tax_money;
                } else {
                    //订单总金额
                    $model_order->money = $goods_total_money + $model_order->freight_money + $model_order->tax_money;
                }
//                dump($send_money,$model_order->getData());exit;
                $mch_get_money = $goods_total_money-$model_order->dis_money;
                $model_order->mch_money = empty($mch_get_money) || $mch_get_money<=0 ? 0 : $mch_get_money ;

                $pay_money = $model_order->money; //支付金额
                $model_order->coupon_dis_money = 0; //优惠券金额
                //验证优惠券是否可以使用  订单金额
                if (!empty($coupon_model)) {
                    if (!empty($coupon_model['full_money']) && $coupon_model['full_money'] > $model_order->money) {
                        throw new \Exception('该优惠券必须满' . $coupon_model['full_money'] . '元才能使用');
                    }
                    //扣取订单优惠金额
                    $model_order->coupon_id = $coupon_model['id'];
                    $model_order->coupon_dis_money = $coupon_model['money'];//优惠金额
                    $model_order->dis_money += $model_order->coupon_dis_money;//记录总优惠金额
                }
//                dump($model_order->dis_money);exit;
                //积分
                $integral_per = SysSettingModel::getContent('integral', 'per_float'); //积分抵现比
                if (!empty($integral_per) && $integral_per > 0 && $use_integral) {
                    if ($use_integral > $change_integral) {
                        throw new \Exception('账户积分不足,无法使用积分抵扣');
                    }

                    $integral_money = $integral_per * $use_integral;
                    $model_order->integral = $use_integral;//使用积分数量
                    $model_order->integral_money = $integral_money;//积分优惠金额
                    $model_order->dis_money += $integral_money;//积分优惠金额

                }

                //实际支付金额扣除优惠金额
                $pay_money = $pay_money - $model_order->dis_money;
//                dump($pay_money);exit;
                $model_order->pay_money = $pay_money <= 0 ? 0 : $pay_money;

                $model_order->total_num = $extra_info['total_buy_num'] ?? 0;

                //需要使用的积分



                $model_order->remark = empty($remark) ? (empty($vo['remark']) ? '' : $vo['remark']) : $remark;
                //保存订单信息
                $model_order->save();


                //积分抵扣
                if (!empty($model_order->integral)) {
                    $order_use_integral = $model_order->integral;
                    try {
                        $row_num = UserModel::where(['id' => self::$user_model['id'], 'integral' => self::$user_model['integral']])->update([
                            'integral' => Db::raw('integral-' . $model_order->integral)
                        ]);
                        if (empty($row_num)) throw new \Exception('操作频繁,请重新操作');
                    } catch (\Exception $e) {
                        throw new \Exception('积分扣除异常,请返回重新操作');
                    }

                }
                //购物车过来删除购物车内容
                if ($channel == 'cart') {
                    $cart_where = [];
                    $cart_where[] = ['uid', '=', self::$user_model['id']];
                    if ($channel == 'cart') {
                        $cart_where[] = ['is_checked', '=', 1];
                    }
//                    dump($cart_where);exit;
                    //购物车过来删除购物车内容
                    UserCartModel::where($cart_where)->delete();
                }


                //优惠券信息修改
                if (!empty($coupon_model)) {
                    $coupon_model->status = 1; //更改优惠券状态
                    $coupon_model->use_time = time(); //使用时间
                    $coupon_model->save();
                }


                //保存收货地址
                $model_order_addr = new OrderAddrModel();
                $model_order_addr->setAttrs([
                    'oid' => $model_order->id,
                    'name' => !empty($model_addr['name']) ? $model_addr['name'] : '',
                    'phone' => !empty($model_addr['phone']) ? $model_addr['phone'] : '',
                    'lng' => !empty($model_addr['lng']) ? $model_addr['lng'] : '',
                    'lat' => !empty($model_addr['lat']) ? $model_addr['lat'] : '',
                    'addr_extra' => !empty($model_addr['addr_extra']) ? $model_addr['addr_extra'] : '',
                ]);
                $model_order_addr->save();

                //商品数据
                $order_goods_data = [];

                foreach ($goods_list as $item) {
                    $goods_info = $item->apiNormalInfo();
                    array_push($order_goods_data, [
                        'mch_id' => $mch_info['id'],
                        'order_no' => $model_order['no'],//绑定订单号
                        'share_uid' => empty($share_user_model['id']) ? 0 : $share_user_model['id'],
                        'uid' => self::$user_model['id'],
                        'gid' => $goods_info['id'],
                        'price' => $goods_info['sold_price'], //实际购买价
                        'commission_money' => $item['commission_money'], //佣金
                        'sku_id' => $goods_info['sku_id'],
                        'sku_name' => $goods_info['sku_name'],
                        'num' => $goods_info['buy_num'],
                        'name' => $goods_info['name'],
                        'img' => $goods_info['cover_img'],
                        'is_kill' => $goods_info['is_kill'],
                        'extra' => json_encode($goods_info),//保存商品原始数据
                    ]);

                    //赠品
                    $gift = $goods_info['gift'] ?? [];
                    foreach ($gift as $item) {
                        array_push($order_goods_data, [
                            'mch_id' => $mch_info['id'],
                            'order_no' => $model_order['no'],//绑定订单号
                            'share_uid' => empty($share_user_model['id']) ? 0 : $share_user_model['id'],
                            'uid' => self::$user_model['id'],
                            'gid' => $item['id'],
                            'price' => $item['money'], //实际购买价
                            'is_give' => 1, //赠品状态
                            'num' => 1,
                            'name' => $item['name'],
                            'img' => $item['cover_img'],
                            'extra' => json_encode($item),//保存商品原始数据
                        ]);
                    }
                }
                $model_order->linkGoods()->saveAll($order_goods_data);
                if (empty($model_order->pay_money)) {
                    $is_pay = 0;
                    $model_order->_sure_pay(); //直接处理确认订单流程
                }

                array_push($model_order_all, $model_order);
            }
            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollBack();
            throw new \Exception($e->getMessage());
        }
        if($order_use_integral>0){
            UserLogsModel::recordData(2,self::$user_model['id'],-$order_use_integral,'使用积分下单',['q_money'=>$change_integral,'h_money'=>$change_integral-$order_use_integral,'m_type'=>0]);
        }
        return [$model_order_all[0]['id'],count($model_order_all)>1 ? $combined_no : "",$is_pay];


    }


    /**
     * 删除订单
     * @param BaseModel $user_model 用户|管理员模型
     * @param int $id 订单id
     * @throws
     * */
    public static function del(BaseModel $user_model,$id)
    {
        if(empty($id) || !is_numeric($id) || $id<=0) throw new \Exception('订单信息异常:id');
        if(empty($user_model)) throw new \Exception('用户资料异常');

        $model = self::find($id);
        if(empty($model)) throw new \Exception('操作对象不存在或已被删除');
        if($user_model instanceof SysManagerModel){
            //管理员
            $handle_action = $model->getHandleAction('m_handle');
            if(!in_array(self::ORDER_HANDLE_DEL,$handle_action))   throw new \Exception('订单状态未处于可删除状态');
            $model->m_id_opt_del = $user_model->id;

        }else{
            if(empty($model) || $model['uid']!=$user_model->id)   throw new \Exception('订单数据异常');
            $handle_action = $model->getHandleAction('u_handle');
            if(!in_array(self::ORDER_HANDLE_DEL,$handle_action))   throw new \Exception('订单状态未处于可删除状态');
        }

        $exr_num = self::where(['id'=>$id])->whereNull('delete_time')->update(['delete_time'=>time()]);
        $exr_num && $model->trigger('orderBackHandle');

    }


    /**
     * 取消订单
     * @param BaseModel $user_model 用户模型
     * @param $id int 订单id
     * @throws
     * */
    public static function cancel(BaseModel $user_model,$id)
    {
        if(empty($id) || !is_numeric($id) || $id<=0)  throw new \Exception('订单信息异常:id');
        if(empty($user_model))  throw new \Exception('用户资料异常');
        $model = self::find($id);
        if(empty($model))  throw new \Exception('订单异常:1');
        if($user_model instanceof SysManagerModel){
            //管理员
            $handle_action = $model->getHandleAction('m_handle');
            if(!in_array(self::ORDER_HANDLE_CANCEL,$handle_action))   throw new \Exception('订单状态未处于可取消状态');
        }else{
            if(empty($model) || $model['uid']!=$user_model->id)   throw new \Exception('订单数据异常:2');
            $handle_action = $model->getHandleAction('u_handle');
            if(!in_array(self::ORDER_HANDLE_CANCEL,$handle_action))   throw new \Exception('订单状态未处于可取消状态');
        }
        \think\facade\Db::startTrans();
        try{


            $exr_num = self::whereOr(['id'=>$id])->update(['cancel_time'=>time(),'step_flow'=>3,'status'=>2]);


            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollback();
            throw new \Exception($e->getMessage());
        }

        if(!empty($model['pay_time'])){
//            (new \app\common\service\third\Wechat())->setPayConfig('wx_min')->refundJingwai($model);
        }
        //触发取消订单事件
        $exr_num && $model->trigger('orderBackHandle');
        return self::find($id);
    }



    /**
     * 收货--订单
     * @param UserModel $user_model|null 用户模型
     * @param $id int 订单id
     * @param $model self|null 是否强制收货订单
     * @throws
     * */
    public static function receive(BaseModel $user_model=null,$id=0)
    {
        $model = self::find($id);
        if(empty($model))  throw new \Exception('订单异常:1');
        if($user_model instanceof SysManagerModel){

            //管理员
            $handle_action = $model->getHandleAction('m_handle');
            if(!in_array(self::ORDER_HANDLE_SURE_REC,$handle_action))   throw new \Exception('订单状态未处于可取消状态');

        }else{
            if($model['uid']!=$user_model->id)   throw new \Exception('非本人下单,无法进行此操作');
            $handle_action = $model->getHandleAction('u_handle');
            if(!in_array(self::ORDER_HANDLE_SURE_REC,$handle_action))   throw new \Exception('订单状态未处于待收货状态');
        }

        $model->_orderComplete();
        return self::find($id);
    }
    /**
     * 完成--订单
     * @param UserModel $user_model|null 用户模型
     * @param $id int 订单id
     * @param $model self|null 是否强制收货订单
     * @throws
     * */
    public static function complete($id,OrderModel $model=null)
    {
        if(is_null($model)){
            if(empty($id) || !is_numeric($id) || $id<=0)  throw new \Exception('订单信息异常:id');

            $model = self::find($id);
            if(empty($model))   throw new \Exception('订单数据异常');
            $handle_action = $model->getHandleAction('m_handle');
            if(!in_array(self::ORDER_HANDLE_COMPLETE,$handle_action))   throw new \Exception('订单状态未处于可完成状态');

        }
        $model->_orderComplete();

        return self::find($id);
    }


    /**
     * 提醒--订单
     * @param UserModel $user_model 用户模型
     * @param $id int 订单id
     * @throws
     * */
    public static function reminder(UserModel $user_model,$id=0)
    {
        if(empty($id) || !is_numeric($id) || $id<=0)  throw new \Exception('订单信息异常:id');
        if(empty($user_model))  throw new \Exception('用户资料异常');

        $model = self::find($id);
        if(empty($model) || $model['uid']!=$user_model->id)   throw new \Exception('订单数据异常');
        $handle_action = $model->getHandleAction('u_handle');
        if(!in_array(self::ORDER_HANDLE_REMINDER,$handle_action))   throw new \Exception('订单状态未处于可提醒货状态');

        if(!empty($model['reminder_time']) && $model->getData('reminder_time')-time()<15*60){
            throw new \Exception('已提醒商家....');
        }

//        $model=new OrderReminderModel();
        $model->setAttrs([
            'is_reminder' => \think\facade\Db::raw('is_reminder+1'),
            'reminder_time' => time(),
        ]);
        $model->save();
        return self::find($model['id']);
    }




    /**
     * 确认付款
     * @param SysManager $manager 管理员模型
     * @param int $id 操作订单id
     * @throws
     * */
    public static function surePay(BaseModel $manager ,$id)
    {
        $model = self::find($id);
        if(empty($model))  throw new \Exception('订单信息异常');
        $handle_action = $model->gethandleAction('m_handle');
        if(!in_array(self::ORDER_HANDLE_SURE_PAY,$handle_action))   throw new \Exception('订单状态未处于可完成状态');

        //确认支付
        $model->_sure_pay();
        return self::find($id);
    }


    /**
     * 调整订单地址
     * @param $data array
     * @throws
     * */
    public static function modAddr(array $input_data =[])
    {
        if(!isset($input_data['oid']))  throw new \Exception('订单信息异常');
        if(empty($input_data['name']))  throw new \Exception('用户名不能为空');
        if(empty($input_data['phone']))  throw new \Exception('手机号不能为空');
        if(!valid_phone($input_data['phone']))  throw new \Exception('请输入正确的手机号码');
        if(empty($input_data['addr']))  throw new \Exception('请选择地址');
        if(empty($input_data['addr_extra']))  throw new \Exception('请输入详细地址');

        $model = OrderAddrModel::where(['oid'=>$input_data['oid']])->findOrEmpty();

        $model->setAttrs($input_data);
        $model->save();
    }


    /**
     * 交易完成
     * @throws
     * */
    private function _orderComplete()
    {
        \think\facade\Db::startTrans();
        try{
            $time = time();
            $update_num = self::where(['id'=>$this['id']])->where(['is_receive'=>0,'status'=>1])->update([
                'is_receive'=>1,
                'complete_time'=>$time,
                'rec_time'=>$time,
                'step_flow'=>3,
                'status'=>3
            ]);

            if ( $update_num ) {
                OrderGoodsModel::where(['oid'=>$this['id']])->update([
                    'is_receive' => 1,
                    'receive_time' => time(),
                ]);
            }


            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollback();
            throw new \Exception('订单操作异常:'.$e->getMessage());
        }
        //交易通知
        $this->trigger('sendNoticeReceive');
    }


    //订单支付
    public function _sure_pay($pay_way=0)
    {
        $pay_info = $this->getAttr('pay_info');

        self::where(['id'=>$this['id']])->update([
            'pay_way'=>$pay_way,
            'pay_info'=> empty($pay_info)?[]:json_encode($pay_info),
            'pay_time'=>time(),
            'step_flow'=>1,
            'status'=>1]);
        OrderGoodsModel::where(['oid'=>$this['id']])->update([
            'is_pay'=>1,
            'pay_time'=>date('Y-m-d H:i:s'),
        ]);
        //创建商户提现记录
            try{


                //创建消费记录
                SysMoneyQueueModel::producer(0,$this['uid'],$this['id'],$this['goods_money'],'单号:'.$this['no'],[
                    'order_money'=>$this['order_money'],
                    'pay_money'=>$this['pay_money'],
                    'commission_money'=>$this['commission_money'],
                    'queue_type'=>'order',
                    'mch_id'=>$this['mch_id']
                ]);

            }catch (\Exception $e){
                $message = $e->getMessage();
                \think\facade\Log::write("_sure_pay,执行异常id:{$this['id']},错误消息:{$message}:line:{$e->getLine()}");
            }

        //交易通知
        $this->trigger('orderPaySuccess');


    }



    /**
     * 支付凭据
     * @param $origin string   支付来源 微信 支付宝
     * @param $mode string   支付方式 微信 支付宝
     * */
    public function getPayInfo($origin,$mode)
    {
        $pay_money = $this->getAttr('pay_money');
        $order_no = $this->getAttr('no');
        $h_order_no = $this->getAttr('h_order_no');
        $h_order_pay_money = $this->getAttr('h_order_pay_money');
        return [
            'body' => '订单支付',
            'attach' => $origin.'&OrderModel',
            'no' =>  !empty($h_order_no) ? $h_order_no : $order_no,
            'pay_money' => !empty($h_order_no) ? $h_order_pay_money : $pay_money,
//            'pay_money' => 1,
            'expire_time' => self::ORDER_EXP_TIME,
            'goods_tag' => 'goods',
            'notify_url' => url('pay/'.$mode.'Notify',[],false,true)->build(),
            'return_url' => url('order/index',['id'=>$this['id']],false,true)->build()
        ];
    }

    //获取订单支付信息
    public static function getOrderPayInfo(array $php_input=[])
    {
        $order_id = $php_input['order_id']??'';
        $h_order_no = $php_input['h_order_no']??''; //合单单号

        if(empty($order_id)) throw new \Exception('订单信息异常:order_id');
        //模型
        $model = self::where(['id'=>$order_id])->find();
        if(empty($model)) throw new \Exception('订单信息异常');
        //判断订单是否可以支付
        if(!in_array(\app\common\model\OrderModel::ORDER_HANDLE_PAY,$model->getHandleAction('u_handle'))){
            throw new \Exception('订单未处于待支付状态');
        }

        //验证是否是合单支付
        if(!empty($h_order_no)){
            $total_pay_money = self::where(['hno'=>$h_order_no])->sum('pay_money');
            //设置合单单号
            $model->setAttr('h_order_no', $h_order_no);
            //设置合单总金额
            $model->setAttr('h_order_pay_money', $total_pay_money);
        }
        return $model;
    }

    /**
     * @param array $php_input 其它数据
     * @throws
     * @return array
     */
    public static function usePayWay(array $php_input=[])
    {
        $order_id = $php_input['order_id']??'';
        $h_order_no = $php_input['h_order_no']??''; //合单单号
        $pay_way = $php_input['pay_way']??'0';
        $pay_platform = $php_input['platform']??'';
        $is_multi = $php_input['is_multi']??0;//合单支付

        if(empty($order_id)) throw new \Exception('订单信息异常:order_id');
        if(empty($pay_way)) throw new \Exception('订单支付方式异常:pay_way');

        //获取订单支付对象
        $model = self::getOrderPayInfo($php_input);
//
//        //模型
//        $model = self::where(['id'=>$order_id])->find();
//        if(empty($model)) throw new \Exception('订单信息异常');
//        //判断订单是否可以支付
//        if(!in_array(\app\common\model\OrderModel::ORDER_HANDLE_PAY,$model->getHandleAction('u_handle'))){
//            throw new \Exception('订单未处于待支付状态');
//        }
//
//        //验证是否是合单支付
//        if(!empty($h_order_no)){
//            $total_pay_money = self::where(['hno'=>$h_order_no])->sum('pay_money');
//            //设置合单单号
//            $model->setAttr('h_order_no', $h_order_no);
//            //设置合单总金额
//            $model->setAttr('h_order_pay_money', $total_pay_money);
//        }

        !empty($php_input['openid']) && $model->setAttr('pay_way_open_id', $php_input['openid']);
        $pay_info=[
            'provider' => 'yue',
        ];
        if($pay_way==1){
            $user_id = $php_input['uid']??0;
            if(empty($user_id)) throw new \Exception('请先登录');
            $userModelClass = UserModel::class;
            if(app()->http->getName()=='master'){
                $userModelClass = MasterModel::class;
            }
            //余额付款
            $userModel = $userModelClass::find($user_id);
            $user_q_money = $userModel['money'];
            if(empty($userModel)) throw new \Exception('请先登录2');
            $q_money = $userModel['money'];
            if ( $q_money < $model['pay_money'] )  throw new \Exception('钱包余额不足');
            try{
                Db::startTrans();
                //扣钱
                $row_num = $userModelClass::where(['id'=>$user_id,'money'=>$q_money])->update([
                    'money'=>Db::raw('money-'.$model['pay_money'])
                ]);
                if(empty($row_num)) throw new \Exception('操作频繁,请稍后尝试');

                //付款成功
                $model->_sure_pay($pay_way);

//                UsersMoneyLogsModel::recordData(1,$user_id,-$model['pay_money']," 下单支付扣除 ",[
//                    'm_type'=>3,'q_money'=>$user_q_money,'h_money'=>$user_q_money-$model['pay_money'],'cond_id'=>$model['id']
//                ]);

                Db::commit();
            }catch (\Exception $e){
                Db::rollback();
                throw new \Exception($e->getMessage());
            }
            $pay_info['provider'] = 'money';
            $pay_info['is_pay'] = 1;
//            $is_pay = 1;
        }elseif($pay_way==2){
            //微信
            $pay_info['provider'] = 'wxpay';
            if($pay_platform=='h5'){
                $url = \app\common\service\WechatV3Pay::h5($model);
                $pay_info['url'] = $url.'&redirect_url='.urlencode(url('order/index',[],false,true)->build());
            }elseif($pay_platform=='native'){
                $code_url =  \app\common\service\WechatV3Pay::native($model);//
                $pay_info['orderInfo'] = $code_url;
            }elseif($pay_platform=='jsapi'){
                $openid = $php_input['openid']??'';
                if(empty($openid)) throw new \Exception('请先获取用户:openid');
                $sign_data = $sign_data = \app\common\service\WechatV3Pay::jsapi($model,$openid);//
                $pay_info = array_merge($pay_info,$sign_data);

            }else{
                $sign_data = \app\common\service\WechatV3Pay::app($model);
                $pay_info['orderInfo'] = $sign_data;
            }
        }elseif ($pay_way==3){
            //支付宝
            $pay_info['provider'] = 'alipay';
            if($pay_platform=='h5'){
                $sign_data = (new \app\common\service\Alipay())->wapPay($model);//
                $pay_info['orderInfo'] = $sign_data;
            }elseif($pay_platform=='native'){
                $sign_data = (new \app\common\service\Alipay())->webPay($model);//
                $pay_info['orderInfo'] = $sign_data;
            }else{
                $sign_data = (new \app\common\service\Alipay())->appPay($model);//
                $pay_info['orderInfo'] = $sign_data;

            }
        }

        return [
            'pay_way'=>$pay_way,
            'info'=> $pay_info,

        ];
    }



    //订单回调通知
    public static function handleNotify($order_no,array $data,$pay_way=0,$pay_origin='')
    {
        $model = self::where(['no'=>$order_no])->find();
        if(empty($model)){
            return;
        }elseif(!empty($model['pay_time'])){ // 已支付
            return;
        }
        //保存第三方支付信息
        $model->setAttr('pay_way',$pay_way);
        $model->setAttr('pay_origin',$pay_origin);
        $model->setAttr('pay_info',$data);

        return $model->_sure_pay($pay_way);
    }


    //列表统计
    public static function echarts(array $php_input)
    {
        $date_mode = $php_input['date_mode']??'day';
        $where = [];
        $group = 'op_time';
        $field = ' count(*) as count_num,sum(pay_money) as sum_pay_money ';
        if ( $date_mode == 'day' ) { //按小时计算
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))];
            $field .= ' ,FROM_UNIXTIME(create_time,\'%H\') as op_time';
        } elseif ( $date_mode == 'week' ) {
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-7*84600];
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
        } elseif ( $date_mode == 'month' ) {
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-30*84600];
        } elseif ( $date_mode == 'month60' ) {
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-60*84600];
        } elseif ( $date_mode == 'year' ) {
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-365*84600];
        }
        $date_list = get_op_time($date_mode);
        $list = [];
        foreach ($date_list as $vo){
            $list[$vo] = [
                'name' => $vo,
                'value' => 0
            ];
        }
        Db::table('order')->field($field)->where(self::getStateWhere(2))->where($where)->group($group)->select()->each(function($item)use(&$list, $date_list){
            $key = $item['op_time'];
            $list[$key]['value'] = $item['sum_pay_money'];
        });

        return [array_column($list,'name'),array_column($list,'value')];
//        self::field($field)->where($where)->gourp($group)->select();

    }




    public function apiFullInfo()
    {
        $user_addr = $this->getRelation('linkAddr');
        $linkLogistics = $this->getRelation('linkLogistics');

        return array_merge($this->apiNormalInfo(),[
            'remark' => (string)$this['remark'],
            'user_addr'=>empty($user_addr)?(object)[]:$user_addr->apiNormalInfo(),
            'logistics'=>empty($linkLogistics)?(object)[]:$linkLogistics->apiNormalInfo(),
        ]);
    }



    public function apiNormalInfo()
    {
        $linkUser = $this->getRelation('linkUser');

        $linkGoods = $this->getRelation('linkGoods')??[];
        $goods = [];
        foreach ($linkGoods as $vo){
            if(app()->http->getName()=='admin'){
                array_push($goods,$vo->apiFullInfo());
            }else{
                array_push($goods,$vo->apiNormalInfo());
            }
        }

        $pay_way = (int)$this->getAttr('pay_way');
        $pay_time = $this->getAttr('pay_time');
        $send_time = $this->getAttr('send_end_time');
        $rec_time = $this->getAttr('rec_time');
        return [
            'id'=> $this['id'],
            'no'=> $this['no'],
            'proxy_uid'=> (int)$this['proxy_uid'],
            'total_num'=> $this['total_num'],
            'money'=> number_format($this['money'],2),
            'goods_money'=> number_format($this['goods_money'],2),
            'dis_money'=> number_format($this['dis_money'],2),
            'pay_money'=> number_format($this['pay_money'],2),
            'freight_money'=> number_format($this['freight_money'],2),
            'integral'=> empty($this['integral'])?0:$this['integral'],
            'q_integral'=> empty($this['q_integral'])?0:$this['q_integral'],
            'create_time' => $this->getAttr('create_time'),  //创建时间
            'pay_way_color' => self::getPropInfo('fields_pay_way',$pay_way,'color','value'),
            'pay_way' => $pay_way,
            'pay_way_name' => self::getPropInfo('fields_pay_way',$pay_way,'name','value'),
            'pay_time' => empty($pay_time)?'':date('Y-m-d H:i:s',$pay_time),  //支付金额
            'send_time' => empty($send_time)?'':date('Y-m-d H:i:s',$send_time),  //支付金额
            'rec_time' => empty($rec_time)?'':date('Y-m-d H:i:s',$rec_time),  //支付金额
            'status'=> (int)$this['status'],
            'status_color'=> (string)$this->getStepFlowInfo($this['step_flow'],'color'),
            'status_name' => $this->getStepFlowInfo($this['step_flow']),
            'handle_action' => $this->getHandleAction(app()->http->getName()==='admin'?'m_handle':'u_handle'),

            'user_id' => (int)$linkUser['id'],
            'user_name' => (string)$linkUser['name'],
            'user_avatar' => (string)$linkUser['avatar'],
            'user_phone' => (string)$linkUser['phone'],

            'goods_list'=>$goods,
        ];
    }

    //商家
    public function linkMch()
    {
        return $this->belongsTo(UserModel::class, 'mch_id');
    }
    //订单地址
    public function linkUser()
    {
        return $this->belongsTo(UserModel::class, 'uid');
    }
    //订单地址
    public function linkAddr()
    {
        return $this->hasOne(OrderAddrModel::class,'oid');
    }

    //订单商品
    public function linkJoinGoods()
    {
        return $this->hasOne(OrderGoodsModel::class, 'oid');
    }
    //订单商品
    public function linkGoods()
    {
        return $this->hasMany(OrderGoodsModel::class, 'oid');
    }

    //订单物流
    public function linkLogistics()
    {
        return $this->hasOne(OrderLogisticsModel::class,'oid');
    }

    //订单佣金
    public function linkCommission()
    {
        return $this->hasMany(OrderCommissionModel::class,'oid');
    }
    //关联商户
    public function linkMerchant()
    {
        return $this->belongsTo(MerchantModel::class,'mch_id');
    }
}