<?php
namespace app\common\model;

use think\model\concern\SoftDelete;
use think\Paginator;

class UserCollGoodsModel extends BaseModel
{
    protected $table='u_coll_goods';

    public static function getCount($user_id = 0)
    {
        if(empty($user_id)){
            return 0;
        }
        return self::withJoin(['linkGoods'],'left')->where(['user_coll_goods_model.uid'=>$user_id,'status'=>1])->whereNotNull('coll_time')->count();
    }

    public static function coll(UserModel $user_model, array $input_data = [])
    {
        $is_coll = !isset($input_data['is_coll']) || $input_data['is_coll']==1?1:0;
        $goods_info = !is_array($input_data['goods_info']) ? [] : $input_data['goods_info'];

        foreach ($goods_info as $vo){
            $goods_id = $vo['goods_id']??0;
            if(empty($goods_id)) throw new \Exception('产品信息不能为空');

            $model = self::where(['uid'=>$user_model['id'],'gid'=>$vo['goods_id']])->findOrEmpty();
            $model->setAttr('uid',$user_model['id']);
            $model->setAttr('gid',$goods_id);
            if($is_coll){
                $model->setAttr('coll_time',date('Y-m-d H:i:s'));
            }else{
                $model->setAttr('coll_time',null);
            }
            $model->save();
        }
        return $is_coll;

    }


    public static function getPageData(array $input_data)
    {
        $where = [];
        $user_id = $input_data['uid']??0;
        if(empty($user_id)){
            return Paginator::make(null,1,1,0);
        }
        $where[] =['uid','=',$user_id];
        return self::withJoin(['linkGoods'],'left')->where($where)->whereNotNull('coll_time')->order('id desc')->paginate();
    }

    public function apiNormalInfo()
    {
        $linkGoods = $this->getRelation('linkGoods');

        return array_merge(empty($linkGoods)?[]:$linkGoods->apiNormalInfo(),[

        ]);
    }




    public function linkGoods()
    {
        return $this->belongsTo(GoodsModel::class,'gid');
    }
}