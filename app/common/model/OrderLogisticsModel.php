<?php
namespace app\common\model;

use app\common\service\ExpressBird;
use think\model\concern\SoftDelete;

class OrderLogisticsModel extends BaseModel
{
    use SoftDelete;

    protected $json = ['info','express_info'];

    protected $table='o_logistics';

    public static $fields_status = [
        ['name'=>'无轨迹'],
        ['name'=>'已揽收'],
        ['name'=>'在途中'],
        ['name'=>'签收'],
        ['name'=>'问题件'],
    ];



    /**
     * 发货2
     * @param $input_data array 请求数据
     * @throws
     * @return OrderModel
     * */
    public static function sendOrder(array $input_data = [])
    {
        $id = $input_data['oid']??0;
        $no = $input_data['no']??'';
        $name = $input_data['name']??'';
        $more_no = trim($input_data['more_no']??'');
        $money = $input_data['money']??0;//运费
        $remark = trim($input_data['remark']??'');//备注
        if(empty($id))  throw new \Exception('订单数据异常');
        //查询订单信息
        $model = OrderModel::find($id);
        if(empty($model))  throw new \Exception('操作对象异常');

        if(empty($no))   throw new \Exception('请输入物流单号');
        if(empty($name))   throw new \Exception('请输入快递公司名称');

        \think\facade\Db::startTrans();
        try{
            //修改发货状态
            //物流
            $model_logistics = OrderLogisticsModel::where(['oid'=>$id])->findOrEmpty();
            $model_logistics->setAttrs([
                'oid' =>$id,
                'name' =>$name,
                'no' =>$no,
                'more_no' =>$more_no,
                'money' =>$money,
                'remark' =>$remark,
            ]);
            $model_logistics->save();
            if(empty($model['is_send']) && $model['step_flow']==1){
                //发货完成
                $model->setAttrs([
                    'send_end_time' => time(),
                    'is_send' => 1,
                    'step_flow' => 2,
                    'is_receive' => 0,
                    'receive_start_time' => time(),
                ]);
                $model->save();

            }

            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollback();
            throw new \Exception('订单操作异常:'.$e->getMessage());
        }
        //交易通知
        $model->trigger('sendNoticeSend');

        return OrderModel::find($model['id']);
    }


    public function apiNormalInfo()
    {
        $no = $this->getAttr('no');

        return [
            'id' => $this['id'],
            'oid' => $this['oid'],
            'no' => (string)$no,
            'name' => (string)$this['name'],
            'flow' => empty($flow)?[]:$flow,
        ];
    }



    public function linkOrder()
    {
        return $this->belongsTo(OrderModel::class, 'oid');
    }
}
