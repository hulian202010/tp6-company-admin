<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class GoodsSkuMixPriceModel extends BaseModel
{
    protected $table='goods_sku_mix_price';

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'gid' => $this['gid'],
            'status' => $this['status'],
            'status_bool'=>$this['status']==1,
        ]);
    }
    public function apiNormalInfo()
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'price' => $this['price'],
            'stock' => $this['stock'],
        ];
    }

}