<?php
namespace app\common\model;

use think\Model;

class OrderBackFollowModel extends BaseModel
{
    //数据库表名
    protected $table = 'o_back_follow';

    public static $fields_status = [
        ['name'=>'申请中'],
        ['name'=>'已处理'],
    ];


    public function getImgAttr($value)
    {
        return empty($value)?[]:explode(',',$value);
    }






    /**
     * 订单回复
     * @param BaseModel $mch_model
     * @param array $input_data
     * @throws \Exception
     */
    public static function reply(BaseModel $user_model, array $input_data = [])
    {
        $user_id = $manager_id = 0;
        if($user_model instanceof UserModel){
            //用户回复
            $user_id = $user_model['id'];
        }elseif($user_model instanceof SysManagerModel){
            //管理员回复
            $manager_id = $user_model['id'];
        }else{
            throw new \Exception('动作异常!');
        }

        if(empty($input_data['id'])) throw new \Exception('参数异常:id');
        $model = OrderGoodsModel::find($input_data['id']);
        if(empty($model)) throw new \Exception('讨论对象不存在');
        if($model['is_back']>2) throw new \Exception('对象未处于待讨论状态');
        if(empty($input_data['content'])) throw new \Exception('请输入内容');
        //进行中
        $back_handle_time = null;
        $is_back = $model['is_back'];
        if($user_model instanceof SysManagerModel){
            $back_handle_time = time();
            $is_back  = 2; //进行中
            $model->setAttrs([
                'is_back' => $is_back,
                'back_handle_time' => $back_handle_time,
            ]);
            $model->save();
        }

        $self = new self();
        $self->setAttrs([
            'uid' => $user_id,
            'm_uid' => $manager_id,
            'cond_id' => $model['id'],
            'content' => $input_data['content'],
            'img' => empty($input_data['img'])?'':(is_array($input_data['img'])?implode(',',$input_data['img']):$input_data['img']),
        ]);
        $self->save();
        return [$self,$back_handle_time,(int)$is_back];
    }

    //完成退款流程
    public static function handle(SysManagerModel $user_model,array $input_data = [])
    {
        //完成状态 agree-通过 refuse-拒绝 complete-完成
        $mode = $input_data['mode']??'';
        $money = $input_data['money']??'';
        if(empty($mode)) throw new \Exception('参数异常:state');
        if(empty($input_data['id'])) throw new \Exception('参数异常:id');
        $model = OrderGoodsModel::find($input_data['id']);
        if(empty($model)) throw new \Exception('讨论对象不存在');
//        $action = OrderGoodsModel::getPropInfo('fields_is_back',$model['is_back'],'action');
//        if(!in_array(OrderGoodsModel::HANDLE_action, $action)) throw new \Exception('对象为处于待处理状态');

        if(!in_array(OrderGoodsModel::HANDLE_RUNNING,$model->getHandleAction('m_handle')))   throw new \Exception('订单未处于可退款状态,无法进行此操作');


        $update_data = [];
        $update_data['back_end_time'] = time();
        $back_handle_time = '';
        if(empty($update_data['back_handle_time'])){
            $back_handle_time = time();
            $update_data['back_handle_time'] = $back_handle_time;
        }

        if($mode=='agree'){ //通过
            if(empty($money)) throw new \Exception('请输入退款金额');
            $goods_price = $model['price']*$model['num'];
            if($money>$goods_price) throw new \Exception('退款金额不得大于产品销售总价');
            $update_data['back_money'] = $money;
            $update_data['is_back'] = 4;
        }elseif($mode=='refuse'){ //拒绝
            $content = $input_data['content']??'';
            if(empty($content)) throw new \Exception('请输入退款理由');
            $update_data['is_back'] = 3;
        }elseif($mode=='complete'){ //完成
            $update_data['is_back'] = 5;
        }else{
            throw new \Exception('请检测参数:mode');
        }
        //处理退款流而成
        try{
            \think\facade\Db::startTrans();
            $model->setAttrs($update_data);
            $model->save();

            if($mode=='agree'){ //订单存在退款记录


                $model_order = OrderModel::where(['id'=>$model['oid']])->find();
                if($model_order['pay_time']){

                    if($model_order['pay_way']==1){//余额支付
                        $change_money = $user_model['money'];
                        //余额支付问题处理
                        $back_money = empty($model_order['pay_money'])?0:$model_order['pay_money'];
                        if($back_money>0){
                            UserModel::update([
                                'money'=>\think\facade\Db::raw('money+'.$back_money),
                            ],['id'=>$model_order['uid']]);
                            //创建日志
                            UserLogsModel::recordData(0,$model_order['uid'],$back_money,"订单取消返回使用余额",["m_type"=>2,'q_money'=>$change_money,'h_money'=>$change_money+$back_money]);

                        }
                    }elseif($model_order['pay_way']==2){ //微信
                        \app\common\service\WechatV3Pay::refund($model_order);
                    }elseif($model_order['pay_way']==3){ //支付宝
                        (new \app\common\service\Alipay())->refund($model_order);
                    }


                }

            }
            if(isset($content)){
                $self = new self();
                $self->setAttrs([
                    'uid' => 0,
                    'm_uid' => $user_model['id'],
                    'cond_id' => $model['id'],
                    'content' => trim($content),
                    'img' => empty($input_data['img'])?'':(is_array($input_data['img'])?implode(',',$input_data['img']):$input_data['img']),
                ]);
                $self->save();
            }
            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollBack();
            throw new \Exception($e->getMessage());
        }
        //成功异常处理
        return [$self??null,$back_handle_time,$model['is_back'],$model];
    }


    //订单退款-申请
    public static function backMoney(UserModel $user_model,array $input_data=[])
    {
        $order_id = $input_data['order_id']??0;
        $content = trim($input_data['content']??'');
        $img = $input_data['img']??'';
        if(empty($order_id)) throw new \Exception('参数异常:order_id');
        if(empty($content)) throw new \Exception('请输入退款理由');

        $model_order = OrderModel::find($order_id);
        $handle_action = $model_order->getHandleAction('u_handle');
//        dump($handle_action);exit;
        if(!in_array(OrderModel::ORDER_BACK_START,$handle_action))   throw new \Exception('订单状态未处于可退款状态');

        $model = self::where(['uid'=>$user_model['id'],'oid'=>$order_id])->find();
        if(!empty($model) && empty($model['status']))   throw new \Exception('已提交退款申请,待商家处理');

        \think\facade\Db::startTrans();
        try{
            $model = new self();
            $model->setAttrs([
                'uid' => $user_model['id'],
                'oid' => $model_order['id'],
                'mch_id' => $model_order['mch_id'],
                'content' => $content,
                'img' => $img,
            ]);
            $model->save();
            $model_order->is_back=1;
            $model_order->back_start_time=time();
            $model_order->save();
            //触发取消订单事件
            $model_order->trigger('order_back_money_handle');

            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[]);
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this->getAttr('id'),
            'uid' => (int)$this->getAttr('uid'),
            'm_uid' => (int)$this->getAttr('m_uid'),
            'content' => (string)$this->getAttr('content'),
            'img' => $this->getAttr('img'),
            'create_time' => (string)$this->getAttr('create_time'),
        ];
    }

    public static function followDetail(OrderModel $order_model,array $input_data=[])
    {
        return self::where(['oid'=>$order_model['id']])->order('id desc')->select();
    }


    public function linkOrder()
    {
        return $this->belongsTo(OrderModel::class,'oid');
    }
}