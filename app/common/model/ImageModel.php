<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class ImageModel extends BaseModel
{
    use SoftDelete;
    protected $table='image';
    public static $fields_type = [
        ['name'=>'首页轮播图', 'value'=>0,'size'=>'750*400'],
        ['name'=>'启动ad', 'value'=>1,'size'=>'750*1242'],
    ];



    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['img'])) throw new \Exception('请上传图片');
        (new self())->actionAdd($input_data);
    }

    /**
     * 页面数据
     * @param array $input_data 图片类型
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $type = empty($input_data['type'])?0:$input_data['type'];
        $where=[];
        $where[] =['type','=',(int)$type];
        if(app()->http->getName()!='admin'){
            $where[] = ['status','=',1];
        }
        $model =self::where($where)->order('sort asc');
        return $model->paginate($limit);
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'sort' => $this['sort'],
            'status' => $this['status'],
            'status_bool' => $this['status']==1,
            'update_time' => $this['update_time'],
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'img' => $this['img'],
            'url' => $this['url'],
        ];
    }
}