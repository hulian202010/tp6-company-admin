<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class ArticleModel extends BaseModel
{
    use SoftDelete;
    protected $table='article';

    public static $fields_type = [
        ['name'=>'文章列表','value'=>0,'page'=>'add'],
        ['name'=>'企业文化','value'=>1,'page'=>'cultureAdd'],
        ['name'=>'荣誉资质','value'=>2,'page'=>'honorAdd'],
        ['name'=>'荣誉风采','value'=>3,'page'=>'honorGloryAdd'],
    ];

    public function getGroupCateIdAttr($value,$data)
    {
        $cid = $data['cid']??'';
        $ct_id = $data['ct_id']??'';
        $str = $cid;
        if(!empty($ct_id)){
            $str .= ','.$ct_id;
        }
        return $str;
    }


    public function getSendDateAttr($value,$data)
    {
        return empty($data['send_date'])?'':substr($data['send_date'],0,10);
    }

    /**
     * 页面数据
     * @param int $type 分类
     * @param array $input_data 封装数据
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $keyword = trim($input_data['keyword']??'');
        $where = [];

        if(isset($input_data['id'])){
            $where[] = ['id','=',$input_data['id']];
        }

        !empty($keyword) && $where[] = ['title','like','%'.$keyword.'%'];

        $cid = $input_data['cid']??0;
        !empty($cid) && $where[]=['cid','=',$cid];
        if(app()->http->getName()!='admin'){
            $where[]=['status','=',1];
        }


        $order = 'send_date desc';
        if(!empty($type)){
            $order = 'sort asc';
        }
        return self::with(['linkCate'])->where($where)->order($order)->paginate($limit);
    }


    public function apiHonorGloryNormalInfo()
    {
        $img = $this['img'];
        $intro = empty($this['intro'])?[]:explode("\r\n",$this['intro']);
        $group_data = [];
        foreach ($img as $key=>$vo){
            $group_data[] = [
                'title' => $intro[$key]??"",
                'image' => $vo,
            ];
        }
        return [
            'id'=>$this['id'],
            'title'=>$this['title'],
            'cover_img'=>$this['cover_img'],
            'group_data'=>$group_data,
        ];
    }

    //保存文章信息
    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['title'])) throw new \Exception('请输入文章标题');
        if(empty($input_data['group_cate_id'])) throw new \Exception('请选择文章分类');
        if(empty($input_data['send_date'])) throw new \Exception('请选择发布日期');
        if(empty($input_data['img'])) throw new \Exception('请上传封面图');

        $group_cid=empty($input_data['group_cate_id'])?[]:explode(',',$input_data['group_cate_id']);
        $input_data['cid'] = $group_cid[0]??0;
        $input_data['ct_id'] = $group_cid[1]??0;
        (new self())->actionAdd($input_data);
    }


    public function apiOtherNormalInfo()
    {
        return [
            'id'=>$this['id'],
            'title'=>$this['title'],
            'cover_img'=>$this['cover_img'],
            'img'=>$this['img'],
            'intro'=>$this['intro'],
        ];
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'group_cate_id' => $this['group_cate_id'],
            'content' => (string)$this['content'],
            'status_bool'=>$this['status']==1,
            'status'=>(string)$this['status'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
        ]);
    }

    public function apiNormalInfo()
    {
        $link_cate = $this->getRelation('linkCate');
        return [
            'id'=>$this['id'],
            'cid'=>$link_cate['id'],
            'cate_name'=>$link_cate['name'],
            'title'=>$this['title'],
            'img'=>$this['img'],
            'intro'=>$this['intro'],
            'author'=>$this['author'],//作者
            'origin'=>$this['origin'],//来源
            'views'=>(int)$this['views'],
            'send_date'=>$this['send_date'],
        ];
    }

    public function linkCate()
    {
        return $this->belongsTo(ArticleCateModel::class,'cid');
    }
}