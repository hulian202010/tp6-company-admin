<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class CouponUserModel extends BaseModel
{
    use SoftDelete;
    protected $table='c_user';
    public static $fields_status = [
        ['name'=>'未使用'],
        ['name'=>'已使用'],
        ['name'=>'已过期'],

    ];

    public static function getTypeName($type)
    {

        return '';
    }


    public function apiNormalInfo()
    {
        $status = $this['status'];
        $create_time = $this->getData('create_time');
        return [
            'id' => $this['id'],
            'over_date' => (string)$this['over_date'],
            'create_date' => (string)empty($create_time)?'':date('Y-m-d',$create_time),
            'type' => (string)$this['type'],
            'img' => $this['img'],
            'name' => $this['name'],
            'money' => $this['money'],
            'full_money' => $this['full_money'],
            'pay_money_intro' => $this['pay_money_intro'],
            'expire_date' => $this['expire_date'],
            'user_intro' => $this['user_intro'],
            'status' => $status,
            'status_intro' => self::getPropInfo('fields_status',$status,'name'),
        ];
    }

    public function getUserIntroAttr($value,$data)
    {
        $type = $this->getAttr('type');
        if($type==-1){
            $intro = '平台通用';
        }else{
            $intro = self::getTypeName($type);
        }

//        if($type){
//            $intro = '指定商户可用';
//        }
        return $intro;
    }

    //验证优惠券是否过期
    public function checkOverDate()
    {
//        dump($this->over_date);exit;
        if(!empty($this->over_date) && $this->over_date<date('Y-m-d')){
            return true;
        }
        return false;

    }
    protected function getExpireDateAttr($value,$data)
    {
        return  !empty($data['over_date'])?date('Y-m-d',$data['create_time']).'至'.$data['over_date']:'永久有效';
    }
    protected function getPayMoneyIntroAttr($value,$data)
    {
        $pay_money = $data['full_money']<=0?0:$data['full_money'];
        return empty($pay_money) ? '直接优惠' : '满'.$pay_money.'可用';
    }
    protected function getCouponIntroAttr($value,$data)
    {
        return '全场通用';
    }

    //过期处理
//    public function overDayCancel()
//    {
//        $this->status=2;
//        $this->save();
//    }


    /**
     * 获取可用的代金券/优惠券
     * @param int $user_id  用户id
     * @param array $order_money 订单金额
     * @throws
     * @return array
     */
    public static function getAllUseData($user_id, array $order_money=[])
    {
        $coupon_list = $voucher_list = [];
        self::where(['uid'=>$user_id,'status'=>0])->select()->each(function($item,$index)use(&$coupon_list,&$voucher_list,$order_money){
            if(empty($item['full_money']) || $item['full_money']<=$order_money['goods_money']){
                if($item['type']==1){
                    array_push($voucher_list,$item);
                }else{
                    array_push($coupon_list,$item);
                }
            }

        });
        return [$coupon_list,$voucher_list];

    }


    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[])
    {
        $mode = $input_data['mode']??'';
        $limit = $input_data['limit']??null;
        $where = [];

        if(app()->http->getName()!='admin'){
            if($mode=='used'){ //不可用
                $where[] = ['status','>',0];
            }else{//可用
                $where[] = ['status','=',0];
            }
        }

//        $state = $input_data['state']??0;
//        $where[] = ['status','=',$state]; //使用状态
        isset($input_data['uid']) && $where[]=['uid','=',$input_data['uid']];
//        if(app()->http->getName()!='admin'){
////            $type = $input_data['type']??0;
////            $where[] = ['type','=',$type];
//        }


        $model = self::where($where)->paginate($limit);
        return $model;
    }

    //获取用户已领取过的优惠券id
    public static function getUserHasData(UserModel $user_model=null)
    {
        $data = [];
        if(empty($user_model)){
            return $data;
        }
        $data = CouponUserModel::where(['uid'=>$user_model['id'],'self_get'=>1])->column('status','cid');
        return $data;
    }


    //领券
    public static function getCoupon(UserModel  $user_model ,array $input_data = [])
    {
        $id = $input_data['id']??0;
        if(empty($id)) throw  new \Exception('参数异常:id');
        $model_coupon = CouponModel::find($id);
        if(empty($model_coupon)) throw  new \Exception('优惠券不存在');

        $model_get = self::where(['uid'=>$user_model['id'],'cid'=>$id,'self_get'=>1])->find();
        if(!empty($model_get)) throw  new \Exception('已领取过该券,无法再次领取');
        $model = new self();
        $model->setAttrs([
            'no'=>self::make_coupon_card(),
            'uid'=>$user_model['id'],
            'cid'=>$id,
            'name'=>$model_coupon['name'],
            'img'=>$model_coupon['img'],
            'money'=>$model_coupon['money'],
            'full_money'=>$model_coupon['full_money'],
            'content'=>$model_coupon['content'],
            'over_date' =>  CouponModel::overDate($model_coupon['expire_day']), //优惠券结束日期
            'status' => 0,
            'self_get' => 1, //自己领取
        ]);
        $model->save();
    }
}