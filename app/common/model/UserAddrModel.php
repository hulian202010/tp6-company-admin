<?php
namespace app\common\model;



use think\Model;
use think\model\concern\SoftDelete;

class UserAddrModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'u_addr';
    protected $_lng_lat="";

    protected function getHidePhoneAttr($value,$data)
    {
        return empty($data['phone'])?'':substr_replace($data['phone'],'****',3,4);
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this->getAttr('id'),
            'name' => $this->getAttr('name'),
            'phone' => $this->getAttr('phone'),
            'addr' => $this->getAttr('addr'),
            'addr_extra' => $this->getAttr('addr_extra'),
            'is_default' => $this->getAttr('is_default'),
        ];
    }



    public static function onAfterWrite(Model $model)
    {
        $data = $model->getData();
        if(!empty($data['is_default']) && !empty($data['uid'])){
            //其它的默认地址为0
            self::update(['is_default'=>'0'],[['uid','=',$data['uid']],['id','<>',$data['id']]]);
        }
    }

    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[])
    {
        $limit = $input_data['limit']??null;
        $where = [];
        if(isset($input_data['id'])){
            $where[] = ['id','=',$input_data['id']];
        }
        isset($input_data['uid']) && $where[] =['uid','=',$input_data['uid']];
        return self::where($where)->order('is_default desc, create_time desc')->paginate($limit);
    }

    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        if(empty($input_data['phone'])) throw new \Exception('请输入联系人号码');
        if(!valid_phone($input_data['phone'])) throw new \Exception('请输入正确的号码');
        if(empty($input_data['addr'])) throw new \Exception('请选择地址');
        if(empty($input_data['addr_extra'])) throw new \Exception('请输入详细地址');
        (new self())->actionAdd($input_data);
    }
    /**
     * 删除地址
     * @param $id int 地址id
     * @param $user_model UserModel|null 用户模型
     * @throws
     * */
    public static function del($id,UserModel $user_model=null)
    {
        $where['id'] =$id;
        !empty($user_model) && $where['uid'] = $user_model['id'];
        $model = new self();
        $model->actionDel($where);
    }

    /**
     * 设置默认地址
     * @param $id int 地址id
     * @param $user_model User|null 用户模型
     * @throws
     * */
    public static function setDef($id,UserModel $user_model=null)
    {
        !empty($user_model) && $where['uid'] = $user_model['id'];
        $model = self::find($id);
        if(empty($model)) throw new \Exception('操作异常');
        $model->is_default = 1;
        $model->save();

    }

    /**
     * 获取收货地址
     * @param $uid int 用户idid
     * @param $id int 地址id
     **/
    public static function getAddr($uid,$id=null)
    {
        $where['uid'] = $uid;
        if($id){
            $where['id'] = $id;
        }else{
            $where['is_default'] = 1;
        }
        return self::where($where)->find();
    }
}