<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class CommentModel extends BaseModel
{
    use SoftDelete;
    protected $table='comment';

    public static $fields_type = [
        ['name'=>'订单'],
    ];

    //订单评论详情
    public static function orderGoodsDetail(UserModel $user_model,array $input_data = [])
    {
        $order_id = $input_data['order_id']??0;
        $model_order = OrderModel::with(['linkUser'])->where(['uid'=>$user_model['id'],'id'=>$order_id])->find();
        $goods_list = [];
        OrderGoodsModel::where(['oid'=>$model_order['id']])->select()->each(function($item)use(&$goods_list){
            array_push($goods_list,$item->apiNormalInfo());
        });
        return [empty($model_order) ? [] : $model_order->apiNormalInfo(), $goods_list];
    }

    //订单评论
    public static function orderGoodsComment(UserModel $user_model,array $input_data = [])
    {
        $order_id = $input_data['order_id'];
        $comment_info = empty($input_data['comment_info']) || !is_array($input_data['comment_info']) ? [] : $input_data['comment_info'];
        if(empty($order_id)) throw new \Exception('没有找到该订单:order_id');
        if(empty($comment_info)) throw new \Exception('请输入评论内容');
        $model_order = OrderModel::where(['uid'=>$user_model['id'],'id'=>$order_id])->find();
        if(empty($model_order)) throw new \Exception('没有找到该订单');
        if(!empty($model_order['is_comment'])) throw new \Exception('您已评论过无需再次评论');

        $save_data = [];
        foreach ($comment_info as $vo){
            $ogid = $vo['id']??'';
            $goods_id = $vo['goods_id']??'';
            $content = $vo['content']??'';
            $level = $vo['number']??5;

            $img = empty($vo['img'])|| !is_array($vo['img']) ? '' : implode(',', $vo['img']);
            if(empty($level))  throw new \Exception('请选择平分');
            if(empty($ogid) || empty($goods_id))  throw new \Exception('评论对象异常');
            if(empty($img) && empty($content))  throw new \Exception('请输入评论内容');
            $save_data[] = [
                'uid' => $user_model['id'],
                'cond_id' => $ogid,
                'sub_cond_id' => $goods_id,
                'img' => $img,
                'content' => $content,
                'level' => $level,
            ];
        }
        $model_order->is_comment = 1;
        $model_order->comment_time = time();
        $model_order->save();

        $model = new self();
        $model->saveAll($save_data);

    }


    public static function getCount($id=0,$type=null)
    {
        $where = [];
        $where[] =['gid','=',$id];
        $where[] =['status','=',1];
        $whereFnc=null;
        if($type =='nice'){
            $where[] = ['level','>',3];
        }elseif($type=='bad'){
            $where[] = ['level','<=',3];
        }elseif($type=='img'){
            $whereFnc=function($query){
              $query->where([['img','<>',""]])->whereNotNull('img');
            };
        }
        return self::where($where)->where($whereFnc)->count();
    }


    //商品评价信息
    public static function getCommentInfo(array $input_data = [])
    {
        $goods_id = $input_data['goods_id']??0;
        $mch_id = $input_data['mch_id']??0;
        $rider_id = $input_data['rider_id']??0;

        $where = [];
        if(!empty($goods_id)){
            $where[] = ['gid','=',$goods_id];
        }elseif(!empty($rider_id)){
            $where[] = ['give_uid','=',$rider_id];
        }else{
            $where[] = ['mch_id','=',$mch_id];
            $where[] = ['give_uid','=',0];
        }
        $where[] = ['status','=',1];
        $nice_per = 0;
        $count = $nice_count =$mind_count= $bad_count =0;
        $level_count = self::where($where)->group('level')->column('count(*) as num','level');
        //晒图
        $img_count = self::where($where)->whereNotNull('img')->count();
        foreach ($level_count as $key=>$vo){
            $count+=$vo;
            if($key>=3){
                $nice_count+=$vo;
            }else{
                $bad_count+=$vo;
            }
        }

        if($count>0){
            $nice_per = (($nice_count/$count)*100);
        }

        $nice_per = sprintf('%d',$nice_per).'%';

        return [
            'count'=>$count,
            'nice_per'=>$nice_per,
            'img_count'=>$img_count,
            'nice_count'=>$nice_count,
            'mind_count'=>$mind_count,
            'bad_count'=>$bad_count,
        ];
    }


    /**
     * 页面数据
     * @param array $input_data
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $keyword = $input_data['keyword']??'';
        $goods_id = $input_data['goods_id']??0;
        $uid = $input_data['uid']??0;

        $limit = $input_data['limit']??null;
        $type = $input_data['type']??'';
        $where=[];
        $model = self::with(['linkUser','linkGoods']);

        if(app()->http->getName()!='admin'){
            if(!empty($goods_id)){
                $where[] = ['sub_cond_id','=',$goods_id];
            }elseif(!empty($uid)){
                $where[] = ['uid','=',$uid];
            }

            $where[] = ['status','=',1];
        }

        if($type=='nice'){ //好评
            $where[] = ['level','>',2];
        }elseif($type=='bad'){ //差评
            $where[] = ['level','<',3];
        }elseif($type=='img'){ //晒图
            $model = $model->where(function($query){
                $query->where([['img','<>',""]])->whereNotNull('img');
            });
        }elseif($type=='new'){ //新
            $where[] =['create_time','>',strtotime(date('Y-m-d'))];
        }
//        dump($where);exit;
        return $model->where($where)->order('id desc')->paginate($limit);
    }


    public static function orderComment(UserModel $user_model ,array $input_data = [])
    {
        $input_data['level'] = (empty($input_data['level']) || $input_data['level']>5 ||  $input_data['level']<0) ? 5 : $input_data['level'];
        $input_data['img'] = empty($input_data['img']) ?  '': (!is_array($input_data['img'])?$input_data['img']:implode(',',$input_data['img']));

        $input_data['type'] = empty($input_data['type'])?0:$input_data['type'];
        if(empty($input_data['cond_id']))  throw new \Exception('参数异常：cond_id');
        if(empty($input_data['content']) && empty($input_data['img']))  throw new \Exception('请输入评论内容');
        if($input_data['type']){

        }else{
            $cond_model = OrderGoodsModel::find($input_data['cond_id']);
            if(empty($cond_model))  throw new \Exception('评论对象不存在');
            if(!empty($cond_model['is_comment']))  throw new \Exception('您已评论');
            $input_data['sub_cond_id'] = $cond_model['gid']; //产品id
            $input_data['cond_name'] = $cond_model['sku_name'];
        }

        $model =new self();
        $model->setAttrs($input_data);
        $model->save();
        //保存评论状态
        $cond_model->setAttrs(['is_comment'=>1,'comment_date'=>date('Y-m-d H:i:s')]);
        $cond_model->save();

    }

    /**
     * @param array $input_data
     * @throws \Exception
     * @return self
     */
    public static function reply(array $input_data = [])
    {
        $mch_id = $input_data['mch_id']??0;
        $id = $input_data['id']??0;
        $content = trim($input_data['content']??'');
        if(empty($content)) throw new \Exception('请输入回复内容');

        $model = self::find($id);
        if(empty($model)) throw new \Exception('评论不存在');
        if($model['mch_id']!=$mch_id) throw new \Exception('无法回复该评论:-');
        if($model->reply_time>0) throw new \Exception('无法再次回复');
        $model->setAttrs([
            'reply_content' => $content,
            'reply_time' => time(),
        ]);
        $model->save();

        $model->linkUser;

        return $model;
    }


    //评论基本信息
    public function apiNormalInfo()
    {
        $user_info = $this->getRelation('linkUser');
        $merchant_info = $this->getRelation('linkMerchant');

        $sku_name = $this->getAttr('sku_name');
        $reply_time = $this->getAttr('reply_time');
        return [
            'id' => $this->getAttr('id'),
            'uid' => $this->getAttr('uid'),
            'user_name' => $user_info['name'],
            'user_face' => $user_info['avatar'],
            'mch_name' => $merchant_info['name'],
            'mch_logo' => $merchant_info['logo'],
            'order_num' => (string)$this->getAttr('order_num'),
            'content' => (string)$this->getAttr('content'),
            'reply_content' => (string)$this->getAttr('reply_content'),
            'reply_time' => empty($reply_time)?'':date("Y-m-d H:i:s",$reply_time),
            'sku_name' => empty($sku_name)?[]:explode(',',$sku_name),
            'level' => (Int)$this->getAttr('level'),
            'img' => $this->getAttr('img'),
            'create_time' => (string)$this->getAttr('create_time'),
        ];
    }

    public function linkUser()
    {
        return $this->belongsTo(UserModel::class,'uid');
    }

    public function linkMerchant()
    {
        return $this->belongsTo(MerchantModel::class,'mch_id');
    }
    public function linkGoods()
    {
        return $this->belongsTo(GoodsModel::class,'gid');
    }


}