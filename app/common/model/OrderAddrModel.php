<?php
namespace app\common\model;



use think\Model;

class OrderAddrModel extends BaseModel
{

    protected $table='o_addr';
    protected $_lng_lat="";

    public function apiNormalInfo()
    {
        return [
            'id' => $this->getAttr('id'),
            'name' => $this->getAttr('name'),
            'phone' => (string)$this->getAttr('phone'),
            'addr' => $this->getAttr('addr'),
            'addr_extra' => $this->getAttr('addr_extra'),
        ];
    }




    protected function getHidePhoneAttr($value,$data)
    {
        return empty($data['phone'])?'':substr_replace($data['phone'],'****',3,4);
    }



}