<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class CouponModel extends BaseModel
{
    use SoftDelete;
    protected $table='coupon';

    public static $fields_type = [
        ['name'=>'优惠券'],
    ];

    public function getIsGetAttr($value,$data)
    {
        $is_get = 0;
        $linkUserCoupon = $this->getRelation('link_user_coupon');
        $get_times = empty($data['get_times'])?1:$data['get_times'];
        $use_num = empty($data['use_num'])?0:$data['use_num'];
        $num = empty($data['num'])?0:$data['num'];
        //查询领取次数
        if(!empty($linkUserCoupon)){
            $has_get_times = count($linkUserCoupon);
            if($has_get_times>=$get_times){
                $is_get = 1;
            }
        }elseif($num<=$use_num){
            $is_get = 2;
        }
        return $is_get;
    }
    public function getGetIntroAttr($value,$data)
    {
        $data = [
            ['name'=>'立即领取'],
            ['name'=>'已领取'],
            ['name'=>'已领完'],
        ];
        $is_get = $this['is_get'];
        $info = empty($data[$is_get])?[]:$data[$is_get];
        return empty($info['name'])?'':$info['name'];
    }

    //优惠券状态
    public function getStateAttr($value,$data)
    {
        $state = 0;
        if(empty($data['num']) || $data['num']>$data['use_num']){
            $state = 1;
        }
        return $state;
    }

    public function getNumIntroAttr($value,$data)
    {
        $num = empty($data['num'])?0:$data['num'];
        $use_num = empty($data['use_num'])?0:$data['use_num'];
        return $num>0 ? $num-$use_num : '不限制次数';

    }
    public function getDayIntroAttr($value,$data)
    {
        $expire_day = empty($data['expire_day'])?0:$data['expire_day'];
        return $expire_day>0 ? '自领取'.$expire_day.'天内有效' : '永久有效';
    }
    public function getPayMoneyIntroAttr($value,$data)
    {
        $pay_money = empty($data['pay_money'])?0:$data['pay_money'];
        return empty($pay_money) ? '直接优惠' : '满'.$pay_money.'立减';
    }

    public function getCouponIntroAttr($value,$data)
    {
        $pay_money = empty($data['pay_money'])?0:$data['pay_money'];
        $money = empty($data['money'])?0:$data['money'];
        if(empty($pay_money)){
            $intro = "直接抵扣金额:".$money.'元';
        }else{
            $intro = '满'.$pay_money.'立减'.$money.'元';
        }
        return $intro;
    }


    //获取过期事件
    public static function overDate($over_date)
    {
        if(empty($over_date)){
            return null;
        }
        return date('Y-m-d',strtotime('+ '.$over_date.'days',time()));
    }




    public static function handleSaveData(array $input_data = [])
    {

        if(empty($input_data['name'])) throw new \Exception('请输入券名');
        if(empty($input_data['money'])) throw new \Exception('请输入优惠额度');

        if(!empty($input_data['money']) && $input_data['money']<0) throw new \Exception('用户优惠额度不得低于 0');
        if(!empty($input_data['full_money']) && $input_data['full_money']<0) throw new \Exception('用户满额度不得低于 0');

        (new self())->actionAdd($input_data);
    }


    /**
     * 发放优惠券
     * @param $input_data array
     * @throws
     * @return array
     * */
    public static function send(array $input_data = [])
    {
        $id = empty($input_data['id'])?0:$input_data['id'];
        $content = empty($input_data['content'])?[]: explode(',',$input_data['content']);
        $state = empty($input_data['state'])?0:$input_data['state'];
        if(empty($id))  throw new \Exception('参数异常:id');
        if($state==1 && empty($content))  throw new \Exception('请输入用户id');
        //优惠券信息
        $model_coupon = self::find($id);
        if(empty($model_coupon))  throw new \Exception('优惠券信息异常');
        if($model_coupon['status']!=1)  throw new \Exception('优惠券已被停用,无法进行发放');

        $over_date = self::overDate($model_coupon['expire_day']);
        $where = [];
        if($state==2){//全平台

        }else{
            //指定用户
            $where[] = ['id','in',$content];

        }
        //查找用户
        $record_content = $record_data = $sys_log = [];
        UserModel::where($where)->select()->each(function($item,$index)use($id,$model_coupon,$over_date,&$record_content, &$record_data,&$sys_log){
            array_push($record_content,(string)$item['id']);
            array_push($record_data,[
//                'no'=>self::make_coupon_card(),
                'uid'=>$item['id'],
                'cid'=>$id,
                'type'=>$model_coupon->getAttr('type'),
                'name'=>$model_coupon['name'],
                'img'=>$model_coupon['img'],
                'money'=>$model_coupon['money'],
                'full_money'=>$model_coupon['full_money'],
                'content'=>$model_coupon['content'],
                'over_date' => $over_date, //优惠券结束日期
                'status' => 0,
                'create_time' => time(),
                'update_time' => time(),
            ]);

        });
        //保存数据
        $record_size = count($record_data);
        $record_size>0 && CouponUserModel::insertAll($record_data);
        //数据取余
        $error_content = [];
        if($state!=2){
            $error_content = array_values(count($content)>count($record_content) ?array_diff($content,$record_content):array_diff($record_content,$content));
        }
        return [$record_content,$error_content,$record_size];

    }

    /**
     * 领取优惠券
     * @param UserModel $user_model
     * @param array $input_data
     * @throws
     */
    public static function getCoupon(UserModel $user_model, array $input_data=[])
    {
        $id = input('id',0,'intval');
        if(empty($id))  throw new \Exception('参数异常:id');
        //优惠券信息
        $model_coupon = self::find($id);
        if(empty($model_coupon))  throw new \Exception('优惠券信息异常');
        if($model_coupon['status']!=1)  throw new \Exception('优惠券已被停用,无法进行领取');

        //验证用户是否领取过
        $exist = CouponUserModel::where(['cid'=>$id,'uid'=>$user_model['id']])->find();
        if(!empty($exist))throw new \Exception('已领取过,无法再次领取');

        $over_date = self::overDate($model_coupon['expire_day']);
        $data = [
//            'no'=>self::make_coupon_card(),
            'uid'=>$user_model['id'],
            'cid'=>$id,
            'type'=>$model_coupon['type'],
            'name'=>$model_coupon['name'],
            'money'=>$model_coupon['money'],
            'full_money'=>$model_coupon['full_money'],
            'content'=>$model_coupon['content'],
            'over_date' => $over_date, //优惠券结束日期
            'status' => 0,
            'create_time' => time(),
            'update_time' => time(),
        ];
        try{
            CouponUserModel::insert($data);
        }catch (\Exception $e){

        }
    }


    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[])
    {
        $limit = $input_data['limit']??null;
        $type = $input_data['type']??0;
        $where = [];
        $where[] = ['type','=',$type];
        if(isset($input_data['status'])){
            $where[]=['status','=',$input_data['status']];
        }
        if(isset($input_data['id'])){
            $where[]=['id','=',$input_data['id']];
        }


        $model = self::where($where)->order('sort asc')->paginate($limit);

        return $model;
    }

    /**
     * @param  int $id    优惠券id
     * @param  string $field 
     * @throws
     * @return $model
     */    
    public static function getDetails(int $id, $field='*')
    {
        $model = self::where(['status'=>1])->field($field)->find($id);
        if(empty($model)) throw new \Exception('优惠券不存在');
        if($model['num']>0 && ($model['num']<=$model['use_num'])) throw new \Exception('优惠券可领取的次数不足');
        return $model;
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'status'=>(int)$this['status'],
            'status_bool'=>$this['status']==1,
            'content'=>(string)$this['content'],
            'update_time'=>$this['update_time']
        ]);
    }


    public function apiNormalInfo()
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'img' => $this['img'],
            'money' => $this['money'],
            'full_money' => $this['full_money'],
            'expire_day' => $this['expire_day'],
            'day_intro' => $this['day_intro'],
            'coupon_intro' => $this['coupon_intro'],
            'is_get' => empty($linkUserCoupon)?0:1,
        ];
    }


    public function linkUserCoupon()
    {
        return $this->hasMany(CouponUserModel::class,'cid');
    }
}