<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class GoodsSkuPriceModel extends BaseModel
{
    protected $table='goods_sku_price';
    public $kill_price = 0;
    //商品实际销售价格
    public function getSoldPriceAttr($value,$data)
    {
        $price = $data['price'];
        if(!empty($data['kill_price'])){
            return empty($data['kill_price'])?'0.00':$data['kill_price'];
        } else {
        return $price;
    }

    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'gid' => $this['gid'],
            'status_bool'=>$this['status']==1,
        ]);
    }
    public function apiNormalInfo()
    {
        $data = [
            'id' => $this['id'],
            'img' => $this['img'],
            'name' => (string)$this['name'],
            'price' => $this['price'],
            'sold_price' => (string)$this['sold_price'],
            'stock' => $this['stock'],
            'status' => (int)$this['status'],
        ];

        if(!empty($this['kill_price'])){
            $data['kill_price'] = $this['kill_price'];
        }
        if(!empty($this['kill_stock'])){
            $data['kill_stock'] = $this['kill_stock'];
        }
        return $data;
    }

}