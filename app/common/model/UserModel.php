<?php
namespace app\common\model;

use app\common\service\third\WechatJS;
use app\common\service\third\WechatOpen;
use app\common\validate\UserValidate;
use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;

class UserModel extends BaseModel
{
    use SoftDelete;

    //邀请者用户模型
    /**
     * @var self
     * */
    protected static $req_user_model;

    protected $table="user";

    //table列问题处理
    public static $fields_type_table_col=[
        ['name'=>'角色名','key'=>'name','input'=>''],
        ['name'=>'奖励积分','key'=>'integral','input'=>'number'],
        ['name'=>'购买金额','key'=>'money','input'=>'number'],
        ['name'=>'优惠比例','key'=>'per','input'=>'number'],
        ['name'=>'说明','key'=>'intro','input'=>'textarea'],
    ];
    public static $fields_type=[
        ['name'=>'会员','value'=>0,'integral'=>'','money'=>'','per'=>'','intro'=>''],
        ['name'=>'VIP1','value'=>1,'integral'=>'','money'=>'','per'=>'','intro'=>''],
        ['name'=>'VIP2','value'=>2,'integral'=>'','money'=>'','per'=>'','intro'=>''],
        ['name'=>'VIP3','value'=>3,'integral'=>'','money'=>'','per'=>'','intro'=>''],
        ['name'=>'VIP4','value'=>4,'integral'=>'','money'=>'','per'=>'','intro'=>''],
    ];

    public static $fields_sex=[
        ['name'=>'未知'],
        ['name'=>'男'],
        ['name'=>'女'],
    ];


    //能力值
    public static function fields_type()
    {
        $data = self::_fields_data_to_value(self::$fields_type,'user_type_json');
//        foreach ($data as &$vo){
//            $vo['img'] = request()->domain().$vo['img'];
//            $vo['imgd'] = request()->domain().$vo['imgd'];
//        }
        return $data;
    }

    protected static function _fields_data_to_value($data,$cache_key)
    {
        $index_key = 'value';
        $data = array_column($data,null,$index_key);
        $user_power = SysSettingModel::getContent($cache_key);
        $user_power = empty($user_power)?[] : array_column($user_power,null,$index_key);
        foreach ($data as $key=>&$vo){
            $power = $user_power[$key]??[];
            foreach ($vo as $item_key=>$item){
                $vo[$item_key] = $power[$item_key]??$item;
            }
        }
        return $data;
    }

    //hide_phone
    public function getHidePhoneAttr($value,$data=[])
    {
        $phone = $data['phone']??'';
        return empty($phone)?'':substr_replace($phone,'****',4,4);
    }

    //用户年龄
    protected function setAgeAttr($value)
    {
        if(empty($value) || $value<=0){
            return 0;
        }
        $this->setAttr('birth_y', date('Y',strtotime('-'.$value.' year',time())));
        return $value;
    }
    //获取用户生日
    public function getBirthdayAttr()
    {
        $birthday = [];
        !empty($this->birth_y) && array_push($birthday,$this->birth_y);
        !empty($this->birth_m) && array_push($birthday,sprintf("%02d",$this->birth_m));
        !empty($this->birth_d) && array_push($birthday,sprintf("%02d",$this->birth_d));
        return empty($birthday)?'':implode('-',$birthday);
    }

    public function setBirthdayAttr($value)
    {
        $birthday = empty($value)?[]:explode('-',$value);

        if(count($birthday)==3){
            $this->setAttr('birth_y',$birthday[0]);
            $this->setAttr('birth_m',$birthday[1]);
            $this->setAttr('birth_d',$birthday[2]);
            //年龄
            $this->setAttr('age',intval(date('Y')-$birthday[0]));
        }
        return $value;
    }

    public static function onBeforeInsert(Model $model)
    {


        $data =$model->getData();
        $attr = [
            'req_code'=> self::make_coupon_card(),
            'ip'    => app()->request->ip(),
        ];
        !array_key_exists('name',$data) && !empty($data['phone']) && $attr['name'] = substr($data['phone'],-3).'用户名';
        !array_key_exists('avatar',$data) && $attr['avatar'] = request()->domain().'/assets/images/avatar.png';

        if(!empty(self::$req_user_model)){
            $attr['f_uid1'] = self::$req_user_model['id'];
            $attr['f_uid2'] = self::$req_user_model['f_uid1'];
            $attr['f_uid3'] = self::$req_user_model['f_uid2'];
        }

        $model->setAttrs($attr);
    }

    public static function onAfterInsert(Model $model)
    {
        if(!empty(self::$req_user_model)){

        }

    }


    //用户密码
    protected function setPasswordAttr($value)
    {
        if(empty($value)){
            return '';
        }
        $salt = rand(1000,9999);
        $this->setAttr('salt',$salt);
        return self::generatePwd($value,$salt);
    }

    //用户支付密码
    protected function setPayPasswordAttr($value)
    {
        if(empty($value)){
            return '';
        }
        $salt = rand(1000,9999);
        $this->setAttr('pay_salt',$salt);
        return self::generatePwd($value,$salt);
    }

    /**
     * 处理用户登录
     * @param array $input_data
     * @throws
     * @return array|self
     * */
    public static function handleLogin(array $input_data = [])
    {
        $account = $input_data['account']??'';
        $password=$input_data['password']??'';
        if(isset($input_data['verify'])){
            $verify = $input_data['verify']??'';
            if(empty($verify)) throw new \Exception("请输入验证码");
            $model = self::where(["phone"=>$account])->find();
            if(empty($model)) throw new \Exception("手机号未注册");
            SmsModel::validVerify(2,$account,$verify);

        }else{
            if(empty($account)) throw new \Exception("请输入账号");
            if(empty($password)) throw new \Exception("请输入密码");
            $model = self::where(["phone"=>$account])->find();
            if(empty($model)) throw new \Exception("用户名或密码错误");
            if(self::generatePwd($password,$model['salt'])!=$model['password']) throw new \Exception("用户名或密码错误:");
        }


        if($model['status']!=1) throw new \Exception("账号已被禁用");
        //更新登录ip
        $model->ip = request()->ip();
        $model->save();
        return $model;

    }


    public static function handleThirdLogin(array $input_data = [])
    {
        $provider = $input_data['provider']??'';
        if(empty($provider)) throw new \Exception('第三方登录方式异常:provider',0);
        if($provider=='weixin'){
            $access_token = $input_data['access_token']??'';
            $openid = $input_data['openid']??'';
            if(empty($access_token)) throw new \Exception('第三方登录方式异常:access_token',0);
            if(empty($openid)) throw new \Exception('第三方登录方式异常:openid',0);
            $login_where[] = ['wx_openid','=',$openid];
            if(!empty($input_data['unionid'])){
                $login_where[] = ['wx_unionid','=',$input_data['unionid']];
            }
            $model=self::whereOr($login_where)->find();

            if(empty($model)){
                throw new \Exception('未关联手机号,请先进行注册',2);
            }
            //微信登录
        }elseif($provider=='apple'){
            $apple_id = $input_data['openid']??'';
            if(empty($apple_id)) throw new \Exception('第三方登录方式异常:apple_id',0);
            $model=self::where(['apple_id'=>$apple_id])->find();
            if(empty($model)){
                throw new \Exception('未关联手机号,请先进行注册',2);

            }
        }else{
            throw new \Exception('第三方登录方式异常:provider',0);
        }
        return $model;
    }

    //列表统计
    public static function echarts(array $php_input)
    {
        $date_mode = $php_input['date_mode']??'day';
        $where = [];
        $group = 'op_time';
        $field = ' count(*) as count_num ';
        if ( $date_mode == 'day' ) { //按小时计算
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))];
            $field .= ' ,FROM_UNIXTIME(create_time,\'%H\') as op_time';
        } elseif ( $date_mode == 'week' ) {
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-7*84600];
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
        } elseif ( $date_mode == 'month' ) {
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-30*84600];
        } elseif ( $date_mode == 'month60' ) {
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-60*84600];
        } elseif ( $date_mode == 'year' ) {
            $field .= ' ,FROM_UNIXTIME(create_time,\'%Y-%m-%d\') as op_time';
            $where[] = ['create_time','>=',strtotime(date('Y-m-d'))-365*84600];
        }
        $date_list = get_op_time($date_mode);
        $list = [];
        foreach ($date_list as $vo){
            $list[$vo] = [
                'name' => $vo,
                'value' => 0
            ];
        }
        Db::table('user')->field($field)->where($where)->group($group)->select()->each(function($item)use(&$list, $date_list){
            $key = $item['op_time'];
            $list[$key]['value'] = $item['count_num'];
        });

        return [array_column($list,'name'),array_column($list,'value')];
//        self::field($field)->where($where)->gourp($group)->select();

    }


    /**
     * 授权绑定手机
     * @param array $data  数据
     * @throws
     * @return self
     * */
    public static function authBindphone(array $php_input)
    {

        $phone = $php_input['phone']??'';
        $verify = $php_input['verify']??'';
        $password = $php_input['password']??'';
        $name = $php_input['name']??'';
        if(empty($phone)) throw new \Exception('请输入手机号');
//        if(empty($name)) throw new \Exception('请输入您的昵称');
        if(empty($verify)) throw new \Exception('请输入验证码');
        if(empty($password)) throw new \Exception('请输入密码');
        if(strlen($password)<6) throw new \Exception('密码长度不得低于6位');
        $third_auth_info = $php_input['third_auth_info']??[];
        if(empty($third_auth_info)) throw new \Exception('请先进行授权');
        //更新的数据
        $update_data = [];
        if($third_auth_info['provider']=='wxh5'){
            try{
                $auth_info = WechatJS::actToUserInfo($third_auth_info['h5_open_id']);


                $update_data['h5_open_id'] = $third_auth_info['h5_open_id'];
                $update_data['sex'] = $auth_info['sex']??0;
                $update_data['avatar'] = $auth_info['headimgurl']??'';
                !empty($auth_info['unionid']) && $update_data['wx_unionid'] = $auth_info['unionid']??'';
            }catch (\Exception $e){
                throw new \Exception('微信信息异常~'.$e->getMessage());
            }
        }elseif($third_auth_info['provider']=='weixin'){
            $access_token = $third_auth_info['access_token']??'';
            $openid = $third_auth_info['openid']??'';

            //微信授权
            try{
                $auth_info = WechatOpen::actToUserInfo($access_token,$openid);

                empty($php_input['name']) && $update_data['name'] = $auth_info['nickname'];
                $update_data['wx_openid'] = $openid;
                $update_data['sex'] = $auth_info['sex'];
                $update_data['avatar'] = $auth_info['headimgurl'];

                !empty($auth_info['unionid']) && $update_data['wx_unionid'] = $auth_info['unionid'];
            }catch (\Exception $e){
                throw new \Exception($e->getMessage());
            }
        }elseif($third_auth_info['provider']=='apple'){
            $apple_id = $third_auth_info['openid']??'';
        }else{
            throw new \Exception('授权信息异常!');
        }
        SmsModel::validVerify(4,$phone,$verify);

        $model=self::where(['phone'=>$phone])->find();
        if(empty($model)){
            if(array_key_exists('h5_open_id',$third_auth_info)){
                $model=self::where(['h5_open_id'=>$third_auth_info['h5_open_id']])->find();
            }if($third_auth_info['provider']=='weixin'){
                $model=self::where(['wx_openid'=>$openid])->find();

            }elseif($third_auth_info['provider']=='apple'){
                $model=self::where(['apple_id'=>$apple_id])->find();
            }
        }
        if(empty($model)){
            $model = new self();
        }

        $model->setAttrs(array_merge($update_data,[
            'password' => $password,
        ]));

        !empty($auth_info['unionid']) && $model->setAttr('wx_unionid',$auth_info['unionid']);

        if(array_key_exists('h5_open_id',$third_auth_info)){
            $model->setAttrs([
                'h5_open_id' => $third_auth_info['h5_open_id'],
            ]);
        }if($third_auth_info['provider']=='weixin'){
            $model->setAttrs([
                'wx_openid' => $openid,
            ]);
        }elseif($third_auth_info['provider']=='apple'){
            $model->setAttrs([
                'apple_id' => $apple_id,
            ]);
        }

        $model->save();
//        throw new \Exception($model['name']);
        //更新其它用户的授权信息
        if($third_auth_info['provider']=='weixin'){
            self::update(['wx_openid'=>null,'wx_unionid'=>null],[['wx_openid','=',$openid],['id','<>',$model['id']],['delete_time','=',null]]);
        }elseif($third_auth_info['provider']=='apple'){
            self::update(['apple_id'=>null],[['apple_id','=',$apple_id],['id','<>',$model['id']],['delete_time','=',null]]);
        }


        return $model;
    }

    //用户注册
    public static function register(array $input_data = [])
    {

        if(empty($input_data['phone'])) throw new \Exception('请输入手机号');
        if(empty($input_data['password'])) throw new \Exception('请输入密码');
        if(empty($input_data['verify'])) throw new \Exception('请输入验证码');
        if(strlen($input_data['password'])<6) throw new \Exception('密码不得低于6位');
        if(!valid_phone($input_data['phone'])) throw new \Exception('请输入正确的手机号码');

        SmsModel::validVerify(0,$input_data['phone'],$input_data['verify']);

        $where_check = [];
        $where_check[] = ['phone','=',$input_data['phone']];
        if(self::where($where_check)->find()) throw new \Exception('手机号已被注册，请更换手机号');
        $model=(new self())->actionAdd($input_data);
        return $model;
    }

    //保存用户
    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['password'])){
            unset($input_data['password']);
        }
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        if(empty($input_data['phone'])) throw new \Exception('请输入手机号');
        if(!valid_phone($input_data['phone'])) throw new \Exception('请输入正确的手机号码');
        if(isset($input_data['password']) && strlen($input_data['password'])<6) throw new \Exception('密码不得低于6位');

        if(!empty($input_data['money']) && $input_data['money']<0) throw new \Exception('用户余额不得低于 0');
        if(!empty($input_data['integral']) && $input_data['integral']<0) throw new \Exception('用户积分不得低于 0');
        $where_check = [];
        $where_check[] = ['phone','=',$input_data['phone']];
        if(!empty($input_data['id'])){
            $where_check[] = ['id','<>',$input_data['id']];
        }
        if(self::where($where_check)->find()) throw new \Exception('手机号已被注册，请更换手机号');
        (new self())->actionAdd($input_data);
    }

    //找回密码
    public static function forget(array $input_data = [])
    {
        if(empty($input_data['phone'])) throw new \Exception('请输入手机号');
        if(empty($input_data['password'])) throw new \Exception('请输入密码');
        if(empty($input_data['verify'])) throw new \Exception('请输入验证码');
        if(strlen($input_data['password'])<6) throw new \Exception('密码不得低于6位');
        if(!valid_phone($input_data['phone'])) throw new \Exception('请输入正确的手机号码');

        SmsModel::validVerify(1,$input_data['phone'],$input_data['verify']);

        $where_check = [];
        $where_check[] = ['phone','=',$input_data['phone']];
        $model = self::where($where_check)->find();
        if(!$model) throw new \Exception('手机号未注册,请检查手机号');
        $model->setAttr('password',$input_data['password']);
        $model->save();
        return $model;
    }


    /**
     * 修改用户数据
     * @param $input_data array
     * @throws
     * */
    public function modifyInfo(array $input_data=[])
    {
        if(empty($input_data)) throw new \Exception('数据异常');

        $this->readonly(['money','integral','f_uid1','phone']);
        $this->setAttrs($input_data);

        $this->save();
    }




    /**
     * 页面数据
     * @param $input_data array
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[],BaseModel  $model = null)
    {
        $limit = $input_data['limit']??null;
        $where=[];
        if(app()->http->getName()!='admin'){
            $where[] = ['status','=',1];
        }

        $keyword = trim($input_data['keyword']??'');
        !empty($keyword) && $where[] = ['name|phone','like','%'.$keyword.'%'];

        return self::where($where)->order('id desc')->paginate($limit);
    }


    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'status_bool' => $this['status']==1,
            'status' => $this['status'],
            'status_name' => self::getPropInfo('fields_status',$this['status'],'name'),
            'create_time' => $this['create_time'],
        ]);
    }


    public function apiNormalInfo()
    {
        return [
            'id' => $this['id'],
            'type' => $this['type'],
            'name' => $this['name'],
            'avatar' => $this['avatar'],
            'logo' => $this['avatar'],
            'phone' => $this['phone'],
            'hide_phone' => $this['hide_phone'],
            'money' => $this['money'],
            'integral' => $this['integral'],
            'intro' => $this['intro'],
            'addr' => $this['addr'],
        ];
    }
}