<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class PlatformGiftModel extends BaseModel
{
    use SoftDelete;
    protected $table='platform_gift';

    public static $fields_type = [
        ['name'=>'签到产品'],
        ['name'=>'积分产品'],
        ['name'=>'赠送产品'],
    ];

    public function getMoneyAttr($value){
        $type = $this->getAttr('type');
        if(!empty($type)){
            return $value;
        }else{
            return (int)$value;
        }
    }

    //商品封面图
    public  function getCoverImgAttr($value,$data)
    {
        return empty($this['img'])?'':$this['img'][0];
    }

    public function getImgAttr($value,$data)
    {
        $value = empty($value)?[]:explode(',',$value);

        return $value;
    }

    public static function getSelectList(array $input_data = [])
    {
        $list = [];
        $type = $input_data['type']??0;
        $mch_id = $input_data['mch_id']??1;
        $where = [];
        $where[]=['mch_id','=',$mch_id];
        $where[]=['type','=',$type];
        $where[]=['status','=',1];

        self::where($where)->select()->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $list;
    }


    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        $input_data['img'] = empty($input_data['img'])?'':(is_array($input_data['img'])?implode(',',$input_data['img']):$input_data['img']);
        (new self())->actionAdd($input_data);
    }


    /**
     * 获取商品数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @param UserModel|null $user_model 用户模型
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[])
    {
        $limit = $input_data['limit']??null;
        $mch_id = $input_data['mch_id']??1;
        $type = $input_data['type']??0;
        $where = [];
        $where[] = ['type','=',$type];
        $where[] = ['mch_id','=',$mch_id];
        if(isset($input_data['id'])){
            $where[]  =['id','=',$input_data['id']];
        }

        $keyword = $input_data['keyword']??'';
        if(!empty($keyword)){
            $where[] = ['name','like','%'.$keyword.'%'];
        }
//        dump($where);exit;
        //处理排序
        $limit_order_field = ['sort','sold_num','views','update_time','price'];
        $order_field = $input_data['order_field']??'';
        $order_field = !in_array($order_field,$limit_order_field)?'sort':$order_field;
        $order_sort = $input_data['order_sort']??'';
        $order_sort = $order_sort=='desc'?'desc':'asc';
        $order = $order_field.' '.$order_sort;

        return  self::where($where)->order($order)->paginate($limit);
    }


    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'status_bool' => $this['status']==1,
            'status' => $this['status'],
            'content' => (string)$this['content'],
            'update_time' => (string)$this['update_time'],
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this['id'],
            'type' => (int)$this['type'],
            'name' => (string)$this['name'],
            'sub_name' => (string)$this['sub_name'],
            'money' => (string)$this['money'],
            'cover_img' =>  (string)$this['cover_img'],
            'img' => $this['img'],
            'stock' => (int)$this['stock'],

        ];
    }
}