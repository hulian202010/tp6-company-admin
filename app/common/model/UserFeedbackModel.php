<?php
namespace app\common\model;


class UserFeedbackModel extends BaseModel
{
    protected $table = 'u_feedback';


    /*
    *@param $data array 数据
	*@return
     */
    public static function handleFeedback(array $input_data = [])
    {
        if( empty($input_data['content']) && empty($input_data['img']) ) throw new \Exception("请输入反馈内容");
        if(!empty($input_data['img'])){
            if(is_array($input_data['img'])){
                $input_data['img'] = implode(',',$input_data['img']);
            }
        }

        $model = new self();
        $model->setAttrs($input_data);

        $model->save();
    }




    public function linkUser()
    {
        return $this->belongsTo(UserModel::class,'uid');
    }
}