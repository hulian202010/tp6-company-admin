<?php
namespace app\common\model;

use app\common\service\TimeActivity;
use app\common\service\Upload;
use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;
use think\Paginator;

class GoodsModel extends BaseModel
{
    use SoftDelete;
    //指定sku数据
    public $sku_id = 0;

    /**
     * @var UserModel
     * */
    public static $user_model=null;

    public static  $fields_type = [
        ['name'=>'普通商品'],
    ];
    public static  $fields_mch_status = [
        ['name'=>'强制下架'],
        ['name'=>'正常'],
    ];



    //数据库表名
    protected $table = 'goods';

    protected $json = ['spu','sku'];

    public static $fields_default_spu = [
        ['name'=>'品牌','value'=>''],
    ];


    //--指定秒杀
    public static $kill_at = -1;

    public function getGroupCateIdAttr($value,$data)
    {
        $cid = $data['cid']??'';
        $ct_id = $data['ct_id']??'';
        $str = $cid;
        if(!empty($ct_id)){
            $str .= ','.$ct_id;
        }
        return $str;
    }

    //商品封面图
    public  function getCoverImgAttr($value,$data)
    {
        return empty($this['img'])?'':$this['img'][0];
    }

    //组合sku
    public function getSkuAttrAttr($value,$data)
    {
        $sku = $this['sku'];
        $sku = empty($sku) ? [] : $sku;

        foreach ($sku as &$vo){
            $vo['content'] = empty($vo['content'])?[]: explode(',',$vo['content']);
        }
        return $sku;
    }

    //获取sku信息
    public function getSoldSkuInfoAttr()
    {
        $linkSkuPriceOne = $this->getRelation('linkSkuPriceOne');
        $linkSkuPrice = $this->getRelation('linkSkuPrice');

        $info = null;
        if(!empty($linkSkuPriceOne)){
            return $linkSkuPriceOne;
        }elseif(!empty($linkSkuPrice) && !$linkSkuPrice->isEmpty()){
            foreach ($linkSkuPrice as $vo){
                if(!empty($this->sku_id)){
                    if($vo['id']==$this->sku_id){
                        $info =  $vo;
                        break;
                    }
                }else{
                    if($vo['status']==1){
                        $info = $vo;
                        break;
                    }
                }

            }
        }
        return $info;
    }



    //商品实际销售价格
    public function getSoldPriceAttr($value,$data)
    {
        $price = $data['price'];
        if(!empty($this['is_kill'])){
            $kill_price = $data['kill_price']??'0.00';
            return empty($this['sold_sku_info'])?$kill_price:$this['sold_sku_info']['sold_price'];
        }else{
            return empty($this['sold_sku_info'])?$price:$this['sold_sku_info']['sold_price'];
        }

    }



    public function getImgAttr($value,$data)
    {
        $value = empty($value)?[]:explode(',',$value);

        return $value;
    }


    //商品删除--之后
    public static function onAfterDelete(Model $model)
    {
        //清空-购物车-收藏
//        UserCartModel::where(['gid'=>$model['id']])->delete();
//        UserCollModel::where(['gid'=>$model['id']])->delete();
    }



    //快捷复制
    public static function copy(array $input_data = [])
    {
        $id = $input_data['id']??0;
        if(empty($id)) throw new \Exception('参数异常:id');

        $model_goods = Db::table('goods')->where(['id'=>$id])->find();
        if(empty($model_goods)) throw new \Exception('产品不存在');
        $model_goods_sku_mix_price = Db::table('goods_sku_mix_price')->where(['gid'=>$model_goods['id']])->select();
        $model_goods_sku_price = Db::table('goods_sku_price')->where(['gid'=>$model_goods['id']])->select();
        unset($model_goods['id']);
        $new_model  = new self();
        $new_model->setAttrs($model_goods);
        $new_model->save();
        if(!$model_goods_sku_mix_price->isEmpty()){
            $model_goods_sku_mix_price->each(function($item)use($new_model){
                unset($item['id']);
                $item['gid']= $new_model['id'];
                return $item;
            });
            Db::table('goods_sku_mix_price')->insertAll($model_goods_sku_mix_price->toArray());
        }
        if(!$model_goods_sku_price->isEmpty()){
            $model_goods_sku_price->each(function($item)use($new_model){
                unset($item['id']);
                $item['gid']= $new_model['id'];
                return $item;
            });
            Db::table('goods_sku_price')->insertAll($model_goods_sku_price->toArray());
        }

    }

    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['group_cate_id'])) throw new \Exception('请选择分类');
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        if(empty($input_data['og_price'])) throw new \Exception('请输入产品原价');
        if(empty($input_data['price'])) throw new \Exception('请输入产品售价');
        if(empty($input_data['img'])) throw new \Exception('请上传图片');

        $group_cid=empty($input_data['group_cate_id'])?[]:explode(',',$input_data['group_cate_id']);
        $input_data['cid'] = $group_cid[0]??0;
        $input_data['ct_id'] = $group_cid[1]??0;

        $input_data['img'] = empty($input_data['img'])?'':(is_array($input_data['img'])?implode(',',$input_data['img']):$input_data['img']);
        //spu
        $spu = empty($input_data['spu'])?[]:(!is_array($input_data['spu'])?[]:$input_data['spu']);
        $spu_arr = [];
        foreach ($spu as $item){
            if(!empty($item['name']) && !empty($item['value'])){
                $spu_arr[] = $item;
            }
        }
        $input_data['spu'] = $spu_arr;
        //sku处理
        $sku = (empty($input_data['sku']) || !is_array($input_data['sku']))?null: $input_data['sku'];
        $input_data['sku'] = null;
        $sku_price = (empty($input_data['sku_price']) || !is_array($input_data['sku_price']))?null: $input_data['sku_price'];
        //sku-mix
        $sku_mix = (empty($input_data['sku_mix']) || !is_array($input_data['sku_mix']))?null: $input_data['sku_mix'];
        $record_sku_price = [];
        $sku_price_exist_id = [];

        //处理数据
        if(!empty($sku)){
            if(empty($sku_price)){
                $input_data['sku'] = null;
            }else{
                unset($input_data['sku_price']);
                //处理sku问题
                $sku_all_name = [];
                foreach ($sku as &$item){
                    if(empty($item['name']))  throw new \Exception('请检测sku名称问题:1');
                    if(empty($item['content']))  throw new \Exception('sku属性不能为空');
                    $content = array_filter(explode(',',str_replace(' ','',$item['content'])));
                    if(count($content)!=count(array_unique($content))) throw new \Exception('规格【'.$item['name'].'】存不得存在同样sku');
                    $sku_all_name = array_merge($sku_all_name,$content);
                    $item['content'] = implode(',',$content);
                }
                $input_data['sku'] = $sku; //绑定sku
                //处理价格问题
                $sku_price_all_name = [];
                foreach ($sku_price as $vo){
                    $id = $vo['id']??0;
                    if( $id == -1 ) continue;
                    if(empty($vo['name']))  throw new \Exception('请检测sku名称问题');
                    if(empty($vo['price']))  throw new \Exception('请输入sku'.$vo['name'].'的价格');
                    $name = array_filter(explode(',',str_replace(' ','',$vo['name'])));
                    $sku_price_all_name = array_merge($sku_price_all_name,$name);
                    $info=[
                        'name' => implode(',', $name),
                        'price' => $vo['price'],
                        'stock' => $vo['stock']??0,
                        'status' => $vo['status_bool']==='true'?1:0,//启用状态
                    ];
                    if(!empty($vo['id'])){
                        $info['id'] = $id;
                        $sku_price_exist_id[] = $id;
                    }

                    $record_sku_price[] = $info;
                }

                if(array_diff($sku_price_all_name,$sku_all_name) || array_diff($sku_all_name,$sku_price_all_name)){
                    throw new \Exception('请检测sku数据信息异常,请刷新页面重新处理');
                }
            }
        }
        //处理sku_mix
        $sku_mix_price_exist_id = [];
        if(!empty($sku_mix)){
            foreach ($sku_mix as $mix_item){
                $id = $mix_item['id']??0;
                if(!empty($id)){
                    $sku_mix_price_exist_id[] = $id;
                }
                if(empty($mix_item['name']))  throw new \Exception('键输入配料名称');
                if(empty($mix_item['price']))  throw new \Exception('键输入配料价格');
                $mix_item['status'] =  $mix_item['status_bool']==='true'?1:0;//启用状态
            }
        }
        //直接保存信息
        $model = (new self())->actionAdd($input_data);

        //sku处理-开始
        $insert_sku_price_data = [];
        foreach ($record_sku_price as $vo){
            if(!empty($vo['id'])){
                //更新信息
                GoodsSkuPriceModel::where(['id'=>$vo['id']])->update($vo);
            }else{
                $vo['gid'] = $model['id'];
                $insert_sku_price_data[] = $vo;
            }
        }
        //删除不需要的
        $del_where[] = ['gid','=',$model['id']];
        if(!empty($sku_price_exist_id)){
            $del_where[] = ['id','not in',$sku_price_exist_id];
        }
//        dump($del_where);exit;
        GoodsSkuPriceModel::where($del_where)->delete();
        //保留sku数据
        if(count($insert_sku_price_data)){
            GoodsSkuPriceModel::insertAll($insert_sku_price_data);
        }
        //sku处理-结束


        //sku-mix处理-开始
        $insert_sku_mix_price_data = [];
        foreach ($sku_mix as $vo){
            $vo['status'] = $vo['status_bool']==='true'?1:0;//启用状态
            unset($vo['status_bool']);
            if(!empty($vo['id'])){
                //更新信息
                GoodsSkuMixPriceModel::where(['id'=>$vo['id']])->update($vo);
            }else{
                unset($vo['id']);
                $vo['gid'] = $model['id'];
                $insert_sku_mix_price_data[] = $vo;
            }
        }
        //删除不需要的
        $del_where[] = ['gid','=',$model['id']];
        if(!empty($sku_mix_price_exist_id)){
            $del_where[] = ['id','not in',$sku_mix_price_exist_id];
        }
//        dump($del_where);exit;
        GoodsSkuMixPriceModel::where($del_where)->delete();
        //保留sku数据
        if(count($insert_sku_mix_price_data)){
            GoodsSkuMixPriceModel::insertAll($insert_sku_mix_price_data);
        }
        //sku-mix-结束
    }





    /**
     * 获取商品数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @param UserModel|null $user_model 用户模型
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[],UserModel $user_model=null)
    {
        $limit = $input_data['limit']??null;

        $where = [];
        if(isset($input_data['id'])){
            $where[]  =['id',is_array($input_data['id']) ? 'in' : '=',$input_data['id']];
        }

        $cid = $input_data['cid']??0;
        if(!empty($cid)){
            $where[] = ['cid','=',$cid];
        }
        $ct_id = $input_data['ct_id']??0;
        if(!empty($ct_id)){
            $where[] = ['ct_id','=',$ct_id];
        }
        $keyword = $input_data['keyword']??'';
        if(!empty($keyword)){
            $where[] = ['goods_model.name','like','%'.$keyword.'%'];
        }

        $state = $input_data['state']??'all';
        if($state=='up'){
            //上架
            $where[] = ['goods_model.status','=',1];
        }elseif($state=='down'){
            //下架
            $where[] = ['goods_model.status','=',2];
        }

        if(app()->http->getName()!='admin' && !isset($input_data['ignore_status'])){
            $where[] = ['status','=',1];
        }

        //处理排序
        $limit_order_field = ['sort','sold_num','views','update_time','price'];
        $order_field = $input_data['order_field']??'';
        $order_field = !in_array($order_field,$limit_order_field)?'sort':$order_field;
        $order_sort = $input_data['order_sort']??'';
        $order_sort = $order_sort=='desc'?'desc':'asc';
        $order = $order_field.' '.$order_sort;
        $model = self::alias('goods_model')->with(['linkKill','linkCate','linkSkuPrice','linkSkuMixPrice']);


        $kill_at = $input_data['kill_at']??0;
        if(!empty($kill_at)){
            //绑定秒杀类型
            self::$kill_at = $kill_at;
            $all_goods = GoodsKillModel::where(['at'=>$kill_at,'is_open'=>1])->column('gid');
            if(empty($all_goods)){
                return Paginator::make(null,1,1,0);
            }else{
                $where[] = ['id','in',$all_goods];
            }
        }


        if($order_field=='price'){
            $order = 'goods_model.price '.$order_sort;
            $model = $model->withJoin('linkSkuPriceOne','left');
            $join_cond = $model->getOptions('join');
            $join_cond[0][2].=' and `linkSkuPriceOne`.`id`= (SELECT id from goods_sku_price s where s.gid=`goods_model`.`id`  ORDER BY price '.$order_sort.' limit 1)';
            $model->setOption('join',$join_cond);
        }


        //获取当前秒杀时间段
        if(self::$kill_at<0){
            $info = TimeActivity::getRunning();
            self::$kill_at = $info['value']??0;
        }

//        dump($where);exit;
        $model = $model->where($where)->order($order)->paginate($limit)->each(function($item,$index){
            $item['is_kill'] = 0; //是否在秒杀中.....
            $linkKill = $item->getRelation('linkKill');

            //获取当前正在进行得秒杀商品
            if(!empty($linkKill)){
                foreach ($linkKill as $vo){
                    if($vo['at']==self::$kill_at){ //当前选中
                        $item['is_kill'] = TimeActivity::isRunning(self::$kill_at);

                        if($vo['is_open']){  //开启
                            if(empty($vo['sku_id'])){
                                $item['kill_price'] = $vo['price'];
                                $item['kill_stock'] = $vo['stock'];
                            }else{
                                foreach ($item['linkSkuPrice'] as $sku_price){
                                    if($vo['sku_id']==$sku_price['id']){
                                        //绑定秒杀sku
                                        empty($item->sku_id) && $item->sku_id = $vo['sku_id'];
                                        $sku_price['kill_price'] = $vo['price'];
                                        $sku_price['kill_stock'] = $vo['stock'];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });


        return $model;
    }

    public function apiFullInfo()
    {
        $linkCate = $this->getRelation('linkCate');
        $linkSkuPrice = $this->getRelation('linkSkuPrice');
        $linkSkuMixPrice = $this->getRelation('linkSkuMixPrice');
        $sku_price = [];
        if(!empty($linkSkuPrice)){
            foreach ($linkSkuPrice as $vo){
                $sku_price[] = $vo->apiFullInfo();
            }
        }
        $sku_mix_price = [];
        if(!empty($linkSkuMixPrice)){
            foreach ($linkSkuMixPrice as $vo){
                $sku_mix_price[] = $vo->apiFullInfo();
            }
        }
        return array_merge($this->apiNormalInfo(),[
            'status_bool'=> $this['status']==1,
            'status'=> $this['status'],
            'status_name'=> self::getPropInfo('fields_status',$this['status'],'name'),
            'intro' => (string)$this->getAttr('intro'),
            'content' => (string)$this->getAttr('content'),
            'spu' => $this->getAttr('spu'),
            'sku' => $this->getAttr('sku'),
            'sku_mix' => $sku_mix_price, //配料信息
            'sku_price' => $sku_price,
            'sort' => (int)$this['sort'],    //排序
            'update_time' => (string)$this['update_time'],    //排序


            //分类
            'group_cate_id' => (string)$this['group_cate_id'],
            'cid' => (string)$linkCate['id'],
            'cate_name' => (string)$linkCate['name'],
        ]);

    }

    //api详情id
    public function apiDetailInfo()
    {
        $is_kill = empty($this['is_kill'])?0:1;

        $linkSkuPrice = $this->getRelation('linkSkuPrice');
        $sku_price = [];
        if(!empty($linkSkuPrice)){
            foreach ($linkSkuPrice as $vo){
//                if($vo['status']==1){
                    $sku_price[] = $vo->apiNormalInfo();
//                }
            }
        }
        return array_merge($this->apiNormalInfo(),[
            'intro' => (string)$this->getAttr('intro'),
            'content' => (string)$this->getAttr('content'),
            'spu' => empty($this['spu']) || !is_array($this['spu']) ? [] : $this['spu'],
            'sku_attr' => $this->getAttr('sku_attr'),
            'sku_price' => $sku_price,

            'reduce_second' => $is_kill?TimeActivity::getRunning('reduce_second'):0, //活动倒计时
        ]);
    }


    //获取商品基本信息
    public  function apiNormalInfo(){
        $sold_sku_info = $this['sold_sku_info'];

        $is_kill = empty($this['is_kill'])?0:1;

        $data = [
            'id' => (int)$this['id'],
            'img' => $this['img'],
            'cover_img' => (string)$this['cover_img'],
            'name' => (string)$this['name'],
            'sub_name' => (string)$this['sub_name'],
            'price' => (string)$this['price'],          //标价 售价
            'sold_price' => (string)$this['sold_price'],          //售价
            'og_price' => (string)$this['og_price'],    //原价
            'stock' => (int)$this['stock'],    //库存
            'freight_money' => $this['freight_money'],    //运费
            'sold_num' => (int)$this['sold_num'],    //销量

            'sku_id' => empty($sold_sku_info)?0:(int)$sold_sku_info['id'],
            'sku_name' => empty($sold_sku_info)?'':(string)$sold_sku_info['name'],


            'is_kill' => $is_kill, //是否进行秒杀活动中...

        ];

        $get_data = $this->getData();
        if(!empty($this['kill_price'])){
            $data['kill_price'] = $this['kill_price'];
        }

        if(!empty($this['kill_stock'])){
            $data['kill_stock'] = $this['kill_stock'];
        }
        $buy_num = $this->getAttr('buy_num');
        if(!empty($buy_num)){
            $data['buy_num'] = $buy_num;
        }
        $is_checked = $this->getAttr('is_checked');
        if(isset($get_data['is_checked'])){
            $data['is_checked'] = $get_data['is_checked'];
        }
        $cart_id = $this->getAttr('cart_id');
        if(!empty($cart_id)){
            $data['cart_id'] = $cart_id;
        }
        return $data;


    }


    public function linkCate()
    {
        return $this->belongsTo(GoodsCateModel::class,'cid');
    }
    public function linkSkuPrice()
    {
        return $this->hasMany(GoodsSkuPriceModel::class,'gid')->order('id asc');
    }
    public function linkSkuMixPrice()
    {
        return $this->hasMany(GoodsSkuMixPriceModel::class,'gid')->order('id asc');
    }
    public function linkSkuPriceOne()
    {
        return $this->hasOne(GoodsSkuPriceModel::class,'gid');
    }


    //秒杀产品
    public function linkKill()
    {
        return $this->hasMany(GoodsKillModel::class,'gid');
    }
}