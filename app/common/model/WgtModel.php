<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class WgtModel extends BaseModel
{
    use SoftDelete;
    protected $table='wgt';
    public static $fields_type = [
        ['name'=>'用户端'],
        ['name'=>'商家端'],
    ];
    public static $fields_platform=[
        ['name'=>'全平台'],
        ['name'=>'android'],
        ['name'=>'ios'],
    ];

    /**
     * 页面数据
     * @param int $type 分类
     * @param array $input_data 封装数据
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $keyword = trim($input_data['keyword']??'');
        $where = [];

        if(isset($input_data['id'])){
            $where[] = ['id','=',$input_data['id']];
        }

        !empty($keyword) && $where[] = ['title','like','%'.$keyword.'%'];



        $order = 'id desc';

        return self::where($where)->order($order)->paginate($limit);
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'status_bool'=>$this['status']==1,
            'status'=>(string)$this['status'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>$this['id'],
            'type'=>$this['type'],
            'type_name'=>self::getPropInfo('fields_type',$this['type'],'name'),
            'platform'=>$this['platform'],
            'platform_name'=>self::getPropInfo('fields_platform',$this['platform'],'name'),
            'content' =>(string)$this['content'],
            'version' =>(string)$this['version'],
            'path' =>(string)$this['path'],
            'create_time' =>(string)$this['create_time'],
        ];
    }

}