<?php
namespace app\common\model;

use think\Model;
use think\Validate;

class UserDataSignModel extends BaseModel
{

    protected $table='u_date_sign';



    /**
     * 用户签到
     * @param UserModel $user_model 用户对象
     * @throws
     * @return
     * */
    public static function sign(UserModel $user_model,array $input_data = [])
    {

        $change_integral = $user_model['integral']; //变动前积分

        $yesterday = date('Y-m-d',strtotime('-1 day'));
        $today = date('Y-m-d');
        $model = self::where([
            ['uid','=',$user_model->id],
        ])->order('id desc')->find();
        if(!empty($model) && $model['date']==$today){
            throw new \Exception('您今天已签到过');
        }

        //获取可签到的列表数据
        $sign_integral =  SysSettingModel::getContent('integral','sign_float');

        try{
            \think\facade\Db::startTrans();
            //增加用户积分
            $user_model->setAttrs([
                'integral' => \think\facade\Db::raw('integral+'.$sign_integral),
                'history_integral' => \think\facade\Db::raw('history_integral+'.$sign_integral),
            ]);
            $user_model->save();

            //增加签到
            $lx_times = empty($model->lx_times)?1:($model->date!=$yesterday?1:$model->lx_times+1);

            $new_model = new self();
            $new_model->setAttrs([
                'uid'=>$user_model['id'],
                'date'=>$today,
                'times'=>empty($model->times)?1:$model->times+1,//总签到次数
                'lx_times'=>$lx_times,//总签到次数
                'integral'=>$sign_integral,//总签到次数
            ]);

            $new_model->save();
            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollback();
            throw new \Exception($e->getMessage());
        }
        //积分日志
        if($sign_integral>0) {
            UserLogsModel::recordData(1, $user_model['id'], $sign_integral, '签到增加', [
                'q_money'=>$change_integral,
                'h_money'=>($change_integral+$sign_integral),
                'm_type'=>1,
                'id' => $new_model['id'],
                'sign_integral'=>$sign_integral,
            ]);
        }

        return [$new_model->lx_times,$sign_integral,$today];
    }





    /**
     * 每日签到数据
     * @param array $input_data 请求内容
     * @throws
     * @return \think\Paginator
     * */
    public static function dayPageData(array $input_data=[])
    {
        $limit = $input_data['limit']??null;
        $start_date = $input_data['start_date']??'';
        $start_time = empty($start_date)?'':$start_date;
        $end_date = $input_data['end_date']??'';
        $end_time = empty($end_date)?'':$end_date;
        $keyword = trim($input_data['keyword']??'');
        $where = [];

        if($start_time && $end_time){
            $where[] = ['date','between',[$start_time,$end_time]];
        }elseif($start_time){
            $where[] = ['date','>',$start_time];
        }elseif($end_time){
            $where[] = ['date','<=',$end_time];
        }


        $fields = '*,sum(integral) as sum_integral';
        return self::where($where)->field($fields)->group('date')->order('date desc')->paginate($limit);
    }
}