<?php
namespace app\common\model;
class UserLogsModel extends BaseModel
{
    protected $table = 'u_logs';
    public static $fields_type = [
        ['name'=>'系统','value'=>0,'more'=>[]],
        ['name'=>'消费','value'=>1,'more'=>[
            ['name'=>'消费'],
            ['name'=>'佣金'],
        ]],
        ['name'=>'积分','value'=>2,'more'=>[
            0=>['name'=>'消费'],
            1=>['name'=>'销售'],
            2=>['name'=>'转让'],
            3=>['name'=>'增益'],
            4=>['name'=>'签到'],
            5=>['name'=>'代购'],
            6=>['name'=>'导购签到赠送'],
            7=>['name'=>'注册'],
            8=>['name'=>'摇奖池'],
            9=>['name'=>'众筹参与'],
            11=>['name'=>'连标'],
            12=>['name'=>'系统调整'],
            13=>['name'=>'大爱积分'],
            14=>['name'=>'代理获得'],
        ]],
        ['name'=>'券分','value'=>3,'more'=>[
            ['name'=>'消费'],
            ['name'=>'签到'],
            ['name'=>'被邀请者签到'],
            ['name'=>'被邀请者消费获得券分'],
            ['name'=>'系统调整'],

        ]],
    ];

    protected $json = ['info'];


    /**
     *
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[])
    {
        $show_type = $input_data['show_type']??'';
        $start_date = $input_data['start_date']??'';
        $start_time = empty($start_date)?'':strtotime($start_date);
        $end_date = $input_data['end_date']??'';
        $end_time = empty($end_date)?'':(strtotime($end_date)+86400);
        $keyword = trim($input_data['keyword']??'');
        $where = [];
        !empty($keyword) && $where[] = ['uid','like','%'.$keyword.'%'];
        $limit = input('limit', config('paginate.list_rows') ,'intval');
        if(isset($input_data['uid'])){
            $where[] = [ 'uid' , '=' , $input_data['uid'] ];
        }

        if(app()->http->getName()!='admin'){
            if($show_type=='q_integral'){
                //券分
                $where[] = ['type','=',3];
            }else{
                //其它
                $where[] = ['type','<>',3];
            }
        }else{
            if(isset($input_data['type'])){
                $where[] = ['type','=',$input_data['type']];
            }
        }

        if($start_time && $end_time){
            $where[] = ['create_time','between',[$start_time,$end_time]];
        }elseif($start_time){
            $where[] = ['create_time','>',$start_time];
        }elseif($end_time){
            $where[] = ['create_time','<=',$end_time];
        }

        //余额日志
        $money_state = $input_data['money_state']??0;
        if($money_state==1){
            //收入
            $where[]=['money','>=',0];
        }else if($money_state==2){
            //支出
            $where[]=['money','<',0];
        }else if($money_state==2){
            //提现
            $where[]=['type','=',1];
        }
//        !empty($money_state) && $where[]=['type','=',$money_state];

        $model = self::where($where)->order('id desc')->paginate($limit);
        return $model;
    }


    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @throws
     * @return \think\Paginator
     * */
    public static function getPaginateData(array $input_data=[])
    {
        $where = [];
        !empty($input_data['uid']) && $where[]=['uid','=',$input_data['uid']];
        if(isset($input_data['type'])){
            $where[]=['type','=',$input_data['type']];
        }

        $model = self::where($where);
        if(!empty($input_data['with'])){
            $model->with(['linkUser']);
        }

        return $model->order('id desc')->paginate();
    }

    /**
     * 记录日志
     * @param $type int 类型
     * @param $uid int 用户id
     * @param float $money 金额
     * @param string $intro 说明
     * @param array $info  扩展数据
     * */
    public static function recordData($type,$uid,$money,$intro,array $info=[])
    {
        $q_money = $info['q_money']??0;//变动前
        $h_money = $info['h_money']??0;//变动后
        $m_type = $info['m_type']??0;//更多
        $is_hide = $info['is_hide']??0;//是否隐藏
        unset($info['m_type']);
        $model = new self();
        $model->data([
            'uid'=>$uid,
            'type'=>$type,
            'is_hide'=>$is_hide,
            'm_type'=>$m_type,
            'q_money'=>$q_money,
            'money'=>$money,
            'h_money'=>$h_money,
            'intro'=>$intro,
        ],true);
        //扩展数据
        if(!empty($info)) $model->setAttr('info',$info);
        $model->save();
    }

    public function linkUser()
    {
        return $this->belongsTo(UserModel::class,'uid');
    }
}