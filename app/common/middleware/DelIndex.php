<?php
namespace app\common\middleware;

use think\Response;

class DelIndex
{
    /**
     * @param $request \think\Request
     * @param $next \Closure
     * @return \Closure|Response
     * */
    public function handle($request, \Closure $next)
    {
        if($request->isGet() && stripos($request->baseUrl(),'/index.php')===0){
            $url = str_ireplace('/index.php','',$request->baseUrl());
            if($url=='/' || empty($url)){
                $url ='/index';
            }
            $url = $request->domain().$url;
            return redirect($url);
        }
        return $next($request);
    }
}