<?php
namespace app\admin\controller;

use app\common\model\CouponModel;

class Coupon extends Common
{
    public function lists()
    {
        $list = [];
        $info = \app\common\model\CouponModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);

    }


    //
    public function add()
    {
        $php_input = $this->request->param();
        try{
            CouponModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);

    }

    //删除数据
    public function del()
    {
        $id = $this->request->param('id',0,'int');
        try{
            CouponModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    //修改分类信息
    public function modInfo()
    {
        $php_input = input();
        try{
            CouponModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    //按手机发放优惠券
    public function send()
    {
        try{
            list($record_content,$error_content,$record_size)=CouponModel::send(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1, '已成功发放:'.$record_size.'张', [
            'success_content' => $record_content,
            'error_content' => $error_content,
            'record_size' => $record_size,
        ]);
    }


    public function package()
    {
        $keyword = input('keyword','','trim');
        $php_input = input();
        $list = CouponPackageModel::getPageData($php_input);
        // 获取分页显示
        $page = $list->render();
        return view('package',[
            'keyword' => $keyword,
            'list' => $list,
            'page'=>$page,
        ] );

    }


    //
    public function packageAdd()
    {
        $type = input('type',0,'intval');
        $id = $this->request->param('id');
        $model = new \app\common\model\CouponPackageModel();

        //表单提交
        if($this->request->isAjax()){
            $php_input = input();
            $php_input['cids'] = empty($php_input['cids'])?'':implode(',',$php_input['cids']);
            $validate = new \app\common\validate\CouponPackageValidate();
            try{
                $model->actionAdd($php_input,$validate);//调用BaseModel中封装的添加/更新操作
            }catch (\Exception $e){
                return $this->_resData(0,$e->getMessage());
            }
            return $this->_resData(1,'操作成功');
        }
        $model = $model->get($id);
        $coupon_list = CouponModel::where(['status'=>1])->select();
        //含有那些优惠券
        $has_coupon_list = [];
        if(!empty($model['cids'])){
            $cids = explode(',',$model['cids']);
            foreach ($cids as $vo){
                $has_coupon_list[] = CouponModel::find($vo);
            }
//            $has_coupon_list = CouponModel::where([['id','in',$model['cids']]])->select();
        }
        return view('packageAdd',[
            'has_coupon_list' => $has_coupon_list,
            'coupon_list' => $coupon_list,
            'model' => $model,
            'type' => $type,
        ]);

    }


    //删除数据
    public function packageDel()
    {
        $id = $this->request->param('id',0,'int');
        $model = new \app\common\model\CouponPackageModel();
        try{
            $model->actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


}