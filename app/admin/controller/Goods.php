<?php
namespace app\admin\controller;

use app\common\model\ArticleCateModel;
use app\common\model\ArticleModel;
use app\common\model\GdCateModel;
use app\common\model\GoodsCateModel;
use app\common\model\GoodsKillModel;
use app\common\model\GoodsModel;
use app\common\model\PageHonorModel;
use app\common\model\SingleArticleModel;
use app\common\validate\ArticleCateValidate;
use app\common\validate\ArticleValidate;
use app\common\validate\SingleArticleValidate;

class Goods extends Common
{

    public function cate()
    {
        $input_data = input();
        $list =[];
        GoodsCateModel::getAllCate($input_data)->each(function($item,$index)use(&$list){
            $info = $item->apiFullInfo();
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if(!empty($linkChild)){
                foreach ($linkChild as $vo){
                    array_push($child_list,$vo->apiFullInfo());
                }
            }
            $info['child_list'] = $child_list;
            array_push($list,$info);
        });

        return $this->_resData(1,'获取成功',['list'=>$list]);
    }

    public function cateAdd()
    {

        $php_input = $this->request->param();
        try{
            GoodsCateModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);

    }

    //删除数据
    public function cateDel()
    {
        $id = input('id',0,'intval');
        try{
            GoodsCateModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //修改分类信息
    public function cateModInfo()
    {
        $php_input = input();
        try{
            GoodsCateModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    public function lists()
    {
        $input_data = input();
        $list =[];


        $info = GoodsModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });

        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);


    }

    public function handleSaveData()
    {

        $php_input = $this->request->param();
        try{
            GoodsModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    //删除数据
    public function del()
    {
        $id = $this->request->param('id',0,'int');
        try{
            GoodsModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    public function copy()
    {
        try{
            GoodsModel::copy(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //修改分类信息
    public function modInfo()
    {
        $php_input = input();
        try{
            GoodsModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    public function handleSaveKill()
    {

        try{
            GoodsKillModel::handleSaveData(input());
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }

    public function timeActivity()
    {
        $input_data = input();
        $list =[];


        $info = GoodsKillModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });

        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);


    }

    //删除数据
    public function killDel()
    {
        try{
            GoodsKillModel::del(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


}
