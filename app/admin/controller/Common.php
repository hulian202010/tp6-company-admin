<?php
namespace app\admin\controller;

use app\BaseController;
use app\common\model\SysManagerModel;
use app\common\model\UserModel;

class Common extends BaseController {


    protected $open_action_validate = true;

    /**
     * 生产登录凭证
     * @param SysManagerModel $model
     * @return array
     * */
    protected function loginInfo(SysManagerModel $model):array
    {
        return [
            'token'     => $model->generateUserToken($model['password']),
            'userInfo' => [
                'u_id' => $model['id'],
                'u_name' => $model['name'],
                'u_account' => $model['account'],
            ],
            'authList' => [],

        ];

    }

}