<?php
namespace app\admin\controller;

use app\common\model\OrderBackFollowModel;
use app\common\model\OrderGoodsModel;
use app\common\model\OrderLogisticsModel;
use app\common\model\OrderModel;

class Order extends Common
{



    public function lists()
    {
        $list = [];
        $info = \app\common\model\OrderModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }



    //订单详情
    public function detail()
    {

    }



    //删除订单
    public function del()
    {

        $id = $this->request->param('id');
        try{
            \app\common\model\OrderModel::del($this->user_model,$id);
            return $this->_resData(1,'操作成功');
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }

    }

    //取消订单
    public function cancel()
    {

        $id = $this->request->param('id');
        try{
            \app\common\model\OrderModel::cancel($this->user_model,$id);
            return $this->_resData(1,'操作成功');
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
    }

    //确定支付
    public function surePay()
    {
        $id = $this->request->param('id');
        try{
            $model = \app\common\model\OrderModel::surePay($this->user_model,$id);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }
    //发货
    public function sendOrder()
    {
        $input_data = input();
        try{
            $model = \app\common\model\OrderLogisticsModel::sendOrder( $input_data );
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }


    //调整订单地址
    public function modAddr()
    {
        $php_input = input();
        try{
            \app\common\model\OrderModel::modAddr($php_input);
            return $this->_resData(1,'操作成功');
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
    }


    //调整订单地址
    public function commission()
    {
        $id = input('id',0,'intval');
        $order_id = input('order_id',0,'intval');
        try{
            $model = \app\common\model\OrderCommissionModel::where(['id'=>$id,'oid'=>$order_id])->find();
            if($model){
                \app\common\model\OrderCommissionModel::onOptCommission($model);
                return $this->_resData(1,'操作成功');
            }else{
                return $this->_resData(1,'操作对象异常');
            }
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
    }

    public function complete()
    {
        $id = $this->request->param('id');
        try{
            $model = \app\common\model\OrderModel::complete($id);

        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1, '操作成功',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }



    public function mchReceive()
    {

        try{
            $model = OrderModel::receive($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功',[
            'status'=> (int)$model['status'],
            'status_color'=> (string)$model->getStepFlowInfo($model['step_flow'],'color'),
            'status_name' => $model->getStepFlowInfo($model['step_flow']),
            'handle_action' => $model->getHandleAction('m_handle'),
        ]);
    }

    //售后回复
    public function salesAfter()
    {
        $php_input = input();
        try{
            list($model,$back_handle_time,$is_back)=OrderBackFollowModel::reply($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已回复',[
            'detail'=>$model->apiFullInfo(),
            'is_back'=>$is_back,
            'back_handle_time'=>empty($back_handle_time)?'':date('Y-m-d H:i:s',$back_handle_time),
        ]);
    }
    public function salesAfterHandle()
    {
        try{
            list($model,$back_handle_time,$is_back,$back_model) = OrderBackFollowModel::handle($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'已处理',[
            'detail'=>empty($model)?(object)[]:$model->apiFullInfo(),
            'is_back'=>$is_back,
            'back_action' => OrderGoodsModel::getPropInfo('fields_is_back',$is_back,'action'),
            'back_handle_time'=>empty($back_handle_time)?'':date('Y-m-d H:i:s',$back_handle_time),
            'back_end_time'=>empty($back_model['back_end_time'])?'':date('Y-m-d H:i:s',$back_model['back_end_time']),
            'back_money'=>empty($back_model['back_money'])?'':$back_model['back_money'],
        ]);
    }





}