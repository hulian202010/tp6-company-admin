<?php
namespace  app\admin\controller;

use app\common\model\CouponUserModel;
use app\common\model\GdCommentModel;
use app\common\model\MchReqModel;
use app\common\model\OrderGoodsModel;
use app\common\model\RiderCardAuthModel;
use app\common\model\RiderModel;
use app\common\model\SysPlatformCommission;
use app\common\model\UserAddrModel;
use app\common\model\UseReqInfoModel;
use app\common\model\UserLabelModel;
use app\common\model\UserLogsModel;
use app\common\model\UserModel;
use app\common\model\UserSubscribeModel;
use app\common\model\UserWithdrawModel;
use app\common\service\Excel;
use app\common\validate\RiderValidate;
use app\common\validate\UserLabelValidate;
use app\common\validate\UserValidate;

class User extends Common
{


    public function lists()
    {

        $list = [];
        $info = \app\common\model\UserModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }

    public function add()
    {
        $php_input = $this->request->param();
        try{
            UserModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);

    }

    //删除数据
    public function del()
    {
        $id = $this->request->param('id',0,'int');
        try{
            UserModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //修改分类信息
    public function modInfo()
    {
        $php_input = input();
        try{
            UserModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }



    //用户详情
    public function detail()
    {
        $id = input('id',0,'intval');

        $model = UserModel::find($id);
        //余额日志
        $addr_list = UserAddrModel::where('uid',$model['id'])->order('id desc')->select();
        //优惠券
        $coupon_list = CouponUserModel::where(['uid'=>$id])->order('over_date desc')->select();
        return view('detail',[
            'model'=>$model,
            'addr_list'=>$addr_list,
            'coupon_list'=>$coupon_list,

        ]);
    }


    public function moneyLogs()
    {
        $id = input('id',0,'intval');
        $input_data = input();
        $list = [];
        //日志记录
        $info = UserLogsModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,[
                'intro'=>$item['intro'],
                'money'=>$item['money'],
                'date'=>date('Y-m-d',$item->getData('create_time')),
            ]);
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total_page'=>$info->lastPage()]);
    }


    //申请
    public function identReq()
    {
        $keyword = input('keyword','','trim');
        $list = UseReqInfoModel::getPageData(input());
        return view('mchReq',[
            'keyword' => $keyword,
            'list' => $list,
            'page' => $list->render(),
        ]);
    }

    public function handleIdentReq()
    {
        try{
            UseReqInfoModel::auth($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //删除数据
    public function identReqDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            UseReqInfoModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    public function uploadExcel()
    {


        $upload_info = (new \app\common\service\Upload())->upload('upload_user_excel');
        $file_path = root_path().$upload_info['key'];
        $data = Excel::UploadFile($file_path);
        //导入用户
        array_shift($data); //删除第一行

        $user_data = $user_label = $user_coupon = [];
        $phone_exits = $phone_error = [];
        //查询所有用户label
        $all_user_label = UserLabelModel::column('id','name');

        //查询所有手机号
        $all_user_phone = UserModel::column('phone');

        foreach ($data as $vo){
            $user_name = isset($vo[0])?$vo[0]:'';
            $phone = isset($vo[1])?$vo[1]:'';
            $coupon_money = isset($vo[2])?abs(trim($vo[2])):0;
            $label = isset($vo[3]) ? trim($vo[3]) : 0;
            if(!array_key_exists($label,$all_user_label)){
                array_push($user_label,[
                    'name'=>$label,
                ]);
            }


            if(!valid_phone($phone)){
                $phone_error[] = $phone;
            }elseif(in_array($phone,$all_user_phone)){
                $phone_exits[] = $phone;
            }else{
                array_push($user_data,[
                    'type' => 1,//企业会员
                    'name' => $user_name,
                    'phone' => $phone,
                    '_label_name' => $label,
                    '_coupon_money' => $coupon_money,
                ]);
            }

        }
        if(count($user_label)>0){
            UserLabelModel::insertAll($user_label);
            //查询所有用户label
            $all_user_label = UserLabelModel::column('id','name');
        }

        //遍历用户信息
        foreach ($user_data as &$vo){
            $vo['lid'] = isset($all_user_label[$vo['_label_name']])?$all_user_label[$vo['_label_name']]:0;
        }
        if(count($user_data)>0){
            $save_coupon=[];
            $all_save_users = (new UserModel())->saveAll($user_data)->each(function($item,$index)use(&$save_coupon){
                array_push($save_coupon,[
                    'uid' => $item['id'],
                    'money' => $item['_coupon_money'],
                    'name' => '优惠券',
                    'content' => '优惠券',
                ]) ;
            });
            //导入优惠券
            if(count($save_coupon)>0){
                (new CouponUserModel())->saveAll($save_coupon);
            }
        }

        return $this->_resData(1,'成功导入:'.count($user_data).'条数据',['phone_exits'=>$phone_exits,'phone_error'=>$phone_error]);
    }


    public function withdraw()
    {
        $state = input('state',0,'intval');
        $keyword = input('keyword','','trim');
        $where=[];
        !empty($keyword) && $where[]=['name|phone','like','%'.$keyword.'%'];

        if($state==1){
            $where[] =['status','=',0];
        }elseif($state==2){
            $where[] =['status','=',1];
        }elseif($state==3){
            $where[] =['status','=',2];
        }

        $list = UserWithdrawModel::where($where)
            ->with(['linkUser'])
            ->order('status asc,auth_time desc,id desc')
            ->paginate();
        // 获取分页显示
        $page = $list->render();
        return view('withdraw',[
            'state' => $state,
            'keyword' => $keyword,
            'list' => $list,
            'page'=>$page
        ]);
    }


    public function withdrawAuth()
    {
        $id = input('id',0,'intval');
        $state = input('state',0,'intval');
        $auth_content = input('auth_content','','trim');
        try{
            UserWithdrawModel::withdrawAuth($id,$state,$auth_content);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    //删除数据
    public function withdrawDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            UserWithdrawModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }



    public function comment()
    {
        $state = input('state',0,'intval');
        $list = GdCommentModel::getPageData(input());
        return view('comment',[
            'state' => $state,
            'list' => $list,
            'page' => $list->render()
        ]);
    }

    //删除数据
    public function commentDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            GdCommentModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    public function shareGoods()
    {
        $uid = input('id',0,'intval');
        $start_date = input('start_date');
        $end_date = input('end_date');
        $keyword = input('keyword','','trim');
        $where[] = ['share_uid','=',$uid];
        $where[] = ['pay_time','>',0];//已付款
        if(!empty($keyword)){
            $where[] = ['no','like','%'.$keyword.'%'];
        }


        //按时间查询
        $start_time = empty($start_date)?'':strtotime($start_date);
        $end__time = empty($end_date)?'':strtotime('+1 day',strtotime($end_date));
        if(!empty($start_time) && !empty($end__time)){
            $where[] = ['linkOrder.create_time','>=',$start_time];
            $where[] = ['linkOrder.create_time','<=',$end__time];
        }elseif(!empty($start_time)){
            $where[] = ['linkOrder.create_time','>=',$start_time];
        }elseif(!empty($end__time)){
            $where[] = ['clinkOrder.reate_time','<=',$end__time];
        }

        $list = OrderGoodsModel::withJoin('linkOrder','left')->where($where)->order('linkOrder.id desc')->paginate();


        return view('shareGoods',[
            'list' => $list,
            'page'=>$list->render(),
            'keyword'=>$keyword,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
        ]);
    }
}