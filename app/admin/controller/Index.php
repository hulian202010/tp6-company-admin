<?php
namespace app\admin\controller;

use app\common\model\ArticleCateModel;
use app\common\model\GoodsCateModel;
use app\common\model\HelpCenterModel;
use app\common\model\HelpInfoModel;
use app\common\model\ImageModel;
use app\common\model\MerchantCateModel;
use app\common\model\MerchantModel;
use app\common\model\OrderModel;
use app\common\model\RechargeCateModel;
use app\common\model\RechargeModel;
use app\common\model\SysManagerModel;
use app\common\model\SysRoleModel;
use app\common\model\SysSettingModel;
use app\common\model\UserModel;
use app\common\service\TimeActivity;

class Index extends Common {

    protected $ignore_action=['index','login','verify'];

    public function index(){
        return view('index',[

        ]);
    }

//    //获取管理员授权栏目
//    public function authMenu()
//    {
//        return $this->_resData(1,'获取成功',[
//            ['auth_rules'=>'/', 'auth_name' => '首页', 'auth_icon' => 'layui-icon-home'],
//
////            ['auth_rules'=>'', 'auth_name' => '资讯管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
////                ['auth_rules'=>'/article/cate', 'auth_name' => '资讯分类'],
////                ['auth_rules'=>'/article/index', 'auth_name' => '资讯列表'],
////            ]],
//            ['auth_rules'=>'', 'auth_name' => '发布管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
//                ['auth_rules'=>'/recharge/cate', 'auth_name' => '分类管理'],
//                ['auth_rules'=>'/recharge/support', 'auth_name' => '供应列表'],
//                ['auth_rules'=>'/recharge/buy', 'auth_name' => '求购列表'],
//                ['auth_rules'=>'/recharge/fruit', 'auth_name' => '果园列表'],
//                ['auth_rules'=>'/recharge/circle', 'auth_name' => '苗圈列表'],
//            ]],
////            ['auth_rules'=>'', 'auth_name' => '商家管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
////                ['auth_rules'=>'/merchant/index', 'auth_name' => '商家列表'],
////                ['auth_rules'=>'/comment/index', 'auth_name' => '评论列表'],
////            ]],
//            ['auth_rules'=>'', 'auth_name' => '用户管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
//                ['auth_rules'=>'/user/index', 'auth_name' => '用户列表'],
//                ['auth_rules'=>'/user/auth', 'auth_name' => '认证审核'],
//                ['auth_rules'=>'/user/help', 'auth_name' => '会员帮助'],
//            ]],
////            ['auth_rules'=>'', 'auth_name' => '产品管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
////                ['auth_rules'=>'/goods/cate', 'auth_name' => '产品分类'],
////                ['auth_rules'=>'/goods/index', 'auth_name' => '产品列表'],
////            ]],
//
//            ['auth_rules'=>'', 'auth_name' => '协议管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
//                ['auth_rules'=>'/protocol/index/type=aboutUs', 'auth_name' => '关于我们'],
//                ['auth_rules'=>'/protocol/index/type=tipProtocol', 'auth_name' => '隐私政策'],
//                ['auth_rules'=>'/protocol/index/type=registerProtocol', 'auth_name' => '服务条款'],
//                ['auth_rules'=>'/protocol/index/type=guaranteeProtocol', 'auth_name' => '担保认证'],
//            ]],
//            ['auth_rules'=>'', 'auth_name' => '后台管理', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
//                ['auth_rules'=>'/system/setting', 'auth_name' => '系统设置'],
//                ['auth_rules'=>'/system/image', 'auth_name' => '图片管理'],
//                ['auth_rules'=>'/system/wgt', 'auth_name' => '更新包管理'],
//                ['auth_rules'=>'/system/manager', 'auth_name' => '管理员管理'],
//
//            ]],
////            ['auth_rules'=>'', 'auth_name' => '模版文件', 'auth_icon' => 'layui-icon-home','childs'=>[
////                ['auth_rules'=>'', 'auth_name' => '权限管理', 'auth_icon' => 'layui-icon-home','childs'=>[
////                    ['auth_rules'=>'/system/showCache', 'auth_name' => '查看权限缓存', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/system/clearCache', 'auth_name' => '清空权限缓存', 'auth_icon' => 'layui-icon-home'],
////                ]],
////                ['auth_rules'=>'', 'auth_name' => '角色管理', 'auth_icon' => 'layui-icon-home','childs'=>[
////                    ['auth_rules'=>'/roles/list', 'auth_name' => '角色管理', 'auth_icon' => 'layui-icon-home'],
////                ]],
////                ['auth_rules'=>'/user/list', 'auth_name' => '用户管理', 'auth_icon' => 'layui-icon-home'],
////                ['auth_rules'=>'/module/admin', 'auth_name' => '新增模块', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
////                    ['auth_rules'=>'/module/admin', 'auth_name' => 'admin', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/module/helper', 'auth_name' => 'helper', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/module/loadbar', 'auth_name' => 'loadbar', 'auth_icon' => 'layui-icon-home'],
////                ]],
////                ['auth_rules'=>'/detail', 'auth_name' => '详情页', 'auth_icon' => 'layui-icon-rate-half','childs'=>[
////                    ['auth_rules'=>'/detail/plan', 'auth_name' => '工作计划', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/detail/form', 'auth_name' => '表单', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/chart/index', 'auth_name' => '数据统计', 'auth_icon' => 'layui-icon-home'],
////                ]],
////                ['auth_rules'=>'exception', 'auth_name' => '异常页', 'auth_icon' => 'layui-icon-error','childs'=>[
////                    ['auth_rules'=>'/exception/403', 'auth_name' => '403', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/exception/404', 'auth_name' => '404', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/exception/500', 'auth_name' => '500', 'auth_icon' => 'layui-icon-home'],
////                    ['auth_rules'=>'/list/card', 'auth_name' => '卡片列表', 'auth_icon' => 'layui-icon-home'],
////                ]],
////            ]],
//
//        ]);
//    }

    public function defInfo()
    {
        $data = [];
        $type = input('type','','trim');
        $mode = input('mode','','trim');
        if($type=='article_cate'){
            $data['cate_list'] = ArticleCateModel::getSelectList();
        }elseif($type=='goods_cate'){
            $data['cate_list'] = GoodsCateModel::getSelectList();
        }elseif($type=='logistics'){
            //快递
            $data['logistics_list'] = SysSettingModel::getContent('freight','logistics_enter');
        }elseif($type=='user_type'){//快递

            $data['type_list'] = UserModel::getPropInfo('fields_type');
        }elseif($type=='manager'){//管理员
            $data['role_list'] = SysRoleModel::getSelectList();
        }elseif($type=='image'){//管理员
            $data['image_list'] = ImageModel::getPropInfo('fields_type');

        } elseif ($type=='show_total_data') {
            $today_time = strtotime(date('Y-m-d'));
            //今日成交订单数
            $data['today_order_num'] = OrderModel::where('create_time','>',$today_time)->where(OrderModel::getStateWhere(2))->count();
            //今日成交总金额
            $data['today_order_money'] = OrderModel::where('create_time','>',$today_time)->where(OrderModel::getStateWhere(2))->sum('pay_money');
            //累计成交订单数
            $data['order_num'] = OrderModel::where(OrderModel::getStateWhere(2))->count();
            //累计成交总金额
            $data['order_money'] = OrderModel::where(OrderModel::getStateWhere(2))->sum('pay_money');
        } elseif ($type=='show_order') {
            $order_list = [];

            !empty($mode) && OrderModel::getPageData(['activeState'=>$mode])->each(function($item)use(&$order_list){
                array_push($order_list,$item->apiFullInfo());
            });
            $data['order_list'] = $order_list;
        } elseif ($type=='show_echarts') {
            $name = '';
            $series_data = [];
            $x_axis_data = [];
            if($mode=='order') {
                $name = "销售额";
                list($x_axis_data, $series_data) = OrderModel::echarts(input());
            }elseif($mode=='user') {
                $name = "注册量";
                list($x_axis_data, $series_data) = UserModel::echarts(input());
            }
//            dump($echarts_list);exit;
            $data['name'] = $name;
            $data['series_data'] = $series_data;
            $data['x_axis_data'] = $x_axis_data;

        }elseif($type=='time_activity'){//管理员
            $data['time_activity'] = TimeActivity::getTime();
        }elseif($type=='integral_goods'){
            $data['cate_list'] = [];
        }
        return $this->_resData(1,'获取成功',$data);
    }


    /**
     */
    public function login()
    {
        //用户登录
        try{
            $model = SysManagerModel::login(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'登录成功',$this->loginInfo($model));
    }



    public function verify()
    {
        return \think\captcha\facade\Captcha::create();
    }
}