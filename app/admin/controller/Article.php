<?php
namespace app\admin\controller;

use app\common\model\ArticleCateModel;
use app\common\model\ArticleModel;
use app\common\model\GdCateModel;
use app\common\model\PageHonorModel;
use app\common\model\SingleArticleModel;
use app\common\validate\ArticleCateValidate;
use app\common\validate\ArticleValidate;
use app\common\validate\SingleArticleValidate;

class Article extends Common
{

    public function cate()
    {
        $input_data = input();
        $list =[];
        ArticleCateModel::getAllCate($input_data)->each(function($item,$index)use(&$list){
            $info = $item->apiFullInfo();
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if(!empty($linkChild)){
                foreach ($linkChild as $vo){
                    array_push($child_list,$vo->apiFullInfo());
                }
            }
            $info['child_list'] = $child_list;
            array_push($list,$info);
        });

        return $this->_resData(1,'获取成功',['list'=>$list]);
    }

    public function cateAdd()
    {

        $php_input = $this->request->param();
        try{
            ArticleCateModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);

    }

    //删除数据
    public function cateDel()
    {
        $id = $this->request->param('id',0,'int');
        $model = new ArticleCateModel();
        try{
            $model->actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //修改分类信息
    public function cateModInfo()
    {
        $php_input = input();
        try{
            ArticleCateModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    public function lists()
    {
        $input_data = input();
        $list =[];


        $info = ArticleModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });

        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);


    }

    public function handleSaveData()
    {

        $php_input = $this->request->param();
        try{
            ArticleModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);


        $id = $this->request->param('id');
        $model = new ArticleModel();

        //表单提交
        if($this->request->isAjax()){
            $php_input = $this->request->param();
            $cid = input('cid');
            if(!empty($cid)){
                $group_cid=explode(',',$cid);
                $php_input['cid'] = $group_cid[0]??0;
                $php_input['ct_id'] = $group_cid[1]??0;
            }
            //指定类型
            !isset($php_input['type']) && $php_input['type']=$type;
            if(!empty($php_input['img']) && is_array($php_input['img']))$php_input['img']=implode(',',$php_input['img']);
            $validate = new ArticleValidate();
            try{
                $model->actionAdd($php_input,$validate);//调用BaseModel中封装的添加/更新操作
            }catch (\Exception $e){
                return json(['code'=>0,'msg'=>$e->getMessage()]);
            }
            return json(['code'=>1,'msg'=>'操作成功']);
        }

        $model = $model->find($id);
        $cate = ArticleCateModel::with(['linkChild'])->where(['status'=>1,'pid'=>0])->order('sort asc')->select()->toArray();
        $page = ArticleModel::getPropInfo('fields_type',$type,'page','value');
        return view($page,[
            'model' => $model,
            'cate'=>$cate,
        ]);
    }


    //删除数据
    public function del()
    {
        $id = $this->request->param('id',0,'int');
        $model = new ArticleModel();
        try{
            $model->actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }



    //修改分类信息
    public function modInfo()
    {
        $php_input = input();
        try{
            ArticleModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }




}
