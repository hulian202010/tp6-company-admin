<?php
namespace app\admin\controller;

use app\common\model\ArticleCateModel;
use app\common\model\ArticleModel;
use app\common\model\PlatformGiftModel;

class Platform extends Common
{

    public function giftLists()
    {
        $input_data = input();
        $list =[];


        $info = PlatformGiftModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });

        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);


    }

    public function giftSave()
    {
        $php_input = $this->request->param();
        try{
            PlatformGiftModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    //删除数据
    public function giftDel()
    {
        $id = input('id',0,'intval');
        try{
            PlatformGiftModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }
    //修改分类信息
    public function giftModInfo()
    {
        $php_input = input();
        try{
            PlatformGiftModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }



    //奖品列表
    public function awardList()
    {
        $input_data = input();
        $list = [];
        $info = UserAwardModel::getPageData($input_data)->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total_page'=>$info->lastPage()]);
    }


    public function awardSend()
    {
        try{
            UserAwardModel::send($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }
    public function awardDelete()
    {
        $id = input('id',0,'intval');
        try{
            UserAwardModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

}
