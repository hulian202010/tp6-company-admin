<?php
namespace app\admin\controller;

use app\common\model\ImageModel;
use app\common\model\SysManagerModel;
use app\common\model\SysRoleModel;
use app\common\model\SysSettingModel;
use app\common\model\UserModel;
use app\common\model\WgtModel;

class System extends Common
{
    //各种图
    public function image()
    {
        $list = [];
        $info = ImageModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }

    //
    public function imageAdd()
    {
        try{
            ImageModel::handleSaveData(input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');


    }

    //删除数据
    public function imageDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            ImageModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //修改信息
    public function imageModInfo()
    {
        $php_input = input();
        try{
            ImageModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }

    //获取管理员列表
    public function manager()
    {
        $list = [];
        $info = SysManagerModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);
    }

    //
    public function managerAdd()
    {
        $php_input = $this->request->param();
        try{
            SysManagerModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //删除数据
    public function managerDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            SysManagerModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    //修改信息
    public function managerModInfo()
    {
        $php_input = input();
        try{
            SysManagerModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }
    //获取列表
    public function role()
    {
        $list = SysRoleModel::getPageData(input());
        return $this->_resData(1,'获取成功',['list'=>$list]);
    }

    //
    public function roleAdd()
    {
        $php_input = $this->request->param();
        try{
            SysRoleModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //删除数据
    public function roleDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            SysRoleModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    //修改信息
    public function roleModInfo()
    {
        $php_input = input();
        try{
            SysRoleModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }


    //系统设置
    public function setting()
    {
        $type = input('type','','trim');
        $table_type = input('table_type','','trim');
        $content = input('content');

        $data = [];
        if(!empty($content)){

            $content = empty($content)?[]:$content;
            foreach ($content as $key=>$vo){
                $data[$key] = SysSettingModel::getContent($type, $key,false);
            }
        }else{
            $data['content'] = SysSettingModel::getContent($type, null,false);
        }
        if($table_type==='user_type_json'){
            $data['table_col'] = UserModel::getPropInfo('fields_type_table_col');
            $data['table_list'] = UserModel::getPropInfo('fields_type');
        }elseif($table_type==='user_draw_json'){

        }

        return $this->_resData(1,'操作成功',$data);
    }

    //系统设置
    public function settingSave()
    {
        $input_data = input();
        $type = $input_data['type']??'';
        $table_type = $input_data['table_type']??'';
        $table_content = $input_data['table_content']??'';
        $content = $input_data['content']??'';
        unset($input_data['type']);
        unset($input_data['table_type']);
        try{
            if(!empty($table_type)){
                SysSettingModel::setContent($table_type, $table_content);
            }
            if(!empty($type)){
                SysSettingModel::setContent($type, $content);
            }
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    public function wgt()
    {
        $list = [];
        $info = WgtModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'last_page'=>$info->lastPage()]);



    }
    public function wgtSaveData()
    {
        $model = new WgtModel();
        $version = input('version');
        $type = input('type',0,'intval');
        $platform = input('platform',0,'intval');
        $path = input('path');
        $content = input('content');

        if(empty($version)) throw new \Exception('请输入版本号');
        if(empty($path)) throw new \Exception('请上传wgt包');

        //查找最后一条记录
        $last_model = WgtModel::where(['type'=>$type,'platform'=>$platform])->order('id desc')->find();
        if(!empty($last_model) && $last_model['version']>=$version){
            throw new \Exception('最新添加的版本不得低于历史版本');
        }
        $model->setAttrs([
            'version' => $version,
            'platform' => $platform,
            'type' => $type,
            'path' => $path,
            'content' => $content,
        ]);
        $model->save();
        return $this->_resData(1,'保存成功');

    }


    //删除数据
    public function wgtDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            WgtModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    //修改信息
    public function wgtModInfo()
    {
        $php_input = input();
        try{
            WgtModel::modInfo($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>'操作成功']);
    }
}