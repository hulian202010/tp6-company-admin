<?php
// 应用公共文件
/**
 * 验证手机号码
 * */
function valid_phone($phone){
    return preg_match('/^1\d{10}$/',$phone);
}


//本周日期
function get_current_week()
{
    //当前日期
    $sdefaultDate = date("Y-m-d");
//$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
    $first=1;
//获取当前周的第几天 周日是 0 周一到周六是 1 - 6
    $w=date('w',strtotime($sdefaultDate));
//获取本周开始日期，如果$w是0，则表示周日，减去 6 天
    $week_start=date('Y-m-d',strtotime("$sdefaultDate -".($w ? $w - $first : 6).' days'));
//本周结束日期
    $week_end=date('Y-m-d',strtotime("$week_start +6 days"));
    $date = [$week_start];
    for($i=1;$i<7;$i++){
        $date[] = date('Y-m-d',strtotime("$week_start +$i days"));
    }
    return $date;
}

//获取时间
function get_op_time($date_mode)
{
    $start_time = strtotime(date('Y-m-d'));
    $time_mode = 'day';
    $day_format = 'Y-m-d';
    $start_i = 1;
    if ( $date_mode == 'day' ) { //按小时计算
        $last_time = 24;
        $time_mode = 'hours';
        $day_format = 'H';
        $start_i = 0;
    } elseif ( $date_mode == 'week' ) {
        $last_time = 7;
        $start_time -= $last_time*86400;
    } elseif ( $date_mode == 'month' ) {
        $last_time = 30;
        $start_time -= $last_time*86400;
    }  elseif ( $date_mode == 'month60' ) {
        $last_time = 60;
        $start_time -= $last_time*86400;
    } elseif ( $date_mode == 'year' ) {
        $last_time = 365;
        $start_time -= $last_time*86400;
    }
    $data = [];
    for ($i=$start_i; $i<=$last_time; $i++){
        $data[] = date($day_format,strtotime('+'.$i.' '.$time_mode,$start_time));
    }

    return $data;
}

//验证时间
function check_date($date){
    $bool = true;
    if(!preg_match ('@^[0-9][0-9/-: ]*[0-9]$@', $date, $parts)){
        $bool = false;
    }
    return $bool;
}

//计算时间
function time_tranx($the_time)
{
    $now_time = time();
    $dur = $now_time - $the_time;
    if ($dur <= 0) {
        $mas =  '刚刚';
    } else {
        if ($dur < 60) {
            $mas =  $dur . '秒前';
        } else {
            if ($dur < 3600) {
                $mas =  floor($dur / 60) . '分钟前';
            } else {
                if ($dur < 86400) {
                    $mas =  floor($dur / 3600) . '小时前';
                } else {
                    if ($dur < 259200) { //3天内
                        $mas =  floor($dur / 86400) . '天前';
                    } else {
                        $mas =  date("Y-m-d H:i:s",$the_time);
                    }
                }
            }
        }
    }
    return $mas;
}


/**
 *   将数组转换为xml
 *    @param array $data    要转换的数组
 *   @param bool $root     是否要根节点
 *   @return string         xml字符串
 *    @author Dragondean
 *    @url    http://www.cnblogs.com/dragondean
 */
function arr2xml($data, $root = true){
    $str="";
    if($root)$str .= "<xml>";
    foreach($data as $key => $val){
        if(is_array($val)){
            $child = arr2xml($val, false);
            $str .= "<$key>$child</$key>";
        }else{
            $str.= "<$key>$val</$key>";
        }
    }
    if($root)$str .= "</xml>";
    return $str;
}