<?php
namespace app\listener;

use think\facade\Db;

class CouponAutoCancel
{
    public function handle()
    {
        if(!$this->_checkRunTime()){
            return ;
        }

        $where = [];
        $where[] = ['status','=',0];
        $where[] = ['over_date','<', date('Y-m-d')];
        try{
            \app\common\model\CouponUserModel::where($where)->update(['status'=>2]);
        }catch (\Exception $e){
            \think\facade\Log::write('优惠券过期处理异常：'.$e->getMessage());
        }

//        $sys_model = new \app\common\model\SysManagerModel();
//        $sys_model->id=-1;
//        \app\common\model\CouponUserModel::where($where)->select()->each(function($item,$index){
//            try{
//                $item->overDayCancel();
//            }catch (\Exception $e){
//                \think\facade\Log::write('优惠券过期处理异常：'.$item['id'].'处理异常:'.$e->getMessage());
//            }
//        });

    }


    //检测是否到了执行的时间
    //每6小时执行一次
    private function _checkRunTime()
    {
        $is_check = false;
        $cache_name = 'coupon_auto_cancel1';
        if(!cache($cache_name)){
            cache($cache_name, date('Y-m-d H:i:s'), 3500*6);
            $is_check = true;
        }
        return $is_check;

    }
}