<?php
namespace app\listener;

class OrderAutoReceive
{
    public function handle()
    {
        if($this->_checkRunTime()){
            //自动完成时间
            $cond_day = \app\common\model\OrderModel::ORDER_AUTO_COMPLETE_DAY;
            $cond_send_end_time = strtotime("-".$cond_day." day",strtotime(date('Y-m-d'))); //配送结束时间
//        dump($cond_send_end_time);
//        dump(date('Y-m-d H:i:s',$cond_send_end_time));
//        dump(date('Y-m-d H:i:s',1575302399));exit;

            $where = [];
            $where[] = ['status','=',1];
            $where[] = ['is_send','=',1];
            $where[] = ['is_receive','=',0];
            $where[] = ['send_end_time','<=', $cond_send_end_time];
//        dump($where);exit;
            \app\common\model\OrderModel::where($where)->select()->each(function($item,$index){
                try{
                    \app\common\model\OrderModel::receive(null, null, $item);

                }catch (\Exception $e){

                }
//            exit;
            });
        }

    }

    //检测是否到了执行的时间
    //每一小时执行一次
    private function _checkRunTime()
    {
        $is_check = false;
        $cache_name = 'order_auto_receive_cache';
        if(!cache($cache_name)){
            cache($cache_name, date('Y-m-d H:i:s'), 3600);
            $is_check = true;
        }
        return $is_check;

    }
}