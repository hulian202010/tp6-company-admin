<?php
namespace app\listener;

use app\common\model\SysMoneyQueueModel;

class SysMoneyCustomer
{
    public function handle()
    {
        if($this->_checkRunTime()){
            SysMoneyQueueModel::customer();
        }

    }

    //检测是否到了执行的时间
    //每30秒执行一次
    private function _checkRunTime()
    {
        $is_check = false;
        $cache_name = '_cache_name_SysMoneyCustomer';
        if(!cache($cache_name)){
            cache($cache_name, date('Y-m-d H:i:s'), 30);
            $is_check = true;
        }
        return $is_check;

    }
}