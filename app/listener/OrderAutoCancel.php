<?php
namespace app\listener;

use think\facade\Db;

class OrderAutoCancel
{
    public function handle()
    {
        if(!$this->_checkRunTime()){
            return ;
        }
        //有效期
        $cond_day = \app\common\model\OrderModel::ORDER_EXP_TIME;
        $cond_time = time()-$cond_day+3; //取消订单时间--延迟处理

        $where = [];
        $where[] = ['status','=',0];
        $where[] = ['create_time','<', $cond_time];
        $sys_model = new \app\common\model\SysManagerModel();
        $sys_model->id=-1;
        \think\facade\Db::table('order')->field('id')->where($where)->select()->each(function($item,$index)use($sys_model){

            try{
                \app\common\model\OrderModel::cancel($sys_model, $item['id']);
            }catch (\Exception $e){
                \think\facade\Log::write('订单'.$item['id'].'处理异常:'.$e->getMessage());
            }
        });

    }


    //检测是否到了执行的时间
    //每一小时执行一次
    private function _checkRunTime()
    {
        $is_check = false;
        $cache_name = 'order_auto_cancel';
        if(!cache($cache_name)){
            cache($cache_name, date('Y-m-d H:i:s'), 3);
            $is_check = true;
        }
        return $is_check;

    }
}