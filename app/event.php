<?php
// 事件定义文件
return [
    'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [
            //优惠券过期处理
            'app\listener\CouponAutoCancel',
            //取消订单
            'app\listener\OrderAutoCancel',
            //自动收货
            'app\listener\OrderAutoReceive',
            //发放商户获得的金额
//            'app\listener\SysMoneyCustomer',
        ],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
    ],

    'subscribe' => [
    ],
];
